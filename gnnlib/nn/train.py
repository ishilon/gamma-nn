import os
from datetime import datetime
import time
import importlib
import re
import tensorflow as tf
# from tensorflow.python import debug as tf_debug
import numpy as np

from gnnlib.utils import utils


def activation_summary(x, tower_name):
    """Helper to create summaries for activations.

    Creates a summary that provides a histogram of activations.
    Creates a summary that measures the sparsity of activations.

    Args:
    x: Tensor
    Returns:
    nothing
    """
    # Remove 'tower_[0-9]/' from the name in case this is a multi-GPU training
    # session. This helps the clarity of presentation on tensorboard.
    tensor_name = re.sub( '%s_[0-9]*/' % tower_name, '', x.op.name )
    tf.summary.histogram( tensor_name + '/activations', x )
    tf.summary.scalar( tensor_name + '/sparsity', tf.nn.zero_fraction(x) )


def variable_on_cpu(name, shape, initializer):
    """Helper to create a Variable stored on CPU memory.

    Args:
    name: name of the variable
    shape: list of ints
    initializer: initializer for Variable

    Returns:
    Variable Tensor
    """
    with tf.device('/cpu:0'):
        # dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
        dtype = tf.float32
        var = tf.get_variable(name, shape, initializer=initializer, dtype=dtype)
    return var


def variable_with_weight_decay(name, shape, stddev, wd, mean=0.0):
    """Helper to create an initialized Variable with weight decay.

    Note that the Variable is initialized with a truncated normal distribution.
    A weight decay is added only if one is specified.

    Args:
    name: name of the variable
    shape: list of ints
    stddev: standard deviation of a truncated Gaussian
    wd: add L2Loss weight decay multiplied by this float. If None, weight
        decay is not added for this Variable.

    Returns:
    Variable Tensor
    """
    # dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
    dtype = tf.float32
    var = variable_on_cpu( name, shape, tf.truncated_normal_initializer(mean=mean, stddev=stddev, dtype=dtype) )
    if wd is not None:
        weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
        tf.add_to_collection('losses', weight_decay)
    return var


def average_gradients( tower_grads ):
    """Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.

    Args:
        tower_grads: List of lists of (gradient, variable) tuples. The outer list
        is over individual gradients. The inner list is over the gradient
        calculation for each tower.

    Returns:
        List of pairs of (gradient, variable) where the gradient has been averaged
        across all towers.
    """
    average_grads = []
    for grad_and_vars in zip( *tower_grads ):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims( g, 0 )

            # Append on a 'tower' dimension which we will average over below.
            grads.append( expanded_g )

        # Average over the 'tower' dimension.
        grad = tf.concat( axis=0, values=grads )
        grad = tf.reduce_mean( grad, 0 )

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append( grad_and_var )

    return average_grads


def execute_cycle( config_args, ds_dir, train_dir, ckpt_dir, steps_per_train_cycle ):
    """
    Train a Gamma-nn task
    :param config_args: config arguments
    :param ds_dir: datasets directory (where the tfrecords are found)
    :param train_dir: directory to store training log files
    :param ckpt_dir: directory to save and load checkpoint files
    :param steps_per_train_cycle: Number of iterations per training cycle
    :return:
    """
    # If a model is trained with multiple GPUs, prefix all Op names with tower_name
    # to differentiate the operations. Note that this prefix is removed from the
    # names of the summaries when visualizing a model.
    tower_name = utils.get_tower_name()

    task_module = 'gnnlib.tasks.' + config_args.task
    model_module = 'gnnlib.nn.' + config_args.task + '_model'
    task = importlib.import_module( task_module )
    model = importlib.import_module( model_module )

    tmp_dict = model.get_meta_consts()
    moving_average_decay = tmp_dict['moving_average_decay']
    num_epochs_per_decay = tmp_dict['num_epochs_per_decay']
    learning_rate_decay_factor = tmp_dict['learning_rate_decay_factor']
    initial_learning_rate = tmp_dict['initial_learning_rate']
    step_lr = tmp_dict['staircase']

    with tf.Graph().as_default(), tf.device( '/cpu:0' ):
        # Create a variable to count the number of train() calls. This equals the
        # number of batches processed * num_gpus.
        global_step = tf.get_variable( 'global_step', [], initializer = tf.constant_initializer( 0 ), trainable = False )

        # Calculate the learning rate schedule.
        num_batches_per_epoch = (config_args.train_events / config_args.batch_size)
        decay_steps = int( num_batches_per_epoch * num_epochs_per_decay )

        # Decay the learning rate exponentially based on the number of steps.
        lr = tf.train.exponential_decay( initial_learning_rate,
                                         global_step,
                                         decay_steps,
                                         learning_rate_decay_factor,
                                         staircase = step_lr )
        # Create an optimizer.
        opt = None
        if config_args.optimizer == 'adam':
            opt = tf.train.AdamOptimizer(lr)
        elif config_args.optimizer == 'grad_des':
            opt = tf.train.GradientDescentOptimizer(lr)

        # Calculate the gradients for each model tower.
        tower_grads = []
        summaries = None
        loss = None
        with tf.variable_scope( tf.get_variable_scope() ):
            for i in range( config_args.num_gpus ):
                with tf.device( '/gpu:%d' % i ):
                    with tf.name_scope( '%s_%d' % (tower_name, i) ) as scope:
                        # Calculate the loss for one tower of the HESSnn model. This function
                        # constructs the entire HESSnn model but shares the variables across
                        # all towers.
                        loss, _, _ = task.tower_loss( config_args, scope, model, ds_dir, tower_name )

                        # Reuse variables for the next tower.
                        # This function lets get_variable() use an existing varuable,
                        # rather than initialising a new one, with the name Variable_scope/Variable_name.
                        tf.get_variable_scope().reuse_variables()

                        # Retain the summaries from the final tower.
                        # tf.GraphKeys are standard names to graph collections
                        summaries = tf.get_collection( tf.GraphKeys.SUMMARIES, scope )

                        # Calculate the gradients for the batch of data on this HESSnn tower.
                        # This is the first part of the minimize() function.
                        grads = opt.compute_gradients( loss )

                        # Keep track of the gradients across all towers.
                        tower_grads.append( grads )

        # We must calculate the mean of each gradient. Note that this is the
        # synchronization point across all towers.
        grads = average_gradients( tower_grads )

        # Add a summary to track the learning rate.
        summaries.append( tf.summary.scalar( 'learning_rate', lr ) )

        # Add histograms for gradients.
        for grad, var in grads:
            if grad is not None:
                summaries.append( tf.summary.histogram( var.op.name + '/gradients', grad ) )

        # Apply the gradients to adjust the shared variables.
        # This is the second part of the minimize() function.
        apply_gradient_op = opt.apply_gradients( grads, global_step=global_step )

        # Add histograms for trainable variables.
        for var in tf.trainable_variables():
            summaries.append( tf.summary.histogram( var.op.name, var ) )

        # Track the moving averages of all trainable variables.
        # The apply() method adds shadow copies of trained variables and add ops
        # that maintain a moving average of the trained variables in their shadow copies.
        # It is used when building the training model.
        variable_averages = tf.train.ExponentialMovingAverage( moving_average_decay, global_step )
        variables_averages_op = variable_averages.apply( tf.trainable_variables() )

        # Group all updates to into a single train op.
        if 'bn' in config_args.train_type:
            print("Added batch normalization. Make sure that your model makes use of it!")
            update_ops = tf.get_collection( tf.GraphKeys.UPDATE_OPS )
            with tf.control_dependencies( update_ops ):
                train_op = tf.group( apply_gradient_op, variables_averages_op )
        else:
            train_op = tf.group( apply_gradient_op, variables_averages_op )

        # Create a saver.
        saver = tf.train.Saver( tf.global_variables() )

        # Build the summary operation from the last tower summaries.
        summary_op = tf.summary.merge( summaries )

        # Build an initialization operation to run below.
        init_op = tf.global_variables_initializer()

        # Start running operations on the Graph. allow_soft_placement must be set to
        # True to build towers on GPU, as some of the ops do not have GPU
        # implementations.
        config = tf.ConfigProto( allow_soft_placement = True,
                                 log_device_placement = config_args.log_device_placement_bool )
        config.gpu_options.allow_growth = True
        sess = tf.Session( config=config )

        if config_args.load_checkpoint_bool:
            ckpt = tf.train.get_checkpoint_state( ckpt_dir )
            if ckpt and ckpt.model_checkpoint_path:
                # Restores from checkpoint
                saver.restore( sess, ckpt.model_checkpoint_path )
                # Assuming model_checkpoint_path looks something like:
                #   /my-favorite-path/model.ckpt-0,
                # extract global_step from it.
                last_step_from_ckpt = int( ckpt.model_checkpoint_path.split( '/' )[ -1 ].split( '-' )[ -1 ] )
                global_step_init = last_step_from_ckpt + 1
                print( '\nLoaded checkpoint with global_step = {0}'.format( last_step_from_ckpt ) )
            else:
                print( 'No checkpoint file found. Default initializers will be called.' )
                if not config_args.cluster_bool:
                    utils.exit_point( timeout=10.0 )
                sess.run( init_op )
                global_step_init = 1
        else:
            sess.run( init_op )
            global_step_init = 1

        # Start the queue runners.
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners( sess=sess, coord=coord )

        # #####################
        # ##  THE DEBUGGER  ###
        # #####################

        # sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        summary_writer = tf.summary.FileWriter( train_dir, sess.graph )
        eval_point = ((global_step_init // steps_per_train_cycle) + 1) * steps_per_train_cycle
        print( 'Learning rate = ', sess.run( lr ) )
        print('Next evaluation will be at step {0} (epoch = {1})'.format( eval_point, eval_point // (config_args.train_events / config_args.batch_size) ))
        for step in range( global_step_init, eval_point + 1 ):
            start_time = time.time()

            # print( sess.run( scaled_label ) * config_args.max_energy )

            _, loss_value = sess.run( [ train_op, loss ] )
            # _ = sess.run( train_op )
            duration = time.time() - start_time

            assert not np.isnan( loss_value ), 'Model diverged with loss = NaN'

            if step == global_step_init and step > 0:
                summary_str = sess.run( summary_op )
                summary_writer.add_summary( summary_str, step )
                summary_writer.flush()  # flush data to disk

            if step % 50 == 0 or step == global_step_init:
                num_examples_per_step = config_args.batch_size * config_args.num_gpus
                examples_per_sec = num_examples_per_step / duration
                sec_per_batch = duration / config_args.num_gpus

                format_str = ('%s: global_step #%d, loss = %.4f (%.1f examples/sec; %.3f '
                              'sec/batch)')
                print( format_str % (datetime.now(), step, loss_value,
                                     examples_per_sec, sec_per_batch) )

            if step % 100 == 0 or (step + 1) == config_args.max_steps + global_step_init or step == eval_point:
                summary_str = sess.run( summary_op )
                summary_writer.add_summary( summary_str, step )
                summary_writer.flush()  # flush data to disk

                # utils.write_memory_summary(sess, data_dir, step)

            # Save the model checkpoint periodically.
            if (step > 0 and step % 1000 == 0) or step == eval_point:
                checkpoint_path = os.path.join( ckpt_dir, 'model.ckpt' )
                saver.save( sess, checkpoint_path, global_step = step )
                print( 'Checkpoint file for step #{0} written!'.format( step ) )
                print( 'Learning rate = ', sess.run( lr ) )
                print( 'Reminder: {0} steps per l.r. decay'.format( decay_steps ) )

        summary_writer.close()
        coord.request_stop()
        coord.join( threads )
        sess.close()
