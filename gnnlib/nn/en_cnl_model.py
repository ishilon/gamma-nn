"""Builds the HESSnn network.

Summary of available functions:

 # Compute input images and labels for training. If you would like to run
 # evaluations, use inputs() instead.
 inputs, labels = distorted_inputs()

 # Compute inference on the model inputs to make a prediction.
 predictions = inference(inputs)

 # Compute the total loss of the prediction with respect to the labels.
 loss = loss(predictions, labels)

 # Create a graph to run one step of training with respect to the loss.
 train_op = train(loss, global_step)
"""
from gnnlib.nn import train
import tensorflow as tf
import math



def get_meta_consts():
    return {'moving_average_decay': 0.999,      # The decay to use for the moving average
            'num_epochs_per_decay': 2.0,        # Epochs after which learning rate decays.
            'learning_rate_decay_factor': 0.5,  # Learning rate decay factor.
            'initial_learning_rate': 0.0004,     # Initial learning rate value.
            'staircase': False
            }


def en_cnl_218_bn_model( images, sizes, batch_size, tower_name, num_images, is_training_bool, pkeep):
    """Build the 218 combined channel gamma-nn model WITH batch normalization.
        Args:
        images: Images returned from distorted_inputs() or inputs().

        Returns:
        Logits.
    """
    print('\naltaz_channels_model added to graph!')
    print('pkeep rate = ', pkeep)
    # Instantiate all variables using tf.get_variable() instead of
    # tf.Variable() in order to share variables across multiple GPU training runs.
    # If we only ran this model on a single GPU, we could simplify this function
    # by replacing all instances of tf.get_variable() with tf.Variable().

    k = 96  # first convolutional layer output depth (64)
    ll = 96
    m = 192  # third layer (384)
    n = 192
    o = 384
    p = 384
    q = 512
    r = 512
    t = 1024
    u = 256
    v = 128
    w = 16
    x = 128
    y = 512
    z = 64

    pkeep_plh = tf.constant(pkeep, name='pkeep_dropout')
    #is_training_bool = tf.constant(phase, name='is_training', dtype=tf.bool)

    # conv1
    with tf.variable_scope('conv1_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, 4, k], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( images, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv1 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv1, tower_name )

    # conv2
    with tf.variable_scope('conv2_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, k, ll], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv1, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv2 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv2, tower_name )

    # pool1
    pool1 = tf.nn.max_pool( conv2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool1' )

    # conv3
    with tf.variable_scope('conv3_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, ll, m], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool1, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv3 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv3, tower_name )

    # conv4
    with tf.variable_scope('conv4_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, m, n], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv3, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv4 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv4, tower_name )

    # pool2
    pool2 = tf.nn.max_pool( conv4, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool2' )

    # conv5
    with tf.variable_scope('conv5_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, n, o], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool2, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv5 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv5, tower_name )

    # conv6
    with tf.variable_scope('conv6_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, o, p], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv5, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv6 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv6, tower_name )

    # pool2
    pool3 = tf.nn.max_pool( conv6, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool3' )

    # conv7
    with tf.variable_scope('conv7_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, p, q], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool3, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv7 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv7, tower_name )

    # conv8
    with tf.variable_scope('conv8_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[3, 3, q, r], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv7, kernel, [1, stride, stride, 1], padding='SAME' )
        conv_bn =tf.contrib.layers.batch_norm(conv,center=True, scale=True,is_training=is_training_bool)
        conv8 = tf.nn.relu( conv_bn, name=scope.name )
        train.activation_summary( conv8, tower_name )

    reshape = tf.reshape( conv8, [ batch_size, -1 ] )

    # fc1
    with tf.variable_scope('fc1_scope') as scope:
        dim = reshape.get_shape()[1].value
        weights = train.variable_with_weight_decay( 'weights', shape=[dim, t], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [t], tf.constant_initializer(0.1))
        fc1_pre_bn = tf.matmul(reshape, weights) + biases
        fc1_bn =tf.contrib.layers.batch_norm(fc1_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc1 = tf.nn.relu( fc1_bn, name=scope.name )
        train.activation_summary( fc1, tower_name )

    # fc2
    with tf.variable_scope('fc2_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[t, u], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [u], tf.constant_initializer(0.1))
        fc2_pre_bn = tf.matmul(fc1, weights) + biases
        fc2_bn =tf.contrib.layers.batch_norm(fc2_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc2 = tf.nn.relu( fc2_bn, name=scope.name )
        train.activation_summary( fc2, tower_name )

    # fc3
    with tf.variable_scope('fc3_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[u, v], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [v], tf.constant_initializer(0.1))
        fc3_pre_bn = tf.matmul(fc2, weights) + biases
        fc3_bn =tf.contrib.layers.batch_norm(fc3_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc3 = tf.nn.relu( fc3_bn, name=scope.name )
        train.activation_summary( fc3, tower_name )

    # fc4
    with tf.variable_scope('fc4_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[v, w], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [w], tf.constant_initializer(0.1))
        fc4_pre_bn = tf.matmul(fc3, weights) + biases
        fc4_bn =tf.contrib.layers.batch_norm(fc4_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc4 = tf.nn.relu( fc4_bn, name=scope.name )
        train.activation_summary( fc4, tower_name )

    bottle_concat_sizes = tf.concat( [ fc4, sizes ], axis = 1 )

    # fc5
    with tf.variable_scope('fc5_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[w+4, x], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [x], tf.constant_initializer(0.1))
        fc5_pre_bn = tf.matmul(bottle_concat_sizes, weights) + biases
        fc5_bn =tf.contrib.layers.batch_norm(fc5_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc5 = tf.nn.relu( fc5_bn, name=scope.name )
        train.activation_summary( fc5, tower_name )

    # fc6
    with tf.variable_scope('fc6_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[x, y], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [y], tf.constant_initializer(0.1))
        fc6_pre_bn = tf.matmul(fc5, weights) + biases
        fc6_bn =tf.contrib.layers.batch_norm(fc6_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc6 = tf.nn.relu( fc6_bn, name=scope.name )        
        train.activation_summary( fc6, tower_name )

    # fc7
    with tf.variable_scope('fc7_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[y, z], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [z], tf.constant_initializer(0.1))
        fc7_pre_bn = tf.matmul(fc6, weights) + biases
        fc7_bn =tf.contrib.layers.batch_norm(fc7_pre_bn,center=True, scale=True,is_training=is_training_bool)
        fc7 = tf.nn.relu( fc7_bn, name=scope.name )        
        train.activation_summary( fc7, tower_name )

    # linear layer(WX + b),
    # We don't apply softmax here because
    # tf.nn.sparse_softmax_cross_entropy_with_logits accepts the unscaled logits
    # and performs the softmax internally for efficiency.
    with tf.variable_scope('logits_scope'):
        num_classes = 1
        weights = train.variable_with_weight_decay( 'weights', [z, num_classes], stddev=( num_classes / float(z) ), wd=0.0 )
        biases = train.variable_on_cpu( 'biases', [num_classes], tf.constant_initializer(0.0) )
        logits = tf.add( tf.matmul( fc7, weights ), biases, name='output_node' )
        train.activation_summary( logits, tower_name )

    return logits


def en_cnl_8_2_1_rnn_model( images, sizes, batch_size, tower_name, num_images, is_training_bool, pkeep):
    """Build the RNN HESSnn model.
           Args:
           images: Images returned from distorted_inputs() or inputs().

           Returns:
           Logits.
       """
    print('\naltaz_channels_model added to graph!')
    print('pkeep rate = ', pkeep)
    # Instantiate all variables using tf.get_variable() instead of
    # tf.Variable() in order to share variables across multiple GPU training runs.
    # If we only ran this model on a single GPU, we could simplify this function
    # by replacing all instances of tf.get_variable() with tf.Variable().

    k = 96  # first convolutional layer output depth (64)
    l = 96
    m = 192  # third layer (384)
    n = 192
    o = 384
    p = 384
    q = 512
    r = 512
    t = 1024
    u = 512
    v = 256
    w = 4
    x = 256
    y = 1024
    z = 256
    zz = 64

    rnn_NCells = 64

    num_images = tf.cast(num_images, tf.int32)

    # ct1, ct2, ct3, ct4 = tf.unstack(images, axis=4)
    # telescope_sequence = [ct1, ct2, ct3, ct4]
    # stacked_images = tf.convert_to_tensor(telescope_sequence, dtype=tf.float32)
    # batch_sorted_images = tf.transpose(batch_sorted_images, [0, 2, 3, 4, 1])
    # stacked_images=tf.squeeze(stacked_images)
    pkeep_plh = tf.constant(pkeep, name='pkeep_dropout')

    # conv1
    with tf.variable_scope('conv1_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[5, 5, 4, k], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(images, kernel, [1, stride, stride, 1], padding='SAME')
        conv1 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv1, tower_name)

    # conv2
    with tf.variable_scope('conv2_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[5, 5, k, l], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(conv1, kernel, [1, stride, stride, 1], padding='SAME')
        conv2 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv2, tower_name)

        # pool1
    pool1 = tf.nn.max_pool(conv2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool1')

    # conv3
    with tf.variable_scope('conv3_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[3, 3, l, m], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(pool1, kernel, [1, stride, stride, 1], padding='SAME')
        conv3 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv3, tower_name)

    # conv4
    with tf.variable_scope('conv4_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[3, 3, m, n], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(conv3, kernel, [1, stride, stride, 1], padding='SAME')
        conv4 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv4, tower_name)

    # pool2
    pool2 = tf.nn.max_pool(conv4, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool2')

    # conv5
    with tf.variable_scope('conv5_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[3, 3, n, o], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(pool2, kernel, [1, stride, stride, 1], padding='SAME')
        conv5 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv5, tower_name)

    # conv6
    with tf.variable_scope('conv6_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[3, 3, o, p], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(conv5, kernel, [1, stride, stride, 1], padding='SAME')
        conv6 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv6, tower_name)

        # pool2
    pool3 = tf.nn.max_pool(conv6, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool3')

    # conv7
    with tf.variable_scope('conv7_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[3, 3, p, q], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(pool3, kernel, [1, stride, stride, 1], padding='SAME')
        conv7 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv7, tower_name)

    # conv8
    with tf.variable_scope('conv8_scope') as scope:
        kernel = train.variable_with_weight_decay('weights', shape=[3, 3, q, r], stddev=5e-2, wd=0.0)
        stride = 1
        conv = tf.nn.conv2d(conv7, kernel, [1, stride, stride, 1], padding='SAME')
        conv8 = tf.nn.relu(conv, name=scope.name)
        train.activation_summary(conv8, tower_name)

    reshape = tf.reshape(conv8, [batch_size, -1])
    reshape_do = tf.nn.dropout(reshape, pkeep_plh)

    # fc1
    with tf.variable_scope('fc1_scope') as scope:
        dim = reshape_do.get_shape()[1].value
        weights = train.variable_with_weight_decay('weights', shape=[dim, t], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [t], tf.constant_initializer(0.1))
        fc1 = tf.nn.relu(tf.matmul(reshape_do, weights) + biases, name=scope.name)
        train.activation_summary(fc1, tower_name)
        fc1_do = tf.nn.dropout(fc1, pkeep_plh)

    # fc2
    with tf.variable_scope('fc2_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[t, u], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [u], tf.constant_initializer(0.1))
        fc2 = tf.nn.relu(tf.matmul(fc1_do, weights) + biases, name=scope.name)
        train.activation_summary(fc2, tower_name)
        fc2_do = tf.nn.dropout(fc2, pkeep_plh)

    # fc3
    with tf.variable_scope('fc3_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[u, v], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [v], tf.constant_initializer(0.1))
        fc3 = tf.nn.relu(tf.matmul(fc2_do, weights) + biases, name=scope.name)
        train.activation_summary(fc3, tower_name)
        fc3_do = tf.nn.dropout(fc3, pkeep_plh)

    # fc4
    with tf.variable_scope('fc4_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[v, w], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [w], tf.constant_initializer(0.1))
        fc4 = tf.nn.relu(tf.matmul(fc3_do, weights) + biases, name=scope.name)
        train.activation_summary(fc4, tower_name)

    tel1 = tf.concat([tf.reshape(fc4[:,0], [-1, 1]),tf.reshape(sizes[:,0], [-1, 1])], axis = 1)
    tel2 = tf.concat([tf.reshape(fc4[:,1], [-1, 1]),tf.reshape(sizes[:,1], [-1, 1])], axis = 1)
    tel3 = tf.concat([tf.reshape(fc4[:,2], [-1, 1]),tf.reshape(sizes[:,2], [-1, 1])], axis = 1)
    tel4 = tf.concat([tf.reshape(fc4[:,3], [-1, 1]),tf.reshape(sizes[:,3], [-1, 1])], axis = 1)

    telescope_sequence = [tel1, tel2, tel3, tel4]

    # LSTM cell for the RNN
    lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(rnn_NCells)

    # RNN, for outputs > NumTels outputs are neglected and set to zero
    outputs, states = tf.nn.static_rnn(lstm_cell, telescope_sequence, sequence_length=num_images, dtype=tf.float32)

    # Use the last relevant output not outputs[-1] !
    output = tf.transpose(tf.stack(outputs), perm=[1, 0, 2])
    # Query shape.
    max_length = int(output.get_shape()[1])
    num_neurons = int(output.get_shape()[2])
    # Index into flattened array as a workaround.
    index = tf.range(0, batch_size) * max_length + (num_images - 1)
    flat = tf.reshape(output, [-1, num_neurons])
    relevant = tf.gather(flat, index)

    tf.summary.scalar("fc4 raw 1st", tf.reduce_mean(fc4[:,0],axis=0))
    tf.summary.scalar("fc4 raw 2nd", tf.reduce_mean(fc4[:,1],axis=0))
    tf.summary.scalar("fc4 raw 3rd", tf.reduce_mean(fc4[:,2],axis=0))
    tf.summary.scalar("fc4 raw 4th", tf.reduce_mean(fc4[:,3],axis=0))
    tf.summary.scalar("sizes 1st", tf.reduce_mean(sizes[:,0],axis=0))
    tf.summary.scalar("sizes 2nd", tf.reduce_mean(sizes[:,1],axis=0))
    tf.summary.scalar("sizes 3rd", tf.reduce_mean(sizes[:,2],axis=0))
    tf.summary.scalar("sizes 4th", tf.reduce_mean(sizes[:,3],axis=0))
    tf.summary.histogram("sizes 1st", sizes[:,0])
    tf.summary.histogram("sizes 2", sizes[:,1])
    tf.summary.histogram("sizes 3", sizes[:,2])
    tf.summary.histogram("sizes 4", sizes[:,3])

    fc4_size = tf.concat([fc4,sizes],axis=1)

    # fc5
    with tf.variable_scope('fc5_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[rnn_NCells, x], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [x], tf.constant_initializer(0.1))
        fc5 = tf.nn.relu(tf.matmul(relevant, weights) + biases, name=scope.name)
        train.activation_summary(fc5, tower_name)
        fc5_do = tf.nn.dropout(fc5, pkeep_plh)

    # fc6
    with tf.variable_scope('fc6_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[x, y], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [y], tf.constant_initializer(0.1))
        fc6 = tf.nn.relu(tf.matmul(fc5_do, weights) + biases, name=scope.name)
        train.activation_summary(fc6, tower_name)
        fc6_do = tf.nn.dropout(fc6, pkeep_plh)

    # fc7
    with tf.variable_scope('fc7_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[y, z], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [z], tf.constant_initializer(0.1))
        fc7 = tf.nn.relu(tf.matmul(fc6_do, weights) + biases, name=scope.name)
        train.activation_summary(fc7, tower_name)
        fc7_do = tf.nn.dropout(fc7, pkeep_plh)

    # fc8
    with tf.variable_scope('fc8_scope') as scope:
        weights = train.variable_with_weight_decay('weights', shape=[z, zz], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu('biases', [zz], tf.constant_initializer(0.1))
        fc8 = tf.nn.relu(tf.matmul(fc7_do, weights) + biases, name=scope.name)
        train.activation_summary(fc8, tower_name)

    # linear layer(WX + b),
    # We don't apply softmax here because
    # tf.nn.sparse_softmax_cross_entropy_with_logits accepts the unscaled logits
    # and performs the softmax internally for efficiency.
    with tf.variable_scope('logits_scope'):
        num_classes = 1
        weights = train.variable_with_weight_decay('weights', [zz, num_classes], stddev=(num_classes / float(zz)), wd=0.0)
        biases = train.variable_on_cpu('biases', [num_classes], tf.constant_initializer(0.0))
        logits = tf.add(tf.matmul(fc8, weights), biases, name='output_node')
        train.activation_summary(logits, tower_name)

    return logits




def loss( logits, true_labels ):
    """Add L2Loss to all the trainable variables.

    Add summary for "Loss" and "Loss/avg".
    Args:
    logits: Logits from inference().
    labels: Labels from distorted_inputs or inputs(). 1-D tensor
            of shape [batch_size]

    Returns:
    Loss tensor of type float.
    """

    
    true_labels = tf.reshape( true_labels, logits.get_shape().as_list() )

    # MSE showed to perform worse for direction regression --> use absolute error
    '''
    mse = tf.losses.mean_squared_error( labels=true_labels,
                                        predictions=logits,
                                        weights=0.5 )
    tf.add_to_collection( 'losses', mse)
    '''

    abs_err = tf.reduce_mean( tf.abs( true_labels - logits ) )
    tf.add_to_collection( 'losses', abs_err )

    # The total loss is defined as the cross entropy loss plus all of the weight
    # decay terms (L2 loss).
    # get_collection() returns the list of values in the collection with the given name,
    # or an empty list if no value has been added to that collection.
    # The list contains the values in the order under which they were collected.
    # add_n() adds all input tensors in the input list element-wise
    return tf.add_n( tf.get_collection('losses'), name='total_loss' )


def _add_loss_summaries(total_loss):
    """Add summaries for losses in HESSnn model.

    Generates moving average for all losses and associated summaries for
    visualizing the performance of the network.

    Args:
    total_loss: Total loss from loss().
    Returns:
    loss_averages_op: op for generating moving averages of losses.
    """
    # Compute the moving average of all individual losses and the total loss.
    loss_averages = tf.train.ExponentialMovingAverage( 0.9, name='avg' )
    losses = tf.get_collection('losses')
    loss_averages_op = loss_averages.apply( losses + [total_loss] )

    # Attach a scalar summary to all individual losses and the total loss; do the
    # same for the averaged version of the losses.
    for l in losses + [total_loss]:
        # Name each loss as '(raw)' and name the moving average version of the loss
        # as the original loss name.
        tf.summary.scalar( l.op.name + ' (raw)', l )
        tf.summary.scalar( l.op.name, loss_averages.average(l) )

    return loss_averages_op
