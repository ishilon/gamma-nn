"""Builds the GammaNN network.

Summary of available functions:

 # Compute input images and labels for training. If you would like to run
 # evaluations, use inputs() instead.
 inputs, labels = distorted_inputs()

 # Compute inference on the model inputs to make a prediction.
 predictions = inference(inputs)

 # Compute the total loss of the prediction with respect to the labels.
 loss = loss(predictions, labels)

 # Create a graph to run one step of training with respect to the loss.
 train_op = train(loss, global_step)
"""
import tensorflow as tf
from gnnlib.nn import train


def get_meta_consts():
    return {'moving_average_decay': 0.999,      # The decay to use for the moving average
            'num_epochs_per_decay': 9.0,        # Epochs after which learning rate decays.
            'learning_rate_decay_factor': 0.5,  # Learning rate decay factor.
            'initial_learning_rate': 0.0003     # Initial learning rate value.
            }


def alt_model( images, batch_size, tower_name, pkeep ):
    """Build the GammaNN model.

    Args:
    images: Images returned from distorted_inputs() or inputs().

    Returns:
    Logits.
    """
    # Instantiate all variables using tf.get_variable() instead of
    # tf.Variable() in order to share variables across multiple GPU training runs.
    # If we only ran this model on a single GPU, we could simplify this function
    # by replacing all instances of tf.get_variable() with tf.Variable().
    print( '\nalt_model added to graph!' )
    k = 128  # first convolutional layer output depth (64)
    p = 196  # second convolutional layer output depth (64)
    r = 294
    q = 356
    w = 356
    m = 1152  # third layer (384)
    n = 576
    o = 384  # fully connected layer (192)

    pkeep_plh = tf.constant( pkeep, name='pkeep_dropout' )

    # conv1
    with tf.variable_scope('conv1_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[5, 5, 1, k], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( images, kernel, [1, stride, stride, 1], padding='SAME' )
        biases = train.variable_on_cpu( 'biases', [k], tf.constant_initializer(0.0) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv1 = tf.nn.relu( pre_activation, name=scope.name )
        train.activation_summary( conv1, tower_name )

    # pool1
    pool1 = tf.nn.max_pool( conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1],
                            padding='SAME', name='pool1' )

    # conv2
    with tf.variable_scope('conv2_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[5, 5, k, p], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d(pool1, kernel, [1, stride, stride, 1], padding='SAME')
        biases = train.variable_on_cpu( 'biases', [p], tf.constant_initializer(0.1) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv2 = tf.nn.relu( pre_activation, name=scope.name )
        train.activation_summary( conv2, tower_name )

    # pool2
    pool2 = tf.nn.max_pool( conv2, ksize=[1, 3, 3, 1],
                            strides=[1, 2, 2, 1], padding='SAME', name='pool2' )

    # conv3
    with tf.variable_scope( 'conv3_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, p, r ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool2, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        biases = train.variable_on_cpu( 'biases', [ r ], tf.constant_initializer( 0.1 ) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv3 = tf.nn.relu( pre_activation, name = scope.name )
        train.activation_summary( conv3, tower_name )

    # pool3
    pool3 = tf.nn.max_pool( conv3, ksize = [ 1, 3, 3, 1 ],
                            strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool3' )

    # conv4
    with tf.variable_scope( 'conv4_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, r, q ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool3, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        biases = train.variable_on_cpu( 'biases', [ q ], tf.constant_initializer( 0.1 ) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv4 = tf.nn.relu( pre_activation, name = scope.name )
        train.activation_summary( conv4, tower_name )

    pool4 = tf.nn.max_pool( conv4, ksize = [ 1, 3, 3, 1 ],
                            strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool3' )

    # conv5
    with tf.variable_scope( 'conv5_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, q, w ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool4, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        biases = train.variable_on_cpu( 'biases', [ w ], tf.constant_initializer( 0.1 ) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv5 = tf.nn.relu( pre_activation, name = scope.name )
        train.activation_summary( conv5, tower_name )

    pool_last = tf.nn.max_pool( conv5, ksize = [ 1, 3, 3, 1 ],
                                strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool_last' )

    # fc1
    with tf.variable_scope('fc1_scope') as scope:
        # Move everything into depth so we can perform a single matrix multiply.
        reshape = tf.reshape( pool_last, [ batch_size, -1 ] )
        dim = reshape.get_shape()[1].value
        weights = train.variable_with_weight_decay('weights', shape=[dim, m], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu( 'biases', [m], tf.constant_initializer(0.1) )
        fc1 = tf.nn.relu( tf.matmul( reshape, weights ) + biases, name=scope.name )
        train.activation_summary( fc1, tower_name )
        fc1_do = tf.nn.dropout( fc1, pkeep_plh )

    # fc2
    with tf.variable_scope('fc2_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[m, n], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [n], tf.constant_initializer(0.1))
        fc2 = tf.nn.relu( tf.matmul(fc1_do, weights) + biases, name=scope.name )
        train.activation_summary( fc2, tower_name )
        fc2_do = tf.nn.dropout( fc2, pkeep_plh )

    # fc3
    with tf.variable_scope('fc3_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[n, o], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [o], tf.constant_initializer(0.1))
        fc3 = tf.nn.relu( tf.matmul(fc2_do, weights) + biases, name=scope.name )
        train.activation_summary( fc3, tower_name )
        fc3_do = tf.nn.dropout( fc3, pkeep_plh )

    # linear layer(WX + b),
    # We don't apply softmax here because
    # tf.nn.sparse_softmax_cross_entropy_with_logits accepts the unscaled logits
    # and performs the softmax internally for efficiency.
    with tf.variable_scope('logits_scope'):
        weights = train.variable_with_weight_decay( 'weights', [o, 1], stddev=( 1 / float(o) ), wd=0.0 )
        biases = train.variable_on_cpu( 'biases', [1], tf.constant_initializer(0.0) )
        logits = tf.add( tf.matmul( fc3_do, weights ), biases, name='output_node' )
        train.activation_summary( logits, tower_name )

    return logits


def az_model( images, batch_size, tower_name, pkeep ):
    """Build the GammaNN model.
    Args:
    images: Images returned from distorted_inputs() or inputs().

    Returns:
    Logits.
    """
    # Instantiate all variables using tf.get_variable() instead of
    # tf.Variable() in order to share variables across multiple GPU training runs.
    # If we only ran this model on a single GPU, we could simplify this function
    # by replacing all instances of tf.get_variable() with tf.Variable().
    print( '\naz_model added to graph!' )
    k = 128   # first convolutional layer output depth (64)
    p = 196  # second convolutional layer output depth (64)
    r = 294
    q = 356
    w = 356
    m = 1152  # third layer (384)
    n = 576
    o = 384  # fully connected layer (192)

    pkeep_plh = tf.constant(pkeep, name='pkeep_dropout')

    # conv1
    with tf.variable_scope('conv1_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[5, 5, 1, k], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d( images, kernel, [1, stride, stride, 1], padding='SAME' )
        biases = train.variable_on_cpu( 'biases', [k], tf.constant_initializer(0.0) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv1 = tf.nn.relu( pre_activation, name=scope.name )
        train.activation_summary( conv1, tower_name )

    # pool1
    pool1 = tf.nn.max_pool( conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1],
                            padding='SAME', name='pool1' )

    # norm1
    # norm1 = tf.nn.lrn( pool1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75, name='norm1' )

    # conv2
    with tf.variable_scope('conv2_scope') as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape=[5, 5, k, p], stddev=5e-2, wd=0.0 )
        stride = 1
        conv = tf.nn.conv2d(pool1, kernel, [1, stride, stride, 1], padding='SAME')
        biases = train.variable_on_cpu( 'biases', [p], tf.constant_initializer(0.1) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv2 = tf.nn.relu( pre_activation, name=scope.name )
        train.activation_summary( conv2, tower_name )

    # norm2
    # norm2 = tf.nn.lrn( conv2, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75, name='norm2' )
    # pool2
    pool2 = tf.nn.max_pool( conv2, ksize=[1, 3, 3, 1],
                            strides=[1, 2, 2, 1], padding='SAME', name='pool2' )

    # conv3
    with tf.variable_scope( 'conv3_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, p, r ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool2, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        biases = train.variable_on_cpu( 'biases', [ r ], tf.constant_initializer( 0.1 ) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv3 = tf.nn.relu( pre_activation, name = scope.name )
        train.activation_summary( conv3, tower_name )

    # norm3
    # norm3 = tf.nn.lrn( conv3, 4, bias = 1.0, alpha = 0.001 / 9.0, beta = 0.75, name = 'norm3' )
    # pool3
    pool3 = tf.nn.max_pool( conv3, ksize = [ 1, 3, 3, 1 ],
                            strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool3' )

    # conv4
    with tf.variable_scope( 'conv4_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, r, q ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool3, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        biases = train.variable_on_cpu( 'biases', [ q ], tf.constant_initializer( 0.1 ) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv4 = tf.nn.relu( pre_activation, name = scope.name )
        train.activation_summary( conv4, tower_name )

    pool4 = tf.nn.max_pool( conv4, ksize = [ 1, 3, 3, 1 ],
                            strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool3' )

    # conv5
    with tf.variable_scope( 'conv5_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, q, w ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool4, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        biases = train.variable_on_cpu( 'biases', [ w ], tf.constant_initializer( 0.1 ) )
        pre_activation = tf.nn.bias_add( conv, biases )
        conv5 = tf.nn.relu( pre_activation, name = scope.name )
        train.activation_summary( conv5, tower_name )

    pool_last = tf.nn.max_pool( conv5, ksize = [ 1, 3, 3, 1 ],
                                strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool_last' )

    # fc1
    with tf.variable_scope('fc1_scope') as scope:
        # Move everything into depth so we can perform a single matrix multiply.
        reshape = tf.reshape( pool_last, [ batch_size, -1 ] )
        dim = reshape.get_shape()[1].value
        weights = train.variable_with_weight_decay('weights', shape=[dim, m], stddev=0.04, wd=0.004)
        biases = train.variable_on_cpu( 'biases', [m], tf.constant_initializer(0.1) )
        fc1 = tf.nn.relu( tf.matmul( reshape, weights ) + biases, name=scope.name )
        train.activation_summary( fc1, tower_name )
        fc1_do = tf.nn.dropout( fc1, pkeep_plh )

    # fc2
    with tf.variable_scope('fc2_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[m, n], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [n], tf.constant_initializer(0.1))
        fc2 = tf.nn.relu( tf.matmul(fc1_do, weights) + biases, name=scope.name )
        train.activation_summary( fc2, tower_name )
        fc2_do = tf.nn.dropout( fc2, pkeep_plh )

    # fc3
    with tf.variable_scope('fc3_scope') as scope:
        weights = train.variable_with_weight_decay( 'weights', shape=[n, o], stddev=0.04, wd=0.004 )
        biases = train.variable_on_cpu('biases', [o], tf.constant_initializer(0.1))
        fc3 = tf.nn.relu( tf.matmul(fc2_do, weights) + biases, name=scope.name )
        train.activation_summary( fc3, tower_name )
        fc3_do = tf.nn.dropout( fc3, pkeep_plh )

    # linear layer(WX + b),
    # We don't apply softmax here because
    # tf.nn.sparse_softmax_cross_entropy_with_logits accepts the unscaled logits
    # and performs the softmax internally for efficiency.
    with tf.variable_scope('logits_scope'):
        weights = train.variable_with_weight_decay( 'weights', [o, 1], stddev=( 1 / float(o) ), wd=0.0 )
        biases = train.variable_on_cpu( 'biases', [1], tf.constant_initializer(0.0) )
        logits = tf.add( tf.matmul( fc3_do, weights ), biases, name='output_node' )
        train.activation_summary( logits, tower_name )

    return logits


def regression_loss( logits, true_labels ):
    """Add L2Loss to all the trainable variables.
    Add summary for "Loss" and "Loss/avg".
    Args:
    logits: Logits from inference().
    labels: Labels from distorted_inputs or inputs(). 1-D tensor
            of shape [batch_size]
    Returns:
    Loss tensor of type float.
    """
    true_labels = tf.reshape( true_labels, logits.get_shape().as_list() )

    mse = tf.losses.mean_squared_error( labels=true_labels,
                                        predictions=logits,
                                        weights=0.5 )
    tf.add_to_collection( 'losses', mse )

    # The total loss is defined as the cross entropy loss plus all of the weight
    # decay terms (L2 loss).
    # get_collection() returns the list of values in the collection with the given name,
    # or an empty list if no value has been added to that collection.
    # The list contains the values in the order under which they were collected.
    # add_n() adds all input tensors in the input list element-wise
    return tf.add_n( tf.get_collection('losses'), name='total_loss' )


def _add_loss_summaries(total_loss):
    """Add summaries for losses in GammaNN model.

    Generates moving average for all losses and associated summaries for
    visualizing the performance of the network.

    Args:
    total_loss: Total loss from loss().
    Returns:
    loss_averages_op: op for generating moving averages of losses.
    """
    # Compute the moving average of all individual losses and the total loss.
    loss_averages = tf.train.ExponentialMovingAverage( 0.9, name='avg' )
    losses = tf.get_collection('losses')
    loss_averages_op = loss_averages.apply( losses + [total_loss] )

    # Attach a scalar summary to all individual losses and the total loss; do the
    # same for the averaged version of the losses.
    for l in losses + [total_loss]:
        # Name each loss as '(raw)' and name the moving average version of the loss
        # as the original loss name.
        tf.summary.scalar( l.op.name + ' (raw)', l )
        tf.summary.scalar( l.op.name, loss_averages.average(l) )

    return loss_averages_op
