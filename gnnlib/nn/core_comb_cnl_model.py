"""Builds the HESSnn network.

Summary of available functions:

 # Compute input images and labels for training. If you would like to run
 # evaluations, use inputs() instead.
 inputs, labels = distorted_inputs()

 # Compute inference on the model inputs to make a prediction.
 predictions = inference(inputs)

 # Compute the total loss of the prediction with respect to the labels.
 loss = loss(predictions, labels)

 # Create a graph to run one step of training with respect to the loss.
 train_op = train(loss, global_step)
"""
from gnnlib.nn import train
import tensorflow as tf


def get_meta_consts():
    return { 'moving_average_decay': 0.999,  # The decay to use for the moving average
             'num_epochs_per_decay': 1.0,  # Epochs after which learning rate decays.
             'learning_rate_decay_factor': 0.5,  # Learning rate decay factor.
             'initial_learning_rate': 0.0003,  # Initial learning rate value.
             'staircase': True
             }


def core_comb_cnl_218_model( images, batch_size, tower_name, num_images, pkeep ):
    """Build the RNN HESSnn model.
        Args:
        images: Images returned from distorted_inputs() or inputs().

        Returns:
        Logits.
    """
    print( '\ncore_cnl_218_model added to graph!' )
    print( 'pkeep rate = ', pkeep )
    # Instantiate all variables using tf.get_variable() instead of
    # tf.Variable() in order to share variables across multiple GPU training runs.
    # If we only ran this model on a single GPU, we could simplify this function
    # by replacing all instances of tf.get_variable() with tf.Variable().

    k = 96  # first convolutional layer output depth (64)
    l = 96
    m = 192  # third layer (384)
    n = 192
    o = 384
    p = 384
    q = 512
    r = 512
    t = 1024
    u = 256
    v = 64

    # ct1, ct2, ct3, ct4 = tf.unstack(images, axis=4)
    # telescope_sequence = [ct1, ct2, ct3, ct4]
    # stacked_images = tf.convert_to_tensor(telescope_sequence, dtype=tf.float32)
    # batch_sorted_images = tf.transpose(batch_sorted_images, [0, 2, 3, 4, 1])
    # stacked_images=tf.squeeze(stacked_images)
    pkeep_plh = tf.constant( pkeep, name = 'pkeep_dropout' )

    # conv1
    with tf.variable_scope( 'conv1_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, 4, k ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( images, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv1 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv1, tower_name )

    # conv2
    with tf.variable_scope( 'conv2_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, k, l ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv1, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv2 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv2, tower_name )

        # pool1
    pool1 = tf.nn.max_pool( conv2, ksize = [ 1, 3, 3, 1 ], strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool1' )

    # conv3
    with tf.variable_scope( 'conv3_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, l, m ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool1, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv3 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv3, tower_name )

    # conv4
    with tf.variable_scope( 'conv4_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, m, n ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv3, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv4 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv4, tower_name )

    # pool2
    pool2 = tf.nn.max_pool( conv4, ksize = [ 1, 3, 3, 1 ], strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool2' )

    # conv5
    with tf.variable_scope( 'conv5_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, n, o ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool2, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv5 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv5, tower_name )

    # conv6
    with tf.variable_scope( 'conv6_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, o, p ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv5, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv6 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv6, tower_name )

        # pool2
    pool3 = tf.nn.max_pool( conv6, ksize = [ 1, 3, 3, 1 ], strides = [ 1, 2, 2, 1 ], padding = 'SAME', name = 'pool3' )

    # conv7
    with tf.variable_scope( 'conv7_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, p, q ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( pool3, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv7 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv7, tower_name )

    # conv8
    with tf.variable_scope( 'conv8_scope' ) as scope:
        kernel = train.variable_with_weight_decay( 'weights', shape = [ 5, 5, q, r ], stddev = 5e-2, wd = 0.0 )
        stride = 1
        conv = tf.nn.conv2d( conv7, kernel, [ 1, stride, stride, 1 ], padding = 'SAME' )
        conv8 = tf.nn.relu( conv, name = scope.name )
        train.activation_summary( conv8, tower_name )

    reshape = tf.reshape( conv8, [ batch_size, -1 ] )
    reshape_do = tf.nn.dropout( reshape, pkeep_plh )

    # fc1
    with tf.variable_scope( 'fc1_scope' ) as scope:
        dim = reshape_do.get_shape()[ 1 ].value
        weights = train.variable_with_weight_decay( 'weights', shape = [ dim, t ], stddev = 0.04, wd = 0.004 )
        biases = train.variable_on_cpu( 'biases', [ t ], tf.constant_initializer( 0.1 ) )
        fc1 = tf.nn.relu( tf.matmul( reshape_do, weights ) + biases, name = scope.name )
        train.activation_summary( fc1, tower_name )

    # fc2
    with tf.variable_scope( 'fc2_scope' ) as scope:
        weights = train.variable_with_weight_decay( 'weights', shape = [ t, u ], stddev = 0.04, wd = 0.004 )
        biases = train.variable_on_cpu( 'biases', [ u ], tf.constant_initializer( 0.1 ) )
        fc2 = tf.nn.relu( tf.matmul( fc1, weights ) + biases, name = scope.name )
        train.activation_summary( fc2, tower_name )

    # fc3
    with tf.variable_scope( 'fc3_scope' ) as scope:
        weights = train.variable_with_weight_decay( 'weights', shape = [ u, v ], stddev = 0.04, wd = 0.004 )
        biases = train.variable_on_cpu( 'biases', [ v ], tf.constant_initializer( 0.1 ) )
        fc3 = tf.nn.relu( tf.matmul( fc2, weights ) + biases, name = scope.name )
        train.activation_summary( fc3, tower_name )

    # linear layer(WX + b),
    # We don't apply softmax here because
    # tf.nn.sparse_softmax_cross_entropy_with_logits accepts the unscaled logits
    # and performs the softmax internally for efficiency.
    with tf.variable_scope( 'logits_scope' ):
        num_classes = 2
        weights = train.variable_with_weight_decay( 'weights', [ v, num_classes ], stddev = (num_classes / float( v )), wd = 0.0 )
        biases = train.variable_on_cpu( 'biases', [ num_classes ], tf.constant_initializer( 0.0 ) )
        logits = tf.add( tf.matmul( fc3, weights ), biases, name = 'output_node' )
        train.activation_summary( logits, tower_name )

    return logits


def loss( logits, true_labels ):
    """Add L2Loss to all the trainable variables.

    Add summary for "Loss" and "Loss/avg".
    Args:
    logits: Logits from inference().
    labels: Labels from distorted_inputs or inputs(). 1-D tensor
            of shape [batch_size]

    Returns:
    Loss tensor of type float.
    """
    true_labels = tf.reshape( true_labels, logits.get_shape().as_list() )

    mse = tf.losses.mean_squared_error( labels = true_labels,
                                        predictions = logits,
                                        weights = 0.5 )
    tf.add_to_collection( 'losses', mse )

    # The total loss is defined as the cross entropy loss plus all of the weight
    # decay terms (L2 loss).
    # get_collection() returns the list of values in the collection with the given name,
    # or an empty list if no value has been added to that collection.
    # The list contains the values in the order under which they were collected.
    # add_n() adds all input tensors in the input list element-wise
    return tf.add_n( tf.get_collection( 'losses' ), name = 'total_loss' )


def _add_loss_summaries( total_loss ):
    """Add summaries for losses in HESSnn model.

    Generates moving average for all losses and associated summaries for
    visualizing the performance of the network.

    Args:
    total_loss: Total loss from loss().
    Returns:
    loss_averages_op: op for generating moving averages of losses.
    """
    # Compute the moving average of all individual losses and the total loss.
    loss_averages = tf.train.ExponentialMovingAverage( 0.9, name = 'avg' )
    losses = tf.get_collection( 'losses' )
    loss_averages_op = loss_averages.apply( losses + [ total_loss ] )

    # Attach a scalar summary to all individual losses and the total loss; do the
    # same for the averaged version of the losses.
    for l in losses + [ total_loss ]:
        # Name each loss as '(raw)' and name the moving average version of the loss
        # as the original loss name.
        tf.summary.scalar( l.op.name + ' (raw)', l )
        tf.summary.scalar( l.op.name, loss_averages.average( l ) )

    return loss_averages_op
