"""
This is a collection of classes to create input files for cnns from hess MCs and data in hdf5 format.
"""
import h5py
import numpy as np

from gnnlib.utils import utils


class H5DST:
    """
    An h5_file object has all values needed to create image data files for the
    training procedures which are read from .h5 files.
    """
    def __init__(self, h5_dst):
        with h5py.File(h5_dst, 'r') as dst:

            # Getting events parameters from file
            events_list         = dst['Events_List']

            run_id_array = np.ones_like( events_list['Event_ID'] ) * dst['Run_Data']['Run_ID'][0]
            tmp_uid = np.c_[ run_id_array, events_list['Bunch_ID'], events_list['Event_ID'] ]
            self.uid = [ "-".join( item ) for item in tmp_uid.astype( str ) ]

            self.tels_in_event  = events_list['Tels_in_event']

            self.energy         = events_list['Energy']

            self.CT1_images     = events_list['CT1_image']
            self.CT1_pixel_id   = events_list['CT1_pixel_ID']
            self.CT2_images     = events_list['CT2_image']
            self.CT2_pixel_id   = events_list['CT2_pixel_ID']
            self.CT3_images     = events_list['CT3_image']
            self.CT3_pixel_id   = events_list['CT3_pixel_ID']
            self.CT4_images     = events_list['CT4_image']
            self.CT4_pixel_id   = events_list['CT4_pixel_ID']
            self.CT5_images     = events_list['CT5_image']
            self.CT5_pixel_id   = events_list['CT5_pixel_ID']

            dst.close()

            # mc_run_data = dst['MC_Run_Data']
            # self.spectral_index = mc_run_data['Spectral_index']
            # self.dst_max_energy = mc_run_data['Max_energy']
            # self.dst_min_energy = mc_run_data['Min_energy']

    def __exit__(self, exc_type, exc_value, traceback):

        self.uid = None

        self.tels_in_event = None

        self.energy = None
        self.primary_id = None

        self.CT1_images = None
        self.CT1_pixel_id = None
        self.CT2_images = None
        self.CT2_pixel_id = None
        self.CT3_images = None
        self.CT3_pixel_id = None
        self.CT4_images = None
        self.CT4_pixel_id = None
        self.CT5_images = None
        self.CT5_pixel_id = None

    def __enter__( self ):
        return self

    # The commented lines below are examples for using getters and setters in python
    # For more see: http://www.python-course.eu/python3_properties.php and
    # http://stackoverflow.com/questions/2627002/whats-the-pythonic-way-to-use-getters-and-setters
    # @property
    # def x(self):
    #     return self.__x

    # @x.setter
    # def x(self, x):
    #     if x < 0:
    #         self.__x = 0
    #     elif x > 1000:
    #         self.__x = 1000
    #     else:
    #         self.__x = x

    @property
    def images(self):
        """
        Transpose so the shape of images is (num_of_events, num_of_tels). I think it makes the code more consistent,
        because the indices for an H5DST object attributes are given in the same order and
        with images[event_index] all the telescope images are given as a single array - Idan
        :return:
        """
        return np.array( [self.CT1_images, self.CT2_images,
                          self.CT3_images, self.CT4_images, self.CT5_images] ).T

    @property
    def pixel_ids(self):
        """
        The pixel_ids range from 0 to 959 in the MC DSTs
        """
        return np.array( [self.CT1_pixel_id, self.CT2_pixel_id,
                          self.CT3_pixel_id, self.CT4_pixel_id, self.CT5_pixel_id] ).T

    def event_images( self, event_index ):
        return np.array( [ self.CT1_images[ event_index ], self.CT2_images[ event_index ],
                           self.CT3_images[ event_index ], self.CT4_images[ event_index ],
                           self.CT5_images[ event_index ] ] )

    def event_pixel_ids( self, event_index ):
        return np.array( [ self.CT1_pixel_id[ event_index ], self.CT2_pixel_id[ event_index ],
                           self.CT3_pixel_id[ event_index ], self.CT4_pixel_id[ event_index ] ] )  # , self.CT5_pixel_id[ event_index ]] )
