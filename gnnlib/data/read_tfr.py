import os
import tensorflow as tf


def _read_example( filename_queue ):
    """Read an example from a tfrecord file and returns a decoded image and label
    """
    # Uncomment to use compressed tfr
    options = tf.python_io.TFRecordOptions(tf.python_io.TFRecordCompressionType.GZIP)
    # Reader for compressed tfrs
    reader = tf.TFRecordReader(options=options)
    # Reader for uncompressed files
    # reader = tf.TFRecordReader()

    _, serialized_example = reader.read( filename_queue )

    # label and image are stored as bytes but could be stored as
    # int64 or float64 values in a serialized tf.Example protobuf.
    tfrecord_features = tf.parse_single_example( serialized_example,
                                                 features = {'combined_image': tf.FixedLenFeature( [], tf.string ),
                                                             'uid': tf.FixedLenFeature( [], tf.string ),
                                                             'type': tf.FixedLenFeature( [], tf.int64 ),
                                                             'num_images': tf.FixedLenFeature( [], tf.int64 ),
                                                             'energy': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct2_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct3_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct4_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct1_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'src_alt': tf.FixedLenFeature( [], tf.float32 ),
                                                             'src_az': tf.FixedLenFeature( [], tf.float32 ),
                                                             'obs_alt': tf.FixedLenFeature( [], tf.float32 ),
                                                             'obs_az': tf.FixedLenFeature( [], tf.float32 ),
                                                             'obs_time': tf.FixedLenFeature( [], tf.string ),
                                                             'core_x': tf.FixedLenFeature( [], tf.float32 ),
                                                             'core_y': tf.FixedLenFeature( [], tf.float32 ),
                                                             'tels_in_event': tf.FixedLenFeature( [5], tf.int64 )
                                                             },
                                                 name = 'features' )
    return tfrecord_features


def get_tfr_features( ds_dir, eval_data=None, data_type='mc' ):
    """Construct input for HESSnn evaluation using the Reader ops.

    Args:
    eval_data: bool, indicating if one should use the train or eval data set.
    ds_dir: Path to the HESSnn dataset directory.
    batch_size: Number of images per batch.

    Returns:
    images: Images. 4D tensor of [batch_size, IMAGE_SIZE, IMAGE_SIZE, 1] size.
    labels: Labels. 1D tensor of [batch_size] size.
    energies: Event energies. 1D tensor of [batch_size] size.
    """
    if data_type == 'mc':
        if eval_data:
            print( '\nReading {0} data for model evaluation.'.format( eval_data ) )
            # Read the training file
            filenames = [ os.path.join( ds_dir + file ) for file in os.listdir( ds_dir ) if ('{0}_file_'.format(eval_data) in file) ]
        else:
            print( '\nReading training set data for training.' )
            filenames = [ os.path.join( ds_dir + file ) for file in os.listdir( ds_dir ) if 'train_file_' in file ]
    else:
        filenames = [ os.path.join( ds_dir + '/' + file ) for file in os.listdir( ds_dir ) if '.tfrecord' in file ]

    for f in filenames:
        if not tf.gfile.Exists(f):
            raise ValueError('Failed to find file: ' + f)

    # Create a queue that produces the filenames to be read.
    filename_queue = tf.train.string_input_producer( filenames, shuffle=False )

    tfrecord_features = _read_example( filename_queue )

    return tfrecord_features


def handle_image( image, args, depth=1 ):
    # the image tensor is flattened out, so we have to reconstruct the shape
    image = tf.reshape( image, [ args.hess1_resolution, args.hess1_resolution, depth ] )
    # Convert to [-6, 6] floats.
    # In the MCs, the maximum p.e. per pixel is 25000
    if args.scale_images == 'std':
        # Subtract off the mean and divide by the variance of the pixels.
        image = tf.image.per_image_standardization( image )
    elif args.scale_images == 'normalize':
        print( 'PLEASE VERIFY MAXIMUM INTENSITY!!!' )
        image = tf.subtract( tf.multiply( image, 0.01 ), 6.0 )
        # image = tf.cast( image, tf.float32 )
    return image


def get_test_example( tfrecord_features ):
    # Convert from string tensor to 'dtype' tensor (float64 in this case).
    uid = tfrecord_features['uid']
    particle = tfrecord_features['type']
    energy = tfrecord_features['energy']
    ct1_loc_dis = tfrecord_features['ct1_loc_dis']
    ct2_loc_dis = tfrecord_features['ct2_loc_dis']
    ct3_loc_dis = tfrecord_features['ct3_loc_dis']
    ct4_loc_dis = tfrecord_features['ct4_loc_dis']
    ct1_length = tfrecord_features['ct1_length']
    ct2_length = tfrecord_features['ct2_length']
    ct3_length = tfrecord_features['ct3_length']
    ct4_length = tfrecord_features['ct4_length']
    ct1_width = tfrecord_features['ct1_width']
    ct2_width = tfrecord_features['ct2_width']
    ct3_width = tfrecord_features['ct3_width']
    ct4_width = tfrecord_features['ct4_width']
    ct1_size = tfrecord_features['ct1_size']
    ct2_size = tfrecord_features['ct2_size']
    ct3_size = tfrecord_features['ct3_size']
    ct4_size = tfrecord_features['ct4_size']
    src_alt = tfrecord_features['src_alt']
    src_az = tfrecord_features['src_az']
    obs_alt = tfrecord_features['obs_alt']
    obs_az = tfrecord_features['obs_az']
    obs_time = tfrecord_features['obs_time']
    core_x = tfrecord_features['core_x']
    core_y = tfrecord_features['core_y']

    return (uid, particle, energy,  # 0, 1, 2
            ct1_loc_dis, ct2_loc_dis, ct3_loc_dis, ct4_loc_dis,  # 3, 4, 5, 6
            ct1_length, ct2_length, ct3_length, ct4_length,  # 7, 8, 9, 10
            ct1_width, ct2_width, ct3_width, ct4_width,  # 11, 12, 13, 14
            ct1_size, ct2_size, ct3_size, ct4_size,  # 15, 16, 17, 18
            src_alt, src_az, obs_alt, obs_az,  # 19, 20, 21, 22
            obs_time, core_x, core_y)  # 23, 24, 25
