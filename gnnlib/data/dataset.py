import os
from random import shuffle
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import rotate as im_rot
import tensorflow as tf
from gnnlib.utils import utils
from gnnlib.data import pixel_maps as pixm
from gnnlib.data.img_process import ImageProcess as dst


def get_h5dst_folders_list( args ):
    folder_list = [ ]
    gnn_data = utils.get_env_variable( 'GNN_DATA' )
    meta_folder = gnn_data + '/h5_DSTs/' + args.inst + '/'

    for phase in args.phase:
        folder_list.append( os.path.join( meta_folder, phase + '/' ) )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        for j in range( len(args.dtype) ):
            if j == 0:
                folder_list[ i ] += args.dtype[ j ] + '/'
            else:
                folder_list.append( folder + args.dtype[ j ] + '/' )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        for j in range( len( args.hemi ) ):
            if j == 0:
                folder_list[ i ] += args.hemi[ j ] + '/'
            else:
                folder_list.append( folder + args.hemi[ j ] + '/' )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        for j in range( len( args.zenith ) ):
            if j == 0:
                folder_list[ i ] += args.zenith[ j ] + '/'
            else:
                folder_list.append( folder + args.zenith[ j ] + '/' )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        if 'diffuse' in folder:
            continue
        for j in range( len( args.offset ) ):
            if j == 0:
                folder_list[ i ] += args.offset[ j ] + '/'
            else:
                folder_list.append( folder + args.offset[ j ] + '/' )

    return sorted( folder_list )


def get_dsts_list( folders_list ):
    dsts_list = []
    for folder in folders_list:
        if not os.path.exists( folder ):
            print( 'The folder {0} cannot be found.'.format(folder) )  # User request (by Manu) for this important functionality
            utils.exit_point( 20.0 )
        files = os.listdir( folder )
        for file in files:
            if '.h5' in file:
                dsts_list.append( os.path.join( folder, file ) )
    shuffle(dsts_list)
    return dsts_list


def get_folder_fractions( folders_list, cluster=False ):
    """
    Get the ratios of events to collect from each folder
    :return: a dictionary with the folder names and their fractions
    """
    folder_fractions = { }
    num_folders = len( folders_list )
    if num_folders > 1:
        print( 'You specified {0} "offset" dirs for your dataset'.format( num_folders ) )
        for folder in folders_list:
            if cluster:
                frac = 1
            else:
                key = 'phase'
                frac = float( input( 'What is the ratio of events for folder\n{0}?\n'.format( folder[ folder.find( '/', folder.find( key ) - len(key) ): ] ) ) )
            folder_fractions[ folder ] = frac
        print( 'You requested the following ratios:' )
        sum_frac = sum( list( folder_fractions.values() ) )
        for key, value in folder_fractions.items():
            new_val = value / sum_frac
            print( '{0:80} : {1} (corresponding to {2}%)'.format( key[ key.find( '/', key.find( key ) - len(key) ): ], value, round( new_val * 100, 3 ) ) )
            folder_fractions[ key ] = new_val
        utils.exit_point( 30 )
    else:
        folder_fractions[ folders_list[0] ] = 1

    return folder_fractions


def get_event_params(config_args, file, combined_image, event_index, cam1_x, cam1_y, num_images, src_alt, src_az):
    """
    Create the event parametrs list.
    The parameters order must be consistent with the parameters in write_events_to_tfr()!!!
    :param config_args:
    :param file:
    :param combined_image:
    :param event_index:
    :param cam1_x:
    :param cam1_y:
    :param num_images:
    :param src_alt:
    :param src_az:
    :return: list with event parameters
    """
    ct1_image = file.rebin( file.h5dst.CT1_pixel_id[event_index], file.h5dst.CT1_images[event_index], cam1_x, cam1_y, config_args.hess1_resolution, config_args.sampling )
    ct2_image = file.rebin( file.h5dst.CT2_pixel_id[event_index], file.h5dst.CT2_images[event_index], cam1_x, cam1_y, config_args.hess1_resolution, config_args.sampling )
    ct3_image = file.rebin( file.h5dst.CT3_pixel_id[event_index], file.h5dst.CT3_images[event_index], cam1_x, cam1_y, config_args.hess1_resolution, config_args.sampling )
    ct4_image = file.rebin( file.h5dst.CT4_pixel_id[event_index], file.h5dst.CT4_images[event_index], cam1_x, cam1_y, config_args.hess1_resolution, config_args.sampling )

    primary_id = 0
    if file.h5dst.primary_id[ event_index ] == 101:
        primary_id = 1

    return [combined_image,  # 0
            file.h5dst.uid[ event_index ],  # 1
            primary_id,  # 2
            num_images,  # 3
            file.h5dst.energy[ event_index ],  # 4
            ct1_image,  # 5
            ct2_image,  # 6
            ct3_image,  # 7
            ct4_image,  # 8
            file.h5dst.CT1_loc_dis[ event_index ],  # 9
            file.h5dst.CT2_loc_dis[ event_index ],  # 10
            file.h5dst.CT3_loc_dis[ event_index ],  # 11
            file.h5dst.CT4_loc_dis[ event_index ],  # 12
            file.h5dst.CT1_length[ event_index ],  # 13
            file.h5dst.CT2_length[ event_index ],  # 14
            file.h5dst.CT3_length[ event_index ],  # 15
            file.h5dst.CT4_length[ event_index ],  # 16
            file.h5dst.CT1_width[ event_index ],  # 17
            file.h5dst.CT2_width[ event_index ],  # 18
            file.h5dst.CT3_width[ event_index ],  # 19
            file.h5dst.CT4_width[ event_index ],  # 20
            file.h5dst.CT1_size[ event_index ],  # 21
            file.h5dst.CT2_size[ event_index ],  # 22
            file.h5dst.CT3_size[ event_index ],  # 23
            file.h5dst.CT4_size[ event_index ],  # 24
            src_alt,  # 25
            src_az,  # 26
            file.h5dst.obs_alt,  # 27
            file.h5dst.obs_az,  # 28
            file.h5dst.obs_time[ event_index ],  # 29
            file.h5dst.core_x_gr[ event_index ],  # 30
            file.h5dst.core_y_gr[ event_index ],  # 31
            file.h5dst.tels_in_event[event_index]  # 32
            ]


def write_events_to_tfr( events_list, tfr_name ):
    """
    Dump a list of events to a serialized .tfrecord format.
    Must be consistent with get_event_params()!!!
    :param events_list: list of event images and parameters
    :param tfr_name: name of .tfrecords file
    :return: nothing
    """
    print('{0}.tfrecord'.format(tfr_name))
    writer = tf.python_io.TFRecordWriter( '{0}.tfrecord'.format(tfr_name) )
    for event in events_list:
        time_str = str(event[29])
        example = tf.train.Example( features=tf.train.Features( feature={
            'combined_image': tf.train.Feature( bytes_list=tf.train.BytesList( value=[ event[0].tobytes() ] ) ),
            'uid': tf.train.Feature( bytes_list=tf.train.BytesList( value=[ event[1].encode() ] ) ),
            'type': tf.train.Feature( int64_list=tf.train.Int64List( value=[event[2] ] ) ),
            'num_images': tf.train.Feature( int64_list = tf.train.Int64List( value = [ event[3] ] ) ),
            'energy': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[4] ] ) ),
            'ct1_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[5].tobytes() ] ) ),
            'ct2_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[6].tobytes() ] ) ),
            'ct3_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[7].tobytes() ] ) ),
            'ct4_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[8].tobytes() ] ) ),
            'ct1_loc_dis': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[9] ] ) ),
            'ct2_loc_dis': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[10] ] ) ),
            'ct3_loc_dis': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[11] ] ) ),
            'ct4_loc_dis': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[12] ] ) ),
            'ct1_length': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[13] ] ) ),
            'ct2_length': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[14] ] ) ),
            'ct3_length': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[15] ] ) ),
            'ct4_length': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[16] ] ) ),
            'ct1_width': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[17] ] ) ),
            'ct2_width': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[18] ] ) ),
            'ct3_width': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[19] ] ) ),
            'ct4_width': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[20] ] ) ),
            'ct1_size': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[21] ] ) ),
            'ct2_size': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[22] ] ) ),
            'ct3_size': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[23] ] ) ),
            'ct4_size': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[24] ] ) ),
            'src_alt': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[25] ] ) ),
            'src_az': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[26] ] ) ),
            'obs_alt': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[27] ] ) ),
            'obs_az': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[28] ] ) ),
            'obs_time': tf.train.Feature( bytes_list=tf.train.BytesList( value=[ time_str.encode() ] ) ),
            'core_x': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[30] ] ) ),
            'core_y': tf.train.Feature( float_list=tf.train.FloatList( value=[ event[31] ] ) ),
            'tels_in_event': tf.train.Feature( int64_list=tf.train.Int64List( value= event[32] ))
        } ) )
        writer.write( example.SerializeToString() )
    writer.close()
    print( 'tfrecord writer closed!\n' )


def reject_event( config_args, f, event_index ):
    # Skip events that don't pass cuts, etc.
    if f.h5dst.num_of_images[ event_index ] < 2:
        return 0
    elif f.h5dst.tels_in_event[ event_index ][ 4 ]:
        if f.h5dst.num_of_images[ event_index ] < (config_args.min_h1_images_multiplicity + 1):
            return 0
    # Keep events with events below bound
    offset = f.h5dst.get_offset
    if offset[ event_index ] > config_args.max_offset:
        return 0
    # Keep events that have energy between certain bounds
    if f.h5dst.primary_id[ event_index ] != 101:
        if f.h5dst.energy[ event_index ] < config_args.min_energy or f.h5dst.energy[ event_index ] > config_args.max_energy:
            return 0
    else:
        if f.h5dst.energy[ event_index ] < config_args.min_energy or f.h5dst.energy[ event_index ] > (3 * config_args.max_energy):
            return 0
    # HAP presel cuts
    num_h1_images = 4
    h1_loc_dis = config_args.h1_loc_dis_cut
    h1_size_cut = config_args.h1_size_cut
    if f.h5dst.CT1_loc_dis[ event_index ] > h1_loc_dis or f.h5dst.CT1_loc_dis[ event_index ] < 0 or f.h5dst.CT1_size[ event_index ] < h1_size_cut:
        num_h1_images -= 1
        f.h5dst.tels_in_event[ event_index ][ 0 ] = 0
    if f.h5dst.CT2_loc_dis[ event_index ] > h1_loc_dis or f.h5dst.CT2_loc_dis[ event_index ] < 0 or f.h5dst.CT2_size[ event_index ] < h1_size_cut:
        num_h1_images -= 1
        f.h5dst.tels_in_event[ event_index ][ 1 ] = 0
    if f.h5dst.CT3_loc_dis[ event_index ] > h1_loc_dis or f.h5dst.CT3_loc_dis[ event_index ] < 0 or f.h5dst.CT3_size[ event_index ] < h1_size_cut:
        num_h1_images -= 1
        f.h5dst.tels_in_event[ event_index ][ 2 ] = 0
    if f.h5dst.CT4_loc_dis[ event_index ] > h1_loc_dis or f.h5dst.CT4_loc_dis[ event_index ] < 0 or f.h5dst.CT4_size[ event_index ] < h1_size_cut:
        num_h1_images -= 1
        f.h5dst.tels_in_event[ event_index ][ 3 ] = 0
    del f
    return num_h1_images


def create_dataset_files( config_args, folder_fractions, dsts_list, ds_dir, req, ds_type ):
    print('\n\n******************************************')
    print( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) +
           ': Starting image pre-processing for your {0} dataset'.format(ds_type) )
    print('Number of required events for this sub-set: {0}'.format(req))

    cam1_x, cam1_y = pixm.get_mc_cam1_xy()

    set_num = 1  # sub-set number

    total_collected = 0
    events_list = []  # list to hold events until dumped to dataset file
    folder_count_dict = dict.fromkeys(folder_fractions.copy(), 0)  # dict with values that count the number of events collected from each folder
    folder_req_dict = folder_fractions.copy()  # dict counting the number of required events from each folder
    folder_req_dict.update( (key, int(value * req)) for key, value in folder_req_dict.items() )

    # Check that the folder required events add up to the total required events
    tmp = 0
    min_v = 0
    min_k = 0
    for key, value in folder_req_dict.items():
        tmp += value
        if min_v <= value:
            min_v = value
            min_k = key
    diff = req - tmp
    if diff != 0:
        folder_req_dict[min_k] += diff

    num_of_files = 1  # Loop counter
    for h5_file in dsts_list:
        if total_collected >= req:
            print( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) )
            print('\n\nCollected required number of events for your sub-set!')
            if len(events_list) > 0:
                print( 'Dumping the remaining {0} events to dataset file {1} ({2} events collected so far)'.format( len( events_list ), set_num, total_collected ) )
                shuffle(events_list)
                write_events_to_tfr( events_list, '{0}{1}_file_{2}'.format( ds_dir, ds_type, set_num ) )
                del events_list
            print('The {0} sub-set contains the following number of events and folders:'.format(ds_type))
            for key, value in folder_count_dict.items():
                print( '{0} events collected from folder {1}'.format(value, key) )
            utils.exit_point(20)
            break

        k = h5_file.rfind( '/' )
        h5_folder = h5_file[ :k + 1 ]  # The folder in which the file is saved

        if folder_count_dict[h5_folder] >= folder_req_dict[h5_folder]:
            if config_args.debug:
                print('\nSkipping file: ')
                print(h5_file)
                print('enough events collected from this folder)!')
                print(folder_count_dict)
                print(folder_req_dict)
                print()
            continue

        with dst(h5_file) as f:
            if config_args.verbose:
                print( '-------------------------------------' )
                print( 'Opened h5dst!' )
                if config_args.debug:
                    print(h5_file)
                print( 'Number of events in file {0} = {1}'.format( num_of_files, len(f.h5dst.uid) ) )

            for event_index in range( len(f.h5dst.uid) ):
                num_h1_images = reject_event( config_args, f, event_index )
                if num_h1_images < config_args.min_h1_images_multiplicity:
                    continue

                # Process images of the events that survived
                combined_pixel_ids, combined_intensity = getattr( f, config_args.combine_method )( event_index )
                processed_image = getattr( f, config_args.hess1_sampling )( combined_pixel_ids, combined_intensity, cam1_x, cam1_y,
                                                                            config_args.hess1_resolution, config_args.sampling )
                if config_args.num_image_rotations > 0 and 'pointsource' in h5_file:
                    for _ in range( config_args.num_image_rotations ):
                        ang = np.random.uniform( 0.0, 360.0 )

                        # Get the image center
                        center = (-1, -1)
                        if config_args.hess1_resolution == 100:
                            center = (100 / 2 - 0.5, 97 / 2 - 0.5)
                        elif config_args.hess1_resolution == 64:
                            center = (64 / 2 - 0.5, 62 / 2 - 0.5)

                        # Scale images to [0, 1] for skimage
                        max_proc_image = np.amax( processed_image )
                        processed_image_rot = processed_image / max_proc_image
                        processed_image_rot = im_rot( processed_image_rot, angle = ang, center = center, cval = 0.0, mode = 'constant', clip = True, order = 3,
                                                      preserve_range = True ).astype( np.float32 )
                        processed_image_rot = processed_image_rot * max_proc_image

                        # Calculate the new (rotated source position
                        rot_src_alt, rot_src_az = utils.rodrigues_rotation( f.h5dst.src_alt[ event_index ], f.h5dst.src_az[ event_index ], f.h5dst.obs_alt, f.h5dst.obs_az, -ang )

                        event_params = get_event_params(config_args, f, processed_image_rot, event_index, cam1_x, cam1_y, num_h1_images, rot_src_alt, rot_src_az)
                        events_list.append(event_params)
                        folder_count_dict[ h5_folder ] += 1
                        total_collected += 1

                        # if debug show images
                        if config_args.show_images and event_index % 100 == 0:
                            print( 'obs_alt\tobs_az\tsrc_alt\tsrc_az\trot_src_alt\trot_src_az' )
                            print(f.h5dst.obs_alt, f.h5dst.obs_az, f.h5dst.src_alt[ event_index ], f.h5dst.src_az[ event_index ], rot_src_alt, rot_src_az)

                            offset_orig = utils.great_circle_distance( f.h5dst.src_alt[event_index], f.h5dst.obs_alt,
                                                                       f.h5dst.src_az[event_index], f.h5dst.obs_az )
                            offset_rot = utils.great_circle_distance( rot_src_alt, f.h5dst.obs_alt,
                                                                      rot_src_az, f.h5dst.obs_az )
                            print('offset = ', offset_orig, 'offset_rot = ', offset_rot)
                            print('energy = ', f.h5dst.energy[event_index])
                            # processed_image[ center[0], center[1] ] = max_proc_image
                            # processed_image_rot[ center[0], center[1] ] = max_proc_image
                            fig, ax = plt.subplots( 1, 2, figsize = (16, 16) )
                            ax[0].imshow(processed_image)
                            ax[0].set_title( 'original' )
                            ax[1].imshow( processed_image_rot )
                            ax[1].set_title( 'rotated by {0}'.format(ang) )
                            plt.show()

                        # If the number of events collected % (events per file) == 0, dump events to tfrecord
                        if len( events_list ) % config_args.events_per_file == 0:
                            print( 'Dumping {0} events to dataset file {1} ({2} events collected so far)'.format( len( events_list ), set_num, total_collected ) )
                            shuffle( events_list )
                            write_events_to_tfr( events_list, '{0}{1}_file_{2}'.format( ds_dir, ds_type, set_num ) )
                            events_list = []
                            set_num += 1

                        # If enough events collected from the folder, break the loop over rotations
                        if folder_count_dict[ h5_folder ] >= folder_req_dict[ h5_folder ]:
                            if config_args.debug:
                                print( 'Enough events collected from this folder! Stopping rotations!' )
                                print( folder_count_dict )
                                print( folder_req_dict )
                                print()
                            break
                else:
                    event_params = get_event_params(config_args, f, processed_image, event_index, cam1_x, cam1_y, num_h1_images,
                                                    f.h5dst.src_alt[event_index], f.h5dst.src_az[event_index])
                    events_list.append( event_params )
                    folder_count_dict[ h5_folder ] += 1
                    total_collected += 1

                    if config_args.show_images and event_index % 100 == 0:
                        plt.imshow( processed_image )
                        plt.title( 'Primary ID = {0}'.format(f.h5dst.primary_id[event_index]) )
                        plt.pause(1)

                    # If the number of events collected % (events per file) == 0, dump events to tfrecord
                    if len(events_list) % config_args.events_per_file == 0:
                        print( 'Dumping {0} events to dataset file {1} ({2} events collected so far)'.format( len( events_list ), set_num, total_collected ) )
                        shuffle( events_list )
                        write_events_to_tfr( events_list, '{0}{1}_file_{2}'.format( ds_dir, ds_type, set_num ) )
                        events_list = []
                        set_num += 1

                # If enough events collected from the folder, break the loop over events
                if folder_count_dict[ h5_folder ] >= folder_req_dict[ h5_folder ]:
                    if config_args.debug:
                        print( 'Enough events collected from this folder! Moving to next file!' )
                        print( folder_count_dict )
                        print( folder_req_dict )
                        print( 'De-referencing dst/img_process object!' )
                        print()
                    break

        # delete the already read file from the dsts list and write it to dsts_list.txt
        dsts_list = dsts_list[1:]  # delete the used file from the list of files
        num_of_files += 1

    if not os.path.exists( os.path.join( ds_dir, 'logfiles/' ) ):
        os.mkdir( os.path.join( ds_dir, 'logfiles/' ) )
    with open( os.path.join( ds_dir, 'logfiles/{0}_log.txt'.format(ds_type) ), 'w' ) as logfile:
        logfile.write( '{0} dataset was created at: '.format(ds_type) )
        logfile.write( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) )
        logfile.write( '\n' )
        logfile.write( 'The {0} dataset contains {1} events\n'.format( ds_type, total_collected ) )
        for key, value in folder_count_dict.items():
            logfile.write( 'From folder {0}: {1} events were collected\n'.format(key, value) )

    return dsts_list
