import h5py
from gnnlib.utils import utils

def get_mc_cam1_xy():
    gnn = utils.get_env_variable( 'GNN' )
    maps = gnn + '/file_libraries/pix_maps/pixmap_v1.h5'
    with h5py.File( maps, 'r' ) as mapfile:
        cam1_pixmap = mapfile[ 'cam1_pixmap_mc' ]
        cam1_x = cam1_pixmap[ 0 ]
        cam1_y = cam1_pixmap[ 1 ]
        return cam1_x, cam1_y


def get_mc_cam2_xy():
    gnn = utils.get_env_variable( 'GNN' )
    maps = gnn + '/file_libraries/pix_maps/pixmap_v1.h5'
    with h5py.File( maps, 'r' ) as mapfile:
        cam2_pixmap = mapfile[ 'cam2_pixmap_mc' ]
        cam2_x = cam2_pixmap[0]
        cam2_y = cam2_pixmap[1]
        return cam2_x, cam2_y


def get_real_cam1_xy():
    gnn = utils.get_env_variable( 'GNN' )
    maps = gnn + '/file_libraries/pix_maps/pixmap_v1.h5'
    with h5py.File( maps, 'r' ) as mapfile:
        cam1_pixmap = mapfile[ 'cam1_pixmap_data' ]
        cam1_x = cam1_pixmap[0]
        cam1_y = cam1_pixmap[1]
        return cam1_x, cam1_y
