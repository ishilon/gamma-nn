from scipy.sparse import csr_matrix
import numpy as np
# algebra on polygons, creating vector graphics
import Polygon.Shapes as ps
from Polygon.cPolygon import Polygon
from tqdm import tqdm

from gnnlib.data import dst
from gnnlib.utils import utils


class ImageProcess:
    """
    Class for event image processing methods
    """

    def __init__(self, h5dst):
        self.h5dst = dst.H5DST( h5dst )

    def __exit__(self, exc_type, exc_value, traceback):
        self.h5dst.__exit__(exc_type, exc_value, traceback)

    def __enter__( self ):
        return self

    def tel_image_pixels_norm(self, tel_index, event_index, tel_image):
        """
        Takes the event and telescope ID and returns the pixel IDs and
        normalized intensities, tel_id refers to telescope number 1-5 i.e. CT1-5
        Returns: A list of pixel_ids and A list of normalized intensities for a telescope image
        """
        if tel_image is not None and tel_image.size > 0:
            return self.h5dst.pixel_ids[event_index][tel_index], tel_image / np.amax( tel_image )
        else:
            return None, None

    def add_images( self, event_index, tels_in_event ):
        combined_image = np.zeros( 960 )
        event_images = self.h5dst.event_images( event_index )
        event_pixel_ids = self.h5dst.event_pixel_ids( event_index )

        for tel in tels_in_event[ np.nonzero( tels_in_event ) ]:
            tel_image = event_images[ tel - 1 ]
            pix_in_img = event_pixel_ids[ tel - 1 ]
            combined_image[ pix_in_img ] += tel_image

        combined_pixel_ids = np.nonzero( combined_image )
        return combined_pixel_ids, combined_image[ combined_pixel_ids ]

    # Stack only HESS I images together
    def add_images_14( self, event_index ):
        tels_in_event = np.array( self.h5dst.tels_in_event[ event_index, 0:4 ] )
        return self.add_images( event_index, tels_in_event )

    # Stack all available event images together
    def add_images_15( self, event_index ):
        tels_in_event = np.array( self.h5dst.tels_in_event[ event_index ] )
        return self.add_images( event_index, tels_in_event )

    def rebin(self, pixels, intensities, cam_x, cam_y, resolution=100, sampling=3.17):
        """
        horrible hack to ensure compatibility with existing methods
        """
        image = np.zeros(cam_x.size)
        image[pixels] = intensities

        self.prepare_rebin_matrix(cam_x, cam_y, sampling)
        return pad_to_n_pix( np.reshape( self.rebin_matrix.dot( image ), self.rebin_resolution ).astype( np.float32 ), resolution )

    def prepare_rebin_matrix(self, cam_x, cam_y, scale):
        if hasattr(self, 'rebin_matrix'):
            return

        # Get rebin matrices folder
        gnn = utils.get_env_variable( 'GNN' )
        rebin_dir = gnn + '/file_libraries/rebin_matrices/'

        def calculate_rebin_grid(camera_pixels):
            """
            Helper method to calculate a grid for the new square pixels.

            :param camera_pixels: list of polygons
            :return: square grid coords, square grid resolution
            """
            # get basic geometry
            xmin, xmax, ymin, ymax = poly_list_BBox(camera_pixels)
            asp_box = (xmax - xmin) / (ymax - ymin)
            pixel_count = len(camera_pixels)
            # calculate square grid resolution based on scaling parameter
            xres = (np.rint(scale * np.sqrt(pixel_count * asp_box))).astype(int)
            yres = (np.rint(scale * np.sqrt(pixel_count / asp_box))).astype(int)
            # match the aspect ratio
            asp_grid = xres / yres
            if asp_box > asp_grid:  # box wider than grid
                factor = asp_box / asp_grid
                dy = (ymax - ymin) * (factor - 1)
                ymin -= dy / 2
                ymax += dy / 2
            else:  # box higher than grid
                factor = asp_grid / asp_box
                dx = (xmax - xmin) * (factor - 1)
                xmin -= dx / 2
                xmax += dx / 2

            # setup the square grid
            square_distance = (xmax - xmin) / xres
            square_distance *= 1.000000000000001  # numerical thing
            squares = np.mgrid[xmin:xmax:square_distance, ymin:ymax:square_distance].reshape(2, -1).astype(np.float64)
            # return centers of new pixels, not bottom left corner
            return squares + square_distance / 2, (xres, yres)

        try:
            rebin_matrix = np.load( rebin_dir + 'rebinMatrix%f.npy' % scale)
            self.rebin_resolution = np.load( rebin_dir + 'rebinResolution%f.npy' % scale )
        except FileNotFoundError:
            pixmap = np.array([cam_x, cam_y])
            # create prototype pixel polygon
            pix_distance = pixmap[0, 1] - pixmap[0, 0]
            hexa_radius = pix_distance * np.tan(np.pi / 6)
            pixel = ps.Circle(radius=hexa_radius, points=6)
            pixel_area = pixel.area()

            # create list of all camera pixel polygons
            camera_pixels = []
            for pos in np.transpose(pixmap):
                pix = Polygon(pixel)
                pix.shift(pos[0], pos[1])
                camera_pixels.append(pix)

            # calculate rebin grid on the fly, and save it for later!
            rebin_grid, resolution = calculate_rebin_grid(camera_pixels)
            self.grid = rebin_grid
            self.rebin_resolution = resolution

            print( "Calculating matrix of scale %f for resolution %d %d" % (scale, *self.rebin_resolution, *self.rebin_resolution) )

            square_distance = rebin_grid[1, 1] - rebin_grid[1, 0]

            # create list of all rebin pixel polygons
            square_pixels = []
            for coord in np.transpose(rebin_grid):
                square = ps.Rectangle(square_distance)
                square.shift(coord[0] - square_distance / 2, coord[1] - square_distance / 2)
                square_pixels.append(square)

            rebin_matrix = np.zeros((len(square_pixels), len(camera_pixels)))
            for idx in tqdm(np.ndindex(rebin_matrix.shape), total=rebin_matrix.size, desc="rebin matrix"):
                overlap = square_pixels[idx[0]] & camera_pixels[idx[1]]
                rebin_matrix[idx] = overlap.area() / pixel_area
            np.save( rebin_dir + 'rebinMatrix%f' % scale, rebin_matrix)
            np.save( rebin_dir + 'rebinResolution%f' % scale, resolution)
         # print( "Using rebin of scale %f with resolution %d %d" % (scale, *self.rebin_resolution) )
        self.rebin_matrix = csr_matrix(rebin_matrix)


def poly_list_BBox(polylist):
    """
    Takes a list of polygons and returns the bounding box for the sum of all polygons.
    This is basically the bounding box of the convex hull of all the polygons united.

    :param polylist: list of polygons
    :return: tuple of bound coordinates
    """
    BBs = np.array([np.array(p.boundingBox()) for p in polylist])
    mins = np.amin(BBs, axis=0)
    maxs = np.amax(BBs, axis=0)
    xmin = mins[0]
    xmax = maxs[1]
    ymin = mins[2]
    ymax = maxs[3]
    return xmin, xmax, ymin, ymax

def pad_to_n_pix( arr, resolution ):
    # (64, 62) -> (64, 64)
    # (100, 97) -> (100, 100)
    dx = resolution - arr.shape[0]
    dy = resolution - arr.shape[1]
    return np.lib.pad(arr, ((0, dx), (0, dy)), mode='constant', constant_values=0)