import numpy as np
from scipy.interpolate import griddata


def interpolate(PixGrid_X, PixGrid_Y, intensities, loc_x, loc_y):
   
    Data_Loc = np.zeros((len(loc_x),2))
    for pix in range(0,len(loc_x),1):
            Data_Loc[pix,0] = loc_x[pix]
            Data_Loc[pix,1] = loc_y[pix]

    Image = griddata(Data_Loc,intensities, (PixGrid_Y,PixGrid_X),method='linear', fill_value=0)  

    return Image
    