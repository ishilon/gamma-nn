"""
This is a collection of classes to create input files for cnns from hess MCs and data in hdf5 format.
"""
import h5py
import numpy as np

from gnnlib.utils import utils


class H5DST:
    """
    An h5_file object has all values needed to create image data files for the
    training procedures which are read from .h5 files.
    """
    def __init__(self, h5_dst):
        with h5py.File(h5_dst, 'r') as dst:
            # Getting run data from file
            run_data        = dst['Run_Data']
            self.n_tels     = run_data['NTels'][0]
            self.obs_alt    = run_data['ObsAlt'][0]
            self.obs_az     = run_data['ObsAz'][0]
            # self.duration   = run_data['Duration'][0]

            # Getting events parameters from file
            events_list         = dst['Events_List']

            run_id_array = np.ones_like( events_list['Event_ID'] ) * run_data['Run_ID'][0]
            tmp_uid = np.c_[ run_id_array, events_list['Bunch_ID'], events_list['Event_ID'] ]
            self.uid = [ "-".join( item ) for item in tmp_uid.astype( str ) ]

            self.tels_in_event  = events_list['Tels_in_event']

            self.num_of_images  = events_list['Num_of_images']
            self.energy         = events_list['Energy']
            self.primary_id     = events_list['PrimaryID']
            self.obs_time       = events_list['Time']
            self.src_alt        = events_list['Altitude']
            self.src_az         = events_list['Azimuth']
            self.core_x         = events_list['CoreX']
            self.core_y         = events_list['CoreY']
            self.core_z         = events_list['CoreZ']
            self.core_x_gr      = events_list[ 'CoreX_Ground' ]
            self.core_y_gr      = events_list[ 'CoreY_Ground' ]
            self.xmax           = events_list['XMax']

            self.CT1_loc_dis    = events_list['CT1_loc_dis']
            self.CT1_size       = events_list['CT1_size']
            self.CT1_length     = events_list[ 'CT1_length' ]
            self.CT1_width      = events_list[ 'CT1_width' ]
            self.CT2_loc_dis    = events_list['CT2_loc_dis']
            self.CT2_size       = events_list[ 'CT2_size' ]
            self.CT2_length     = events_list[ 'CT2_length' ]
            self.CT2_width      = events_list[ 'CT2_width' ]
            self.CT3_loc_dis    = events_list['CT3_loc_dis']
            self.CT3_size       = events_list[ 'CT3_size' ]
            self.CT3_length     = events_list[ 'CT3_length' ]
            self.CT3_width      = events_list[ 'CT3_width' ]
            self.CT4_loc_dis    = events_list['CT4_loc_dis']
            self.CT4_size       = events_list[ 'CT4_size' ]
            self.CT4_length     = events_list[ 'CT4_length' ]
            self.CT4_width      = events_list[ 'CT4_width' ]
            self.CT5_loc_dis    = events_list['CT5_loc_dis']
            self.CT5_size       = events_list[ 'CT5_size' ]
            self.CT5_length     = events_list[ 'CT5_length' ]
            self.CT5_width      = events_list[ 'CT5_width' ]

            self.CT1_images     = events_list['CT1_image']
            self.CT1_pixel_id   = events_list['CT1_pixel_ID']
            self.CT2_images     = events_list['CT2_image']
            self.CT2_pixel_id   = events_list['CT2_pixel_ID']
            self.CT3_images     = events_list['CT3_image']
            self.CT3_pixel_id   = events_list['CT3_pixel_ID']
            self.CT4_images     = events_list['CT4_image']
            self.CT4_pixel_id   = events_list['CT4_pixel_ID']
            self.CT5_images     = events_list['CT5_image']
            self.CT5_pixel_id   = events_list['CT5_pixel_ID']

            dst.close()

            # mc_run_data = dst['MC_Run_Data']
            # self.spectral_index = mc_run_data['Spectral_index']
            # self.dst_max_energy = mc_run_data['Max_energy']
            # self.dst_min_energy = mc_run_data['Min_energy']

    def __exit__(self, exc_type, exc_value, traceback):
        # run_data = None
        self.n_tels = None
        self.obs_alt = None
        self.obs_az = None
        # self.duration   = run_data['Duration'][0]

        # Getting events parameters from file
        # events_list = None

        # run_id_array = None
        # tmp_uid = None
        self.uid = None

        self.tels_in_event = None

        self.num_of_images = None
        self.energy = None
        self.primary_id = None
        self.obs_time = None
        self.src_alt = None
        self.src_az = None
        # self.core_x         = None
        # self.core_y         = None
        # self.core_z         = None
        self.core_x_gr = None
        self.core_y_gr = None
        self.xmax = None

        self.CT1_loc_dis = None
        self.CT1_size = None
        self.CT1_length = None
        self.CT1_width = None
        self.CT2_loc_dis = None
        self.CT2_size = None
        self.CT2_length = None
        self.CT2_width = None
        self.CT3_loc_dis = None
        self.CT3_size = None
        self.CT3_length = None
        self.CT3_width = None
        self.CT4_loc_dis = None
        self.CT4_size = None
        self.CT4_length = None
        self.CT4_width = None
        self.CT5_loc_dis = None
        self.CT5_size = None
        self.CT5_length = None
        self.CT5_width = None

        self.CT1_images = None
        self.CT1_pixel_id = None
        self.CT2_images = None
        self.CT2_pixel_id = None
        self.CT3_images = None
        self.CT3_pixel_id = None
        self.CT4_images = None
        self.CT4_pixel_id = None
        self.CT5_images = None
        self.CT5_pixel_id = None

    def __enter__( self ):
        return self

    # The commented lines below are examples for using getters and setters in python
    # For more see: http://www.python-course.eu/python3_properties.php and
    # http://stackoverflow.com/questions/2627002/whats-the-pythonic-way-to-use-getters-and-setters
    # @property
    # def x(self):
    #     return self.__x

    # @x.setter
    # def x(self, x):
    #     if x < 0:
    #         self.__x = 0
    #     elif x > 1000:
    #         self.__x = 1000
    #     else:
    #         self.__x = x

    @property
    def images(self):
        """
        Transpose so the shape of images is (num_of_events, num_of_tels). I think it makes the code more consistent,
        because the indices for an H5DST object attributes are given in the same order and
        with images[event_index] all the telescope images are given as a single array - Idan
        :return:
        """
        return np.array( [self.CT1_images, self.CT2_images,
                          self.CT3_images, self.CT4_images, self.CT5_images] ).T

    @property
    def pixel_ids(self):
        """
        The pixel_ids range from 0 to 959 in the MC DSTs
        """
        return np.array( [self.CT1_pixel_id, self.CT2_pixel_id,
                          self.CT3_pixel_id, self.CT4_pixel_id, self.CT5_pixel_id] ).T

    # get a list off offsets for each event
    @property
    def get_offset(self, angles='degrees'):
        return utils.great_circle_distance( self.src_alt, self.obs_alt, self.src_az, self.obs_az, angles  )

    # Get the average local distance in CT1-4 for a specific event
    def get_hess1_avg_loc_dis(self, event_index):
        tot = 0
        loc_dis_list = [ self.CT1_loc_dis[event_index], self.CT2_loc_dis[event_index],
                         self.CT3_loc_dis[event_index], self.CT4_loc_dis[event_index] ]
        for dis in loc_dis_list:
            if dis >= 0:
                tot += dis
        return tot / self.num_of_images[event_index]

    def get_mean_h1_length(self, event_index):
        tot = 0
        hill_len = [self.CT1_length[event_index], self.CT2_length[ event_index ],
                    self.CT3_length[ event_index ], self.CT4_length[ event_index ]]

        for l in hill_len:
            if l > 0:
                tot += l
        return tot / self.num_of_images[event_index]

    def get_mean_h1_width( self, event_index ):
        tot = 0
        hill_len = [ self.CT1_width[ event_index ], self.CT2_width[ event_index ],
                     self.CT3_width[ event_index ], self.CT4_width[ event_index ] ]

        for w in hill_len:
            if w > 0:
                tot += w
        return tot / self.num_of_images[ event_index ]

    # Get the average local distance in CT1-4
    @property
    def get_hess1_avg_loc_dis_array( self ):
        loc_dis_1 = np.copy( self.CT1_loc_dis )
        loc_dis_2 = np.copy( self.CT2_loc_dis )
        loc_dis_3 = np.copy( self.CT3_loc_dis )
        loc_dis_4 = np.copy( self.CT4_loc_dis )

        # replace -1 with zero, if exists
        loc_dis_1[ loc_dis_1 < 0 ] = 0
        loc_dis_2[ loc_dis_2 < 0 ] = 0
        loc_dis_3[ loc_dis_3 < 0 ] = 0
        loc_dis_4[ loc_dis_4 < 0 ] = 0

        total = loc_dis_1 + loc_dis_2 + loc_dis_3 + loc_dis_4

        return total / self.num_of_images

    def event_images( self, event_index ):
        return np.array( [ self.CT1_images[ event_index ], self.CT2_images[ event_index ],
                           self.CT3_images[ event_index ], self.CT4_images[ event_index ],
                           self.CT5_images[ event_index ] ] )

    def event_pixel_ids( self, event_index ):
        return np.array( [ self.CT1_pixel_id[ event_index ], self.CT2_pixel_id[ event_index ],
                           self.CT3_pixel_id[ event_index ], self.CT4_pixel_id[ event_index ] ] )  # , self.CT5_pixel_id[ event_index ]] )
