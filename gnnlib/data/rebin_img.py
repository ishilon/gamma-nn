from scipy.sparse import csr_matrix
import numpy as np
# algebra on polygons, creating vector graphics
import Polygon.Shapes as ps
from Polygon.cPolygon import Polygon
from tqdm import tqdm

from gnnlib.utils import utils


def add_images( event_pixel_ids, event_images, tels ):
    combined_image = np.zeros( 960 )
    event_images = event_images
    event_pixel_ids = event_pixel_ids

    for tel in tels[ np.nonzero( tels ) ]:
        tel_image = event_images[ tel - 1 ]
        pix_in_img = event_pixel_ids[ tel - 1 ]
        combined_image[ pix_in_img ] += tel_image

    combined_pixel_ids = np.nonzero( combined_image )
    return combined_pixel_ids, combined_image[ combined_pixel_ids ]


def rebin(pixels, intensities, cam_x, rebin_matrix, rebin_resolution, resolution=64):
    """
    horrible hack to ensure compatibility with existing methods
    """
    image = np.zeros(cam_x.size)
    image[pixels] = intensities

    return pad_to_n_pix( np.reshape( rebin_matrix.dot( image ), rebin_resolution ).astype( np.float32 ), resolution )


def prepare_rebin_matrix(cam_x, cam_y, scale, data_type='mc'):
    # Get rebin matrices folder
    gnn = utils.get_env_variable( 'GNN' )
    rebin_dir = gnn + '/file_libraries/rebin_matrices/'

    def calculate_rebin_grid( cam1_pixels ):
        """
        Helper method to calculate a grid for the new square pixels.

        :param cam1_pixels: list of polygons
        :return: square grid coords, square grid resolution
        """
        # get basic geometry
        xmin, xmax, ymin, ymax = poly_list_bbox( cam1_pixels )
        asp_box = (xmax - xmin) / (ymax - ymin)
        pixel_count = len( cam1_pixels )
        # calculate square grid resolution based on scaling parameter
        xres = (np.rint(scale * np.sqrt(pixel_count * asp_box))).astype(int)
        yres = (np.rint(scale * np.sqrt(pixel_count / asp_box))).astype(int)
        # match the aspect ratio
        asp_grid = xres / yres
        if asp_box > asp_grid:  # box wider than grid
            factor = asp_box / asp_grid
            dy = (ymax - ymin) * (factor - 1)
            ymin -= dy / 2
            ymax += dy / 2
        else:  # box higher than grid
            factor = asp_grid / asp_box
            dx = (xmax - xmin) * (factor - 1)
            xmin -= dx / 2
            xmax += dx / 2

        # setup the square grid
        square_dist = (xmax - xmin) / xres  # define square distance
        square_dist *= 1.000000000000001  # numerical thing
        squares = np.mgrid[xmin:xmax:square_dist, ymin:ymax:square_dist].reshape(2, -1).astype(np.float64)
        # return centers of new pixels, not bottom left corner
        return squares + square_dist / 2, (xres, yres)

    try:
        rebin_matrix = np.load( rebin_dir + 'rebinMatrix%f_%s.npy' % (scale, data_type))
        rebin_resolution = np.load( rebin_dir + 'rebinResolution%f_%s.npy' % (scale, data_type ))
    except FileNotFoundError:
        pixmap = np.array([cam_x, cam_y])
        # create prototype pixel polygon
        pix_distance = pixmap[0, 1] - pixmap[0, 0]
        hexa_radius = pix_distance * np.tan(np.pi / 6)
        pixel = ps.Circle( radius=hexa_radius, points=6 )
        pixel_area = pixel.area()

        # create list of all camera pixel polygons
        camera_pixels = []
        for pos in np.transpose(pixmap):
            pix = Polygon(pixel)
            pix.shift(pos[0], pos[1])
            camera_pixels.append(pix)

        # calculate rebin grid on the fly, and save it for later!
        rebin_grid, resolution = calculate_rebin_grid(camera_pixels)
        rebin_resolution = resolution
        print( "Calculating matrix of scale %f for resolution %d %d" % (scale, rebin_resolution[0], rebin_resolution[1]) )

        square_distance = rebin_grid[1, 1] - rebin_grid[1, 0]

        # create list of all rebin pixel polygons
        square_pixels = []
        for coord in np.transpose(rebin_grid):
            square = ps.Rectangle( square_distance )
            square.shift(coord[0] - square_distance / 2, coord[1] - square_distance / 2)
            square_pixels.append(square)

        rebin_matrix = np.zeros((len(square_pixels), len(camera_pixels)))
        for idx in tqdm(np.ndindex(rebin_matrix.shape), total=rebin_matrix.size, desc="rebin matrix"):
            overlap = square_pixels[idx[0]] & camera_pixels[idx[1]]
            rebin_matrix[idx] = overlap.area() / pixel_area
        np.save( rebin_dir + 'rebinMatrix%f_%s' % (scale, data_type), rebin_matrix)
        np.save( rebin_dir + 'rebinResolution%f_%s' % (scale, data_type), resolution)
    # print( "Using rebin of scale %f with resolution %d %d" % (scale, *self.rebin_resolution) )
    rebin_matrix = csr_matrix(rebin_matrix)
    return rebin_matrix, rebin_resolution


def poly_list_bbox( polylist ):
    """
    Takes a list of polygons and returns the bounding box for the sum of all polygons.
    This is basically the bounding box of the convex hull of all the polygons united.

    :param polylist: list of polygons
    :return: tuple of bound coordinates
    """
    bbs = np.array([np.array(p.boundingBox()) for p in polylist])
    mins = np.amin(bbs, axis=0)
    maxs = np.amax(bbs, axis=0)
    xmin = mins[0]
    xmax = maxs[1]
    ymin = mins[2]
    ymax = maxs[3]
    return xmin, xmax, ymin, ymax


def pad_to_n_pix( arr, resolution ):
    # (64, 62) -> (64, 64)
    # (100, 97) -> (100, 100)
    dx = resolution - arr.shape[0]
    dy = resolution - arr.shape[1]
    return np.lib.pad(arr, ((0, dx), (0, dy)), mode='constant', constant_values=0)
