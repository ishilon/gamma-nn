import os
import sys
from random import shuffle
from datetime import datetime
from time import sleep
from math import ceil
import numpy as np
import gc
import matplotlib.pyplot as plt
from skimage.transform import rotate as im_rot
import tensorflow as tf
from multiprocessing import Pool
from gnnlib.utils import utils
from gnnlib.data import pixel_maps as pixm
from gnnlib.data import rebin_img
import h5py


def get_h5dst_folders_list( args ):
    folder_list = [ ]
    gnn_dst = utils.get_env_variable( 'GNN_DST' )
    meta_folder = gnn_dst + '/h5_DSTs/' + args.inst + '/'

    for phase in args.phase:
        folder_list.append( os.path.join( meta_folder, phase + '/' ) )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        for j in range( len(args.dtype) ):
            if j == 0:
                folder_list[ i ] += args.dtype[ j ] + '/'
            else:
                folder_list.append( folder + args.dtype[ j ] + '/' )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        for j in range( len( args.hemi ) ):
            if j == 0:
                folder_list[ i ] += args.hemi[ j ] + '/'
            else:
                folder_list.append( folder + args.hemi[ j ] + '/' )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        for j in range( len( args.zenith ) ):
            if j == 0:
                folder_list[ i ] += args.zenith[ j ] + '/'
            else:
                folder_list.append( folder + args.zenith[ j ] + '/' )

    for i in range( len( folder_list ) ):
        folder = folder_list[ i ]
        if 'diffuse' in folder:
            continue
        for j in range( len( args.offset ) ):
            if j == 0:
                folder_list[ i ] += args.offset[ j ] + '/'
            else:
                folder_list.append( folder + args.offset[ j ] + '/' )

    return sorted( folder_list )


def get_dsts_list( folders_list ):
    """
    Get the list of DST files in a list of folders
    :param folders_list: A list of folders containing DSTs
    :return: A list of DST file paths
    """
    dsts_list = []
    for folder in folders_list:
        if not os.path.exists( folder ):
            print( 'The folder {0} cannot be found.'.format(folder) )  # User request (by Manu) for this important functionality
            utils.exit_point( 20.0 )
        files = os.listdir( folder )
        for file in files:
            if '.h5' in file:
                dsts_list.append( os.path.join( folder, file ) )
    shuffle(dsts_list)
    return dsts_list


def get_folder_fractions( folders_list, cluster=False ):
    """
    Get the ratios of events to collect from each folder
    :return: a dictionary with the folder names and their fractions
    """
    folder_fractions = { }
    num_folders = len( folders_list )
    if num_folders > 1:
        print( 'You specified {0} "offset" dirs for your dataset'.format( num_folders ) )
        for folder in folders_list:
            s = 'h5_DSTs'
            short_name = folder[ folder.find( '/', folder.find( s ) + len( s ) ): ]
            if cluster:
                frac = 1
            else:
                frac = float( input( 'What is the ratio of events for folder\n{0}?\n'.format( short_name ) ) )
            folder_fractions[ short_name ] = frac
        print( 'You requested the following ratios:' )
        sum_frac = sum( list( folder_fractions.values() ) )
        for key, value in folder_fractions.items():
            new_val = value / sum_frac
            print( '{0:80} : {1} (corresponding to {2}%)'.format( key, value, round( new_val * 100, 3 ) ) )
            folder_fractions[ key ] = new_val
        utils.exit_point( 30 )
    else:
        s = 'h5_DSTs'
        short_name = folders_list[0][ folders_list[0].find( '/', folders_list[0].find( s ) + len( s ) ): ]
        folder_fractions[ short_name ] = 1

    return folder_fractions


def get_event_params( combined_image, ct1_image, ct2_image, ct3_image, ct4_image,
                      uid, primary_id, num_images, energy,
                      ct1_loc_dis, ct2_loc_dis, ct3_loc_dis, ct4_loc_dis,
                      ct1_length, ct2_length, ct3_length, ct4_length,
                      ct1_width, ct2_width, ct3_width, ct4_width,
                      ct1_size, ct2_size, ct3_size, ct4_size,
                      src_alt, src_az, obs_alt, obs_az, obs_time, core_x_gr, core_y_gr, tels_in_event ):
    """
    Create the event parametrs list.
    The parameters order must be consistent with the parameters in write_events_to_tfr()!!!
    :param combined_image:
    :param uid:
    :param primary_id:
    :param num_images:
    :param energy:
    :param ct1_image:
    :param ct2_image:
    :param ct3_image:
    :param ct4_image:
    :param ct1_loc_dis:
    :param ct2_loc_dis:
    :param ct3_loc_dis:
    :param ct4_loc_dis:
    :param ct1_length:
    :param ct2_length:
    :param ct3_length:
    :param ct4_length:
    :param ct1_width:
    :param ct2_width:
    :param ct3_width:
    :param ct4_width:
    :param ct1_size:
    :param ct2_size:
    :param ct3_size:
    :param ct4_size:
    :param src_alt:
    :param src_az:
    :param obs_alt:
    :param obs_az:
    :param obs_time:
    :param core_x_gr:
    :param core_y_gr:
    :param tels_in_event:
    :return:
    """
    if primary_id == 101:
        primary_id = 1

    return (combined_image,  # 0
            uid,  # 1
            primary_id,  # 2
            num_images,  # 3
            energy,  # 4
            ct1_image,  # 5
            ct2_image,  # 6
            ct3_image,  # 7
            ct4_image,  # 8
            ct1_loc_dis,  # 9
            ct2_loc_dis,  # 10
            ct3_loc_dis,  # 11
            ct4_loc_dis,  # 12
            ct1_length,  # 13
            ct2_length,  # 14
            ct3_length,  # 15
            ct4_length,  # 16
            ct1_width,  # 17
            ct2_width,  # 18
            ct3_width,  # 19
            ct4_width,  # 20
            ct1_size,  # 21
            ct2_size,  # 22
            ct3_size,  # 23
            ct4_size,  # 24
            src_alt,  # 25
            src_az,  # 26
            obs_alt,  # 27
            obs_az,  # 28
            obs_time,  # 29
            core_x_gr,  # 30
            core_y_gr,  # 31
            tels_in_event  # 32
            )


def write_events_to_tfr( events_tuple, set_name, ds_dir, counter=0, events_per_file=0, run_file=None ):
    """
    Dump a list of events to a serialized .tfrecord format.
    Must be consistent with get_event_params()!!!
    :param events_tuple: tuple of lists - each list contains event parameters. Assumes that len(event_tuple) > 0
    :param set_name:
    :param ds_dir:
    :param counter:
    :param events_per_file:
    :param run_file: name of file for real data tfrecord creation
    :return:
    """
    tot = len( events_tuple )
    print( 'Number of events to serialize: ', tot )

    num_of_files = ceil( tot / events_per_file )
    index = int(ceil( tot / num_of_files ))
    set_num = 0
    for i in range( 0, len( events_tuple ), index ):
        if set_name != 'real data' and set_name != 'mc analysis':
            set_num += 1
            tfr_name = '{0}{1}_file_f{2}_{3}'.format( ds_dir, set_name, counter, set_num )
        elif set_name == 'real data':
            tfr_name = '{0}{1}'.format( ds_dir, run_file[:-3][run_file.find('run_'): ] )
        else:
            tfr_name = '{0}{1}'.format( ds_dir, run_file[:-3][run_file.find('run'): ] )
        print( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) + ': Writing {0} events to {1}.tfrecord'.format( len( events_tuple[ i: i + index ] ), tfr_name ) )
        writer = tf.python_io.TFRecordWriter( '{0}.tfrecord'.format( tfr_name ) )
        for event in events_tuple[ i: i + index ]:
            time_str = str( event[ 29 ] )
            example = tf.train.Example( features = tf.train.Features( feature = {
                'combined_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[ 0 ].tobytes() ] ) ),
                'uid': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[ 1 ].encode() ] ) ),
                'type': tf.train.Feature( int64_list = tf.train.Int64List( value = [ event[ 2 ] ] ) ),
                'num_images': tf.train.Feature( int64_list = tf.train.Int64List( value = [ event[ 3 ] ] ) ),
                'energy': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 4 ] ] ) ),
                'ct1_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[ 5 ].tobytes() ] ) ),
                'ct2_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[ 6 ].tobytes() ] ) ),
                'ct3_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[ 7 ].tobytes() ] ) ),
                'ct4_image': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ event[ 8 ].tobytes() ] ) ),
                'ct1_loc_dis': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 9 ] ] ) ),
                'ct2_loc_dis': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 10 ] ] ) ),
                'ct3_loc_dis': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 11 ] ] ) ),
                'ct4_loc_dis': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 12 ] ] ) ),
                'ct1_length': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 13 ] ] ) ),
                'ct2_length': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 14 ] ] ) ),
                'ct3_length': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 15 ] ] ) ),
                'ct4_length': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 16 ] ] ) ),
                'ct1_width': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 17 ] ] ) ),
                'ct2_width': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 18 ] ] ) ),
                'ct3_width': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 19 ] ] ) ),
                'ct4_width': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 20 ] ] ) ),
                'ct1_size': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 21 ] ] ) ),
                'ct2_size': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 22 ] ] ) ),
                'ct3_size': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 23 ] ] ) ),
                'ct4_size': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 24 ] ] ) ),
                'src_alt': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 25 ] ] ) ),
                'src_az': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 26 ] ] ) ),
                'obs_alt': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 27 ] ] ) ),
                'obs_az': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 28 ] ] ) ),
                'obs_time': tf.train.Feature( bytes_list = tf.train.BytesList( value = [ time_str.encode() ] ) ),
                'core_x': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 30 ] ] ) ),
                'core_y': tf.train.Feature( float_list = tf.train.FloatList( value = [ event[ 31 ] ] ) ),
                'tels_in_event': tf.train.Feature( int64_list = tf.train.Int64List( value = event[ 32 ] ) )
            } ) )
            writer.write( example.SerializeToString() )
        writer.close()
        print( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) + ': tfrecord writer closed!\n' )


def reject_event( config_args, num_of_images, tels_in_event, primary_id, energy, offset,
                  ct1_loc_dis, ct1_size, ct2_loc_dis, ct2_size, ct3_loc_dis, ct3_size, ct4_loc_dis, ct4_size):
    """
    Skip an event that do not pass presel cuts, don't fit the energy range, etc.
    :param config_args:
    :param num_of_images:
    :param tels_in_event:
    :param primary_id:
    :param energy:
    :param offset:
    :param ct1_loc_dis:
    :param ct1_size:
    :param ct2_loc_dis:
    :param ct2_size:
    :param ct3_loc_dis:
    :param ct3_size:
    :param ct4_loc_dis:
    :param ct4_size:
    :return:
    """
    # Multiplicity cuts
    if num_of_images < 2:
        return 0
    elif tels_in_event[4]:
        if num_of_images < (config_args.min_h1_images_multiplicity + 1):
            return 0
    if config_args.data == 'mc':
        # Keep events with events below bound
        if offset > config_args.max_offset:
            return 0
        # Keep events that have energy between certain bounds
        if primary_id != 101:
            if energy < config_args.min_energy or energy > config_args.max_energy:
                return 0
        else:
            if energy < config_args.min_energy or energy > (3 * config_args.max_energy):
                return 0
    # HAP presel cuts
    num_h1_images = 4
    h1_loc_dis = config_args.h1_loc_dis_cut
    h1_size_cut = config_args.h1_size_cut
    if ct1_loc_dis > h1_loc_dis or ct1_loc_dis < 0 or ct1_size < h1_size_cut:
        num_h1_images -= 1
        tels_in_event[ 0 ] = 0
    if ct2_loc_dis > h1_loc_dis or ct2_loc_dis < 0 or ct2_size < h1_size_cut:
        num_h1_images -= 1
        tels_in_event[ 1 ] = 0
    if ct3_loc_dis > h1_loc_dis or ct3_loc_dis < 0 or ct3_size < h1_size_cut:
        num_h1_images -= 1
        tels_in_event[ 2 ] = 0
    if ct4_loc_dis > h1_loc_dis or ct4_loc_dis < 0 or ct4_size < h1_size_cut:
        num_h1_images -= 1
        tels_in_event[ 3 ] = 0

    return num_h1_images


def rotate_event( config_args, combined_image, event_images, tels, event_index, src_alt, src_az, obs_alt, obs_az ):
    """
    Rotate the combined image and each of the telescope image by an arbitrary angle
    :param config_args:
    :param combined_image: The rebinned combined image
    :param event_images: numpy array of arrays for each telescope image
    :param tels: a list of the telescopes that have an event passing cuts. 0 indicates no image, otherwise tel_number
    :param event_index: the event index in the dst energy array
    :param src_alt: source alt coordinate
    :param src_az: source az coordinate
    :param obs_alt: observation pointing alt
    :param obs_az: observation pointing az
    :return:
    """
    ang = np.random.uniform( 0.0, 360.0 )

    # Get the image center
    center = (-1, -1)
    if config_args.hess1_resolution == 100:
        center = (100 / 2 - 0.5, 97 / 2 - 0.5)
    elif config_args.hess1_resolution == 64:
        center = (64 / 2 - 0.5, 62 / 2 - 0.5)
    elif config_args.hess1_resolution == 32:
        center = (32 / 2 - 0.5, 31 / 2 - 0.5)
    else:
        raise SystemExit('unknown hess1 resolution %s cannot rotate images!' % config_args.hess1_resolution)

    max_combined_image = np.amax( combined_image )
    combined_image_rot = combined_image / max_combined_image
    combined_image_rot = im_rot( combined_image_rot, angle=ang, center=center, cval=0.0, mode='constant', clip=True, order=3, preserve_range=True ).astype( np.float32 )
    combined_image_rot = combined_image_rot * max_combined_image

    # rotate only triggered images
    for tel in tels[ np.nonzero( tels ) ]:
        max_pe = np.max(event_images[ tel - 1 ])
        image_rot = event_images[ tel - 1 ] / max_pe
        image_rot = im_rot( image_rot, angle=ang, center=center, cval=0.0, mode='constant', clip=True, order=3, preserve_range = True ).astype( np.float32 )
        image_rot = image_rot * max_pe
        event_images[ tel - 1 ] = image_rot

    # Calculate the new (rotated source position
    rot_src_alt, rot_src_az = utils.rodrigues_rotation( src_alt, src_az, obs_alt, obs_az, -ang )

    # if debug show images
    if config_args.show_images and event_index % 100 == 0:
        print( 'obs_alt\tobs_az\tsrc_alt\tsrc_az\trot_src_alt\trot_src_az' )
        print( obs_alt, obs_az, src_alt, src_az, rot_src_alt, rot_src_az )

        offset_orig = utils.great_circle_distance( src_alt, obs_alt,
                                                   src_az, obs_az )
        offset_rot = utils.great_circle_distance( rot_src_alt, obs_alt,
                                                  rot_src_az, obs_az )
        print( 'offset = ', offset_orig, 'offset_rot = ', offset_rot )
        # combined_image[ center[0], center[1] ] = max_proc_image
        # processed_image_rot[ center[0], center[1] ] = max_proc_image
        fig, ax = plt.subplots( 1, 2, figsize = (16, 16) )
        ax[ 0 ].imshow( combined_image )
        ax[ 0 ].set_title( 'original' )
        ax[ 1 ].imshow( combined_image_rot )
        ax[ 1 ].set_title( 'rotated by {0}'.format( ang ) )
        plt.show()

    return combined_image_rot, event_images, rot_src_alt, rot_src_az


def process_dsts_list(config_args, dsts_list, cam1_x, rebin_matrix, rebin_resolution, run_file=None, do_analysis=False):
    list_of_events = []
    print('Processing {0} files...'.format(len(dsts_list)))
    total_events = 0
    for h5_file in dsts_list:
        with h5py.File( h5_file, 'r' ) as dst:
            # Getting run data from file
            run_data = dst[ 'Run_Data' ]
            obs_alt = run_data[ 'ObsAlt' ][ 0 ]
            obs_az = run_data[ 'ObsAz' ][ 0 ]

            # Getting events parameters from file
            events_header = dst[ 'Events_List' ]
            run_id_array = np.ones_like( events_header[ 'Event_ID' ] ) * run_data[ 'Run_ID' ][ 0 ]
            tmp_uid = np.c_[ run_id_array, events_header[ 'Bunch_ID' ], events_header[ 'Event_ID' ] ]
            uid = [ "-".join( item ) for item in tmp_uid.astype( str ) ]
            tels_in_event = events_header[ 'Tels_in_event' ]
            num_of_images = events_header[ 'Num_of_images' ]
            energy = events_header[ 'Energy' ]
            primary_id = events_header[ 'PrimaryID' ]

            obs_time = events_header[ 'Time' ]
            src_alt = events_header[ 'Altitude' ]
            src_az = events_header[ 'Azimuth' ]
            core_x_gr = events_header[ 'CoreX_Ground' ]
            core_y_gr = events_header[ 'CoreY_Ground' ]

            ct1_loc_dis = events_header[ 'CT1_loc_dis' ]
            ct1_size = events_header[ 'CT1_size' ]
            ct1_length = events_header[ 'CT1_length' ]
            ct1_width = events_header[ 'CT1_width' ]
            ct2_loc_dis = events_header[ 'CT2_loc_dis' ]
            ct2_size = events_header[ 'CT2_size' ]
            ct2_length = events_header[ 'CT2_length' ]
            ct2_width = events_header[ 'CT2_width' ]
            ct3_loc_dis = events_header[ 'CT3_loc_dis' ]
            ct3_size = events_header[ 'CT3_size' ]
            ct3_length = events_header[ 'CT3_length' ]
            ct3_width = events_header[ 'CT3_width' ]
            ct4_loc_dis = events_header[ 'CT4_loc_dis' ]
            ct4_size = events_header[ 'CT4_size' ]
            ct4_length = events_header[ 'CT4_length' ]
            ct4_width = events_header[ 'CT4_width' ]

            ct1_images = events_header[ 'CT1_image' ]
            ct1_pixel_ids = events_header[ 'CT1_pixel_ID' ]
            ct2_images = events_header[ 'CT2_image' ]
            ct2_pixel_ids = events_header[ 'CT2_pixel_ID' ]
            ct3_images = events_header[ 'CT3_image' ]
            ct3_pixel_ids = events_header[ 'CT3_pixel_ID' ]
            ct4_images = events_header[ 'CT4_image' ]
            ct4_pixel_ids = events_header[ 'CT4_pixel_ID' ]

            total_events = len(uid)

            if config_args.verbose:
                print( '-------------------------------------' )
                print( 'Opened h5dst! ', datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) )
                if config_args.debug:
                    print(h5_file)
                print( 'Number of events in file {0} = {1}'.format( h5_file, total_events ) )

            offset = utils.great_circle_distance( src_alt, obs_alt, src_az, obs_az, angles='degrees' )
            for event_index in range( len( uid ) ):
                # Skip events that do not pass presel cuts, energy range, etc.
                num_h1_images = reject_event( config_args, num_of_images[ event_index ], tels_in_event[ event_index ], primary_id[ event_index ],
                                              energy[ event_index ], offset[event_index],
                                              ct1_loc_dis[ event_index ], ct1_size[ event_index ], ct2_loc_dis[ event_index ], ct2_size[ event_index ],
                                              ct3_loc_dis[ event_index ], ct3_size[ event_index ], ct4_loc_dis[ event_index ], ct4_size[ event_index ] )
                if num_h1_images < config_args.min_h1_images_multiplicity:
                    continue

                event_images = np.array( [ ct1_images[ event_index ], ct2_images[ event_index ],
                                           ct3_images[ event_index ], ct4_images[ event_index ] ] )
                event_pixel_ids = np.array( [ ct1_pixel_ids[ event_index ], ct2_pixel_ids[ event_index ],
                                              ct3_pixel_ids[ event_index ], ct4_pixel_ids[ event_index ] ] )

                participating_tels = np.array(tels_in_event[ event_index, 0:4 ])
                # Process images of the events that survived
                combined_pixel_ids, combined_intensity = rebin_img.add_images( event_pixel_ids, event_images, participating_tels )
                processed_image = rebin_img.rebin( combined_pixel_ids[0], combined_intensity, cam1_x, rebin_matrix, rebin_resolution, config_args.hess1_resolution )

                rebinned_tel_imgs = np.array([np.zeros([config_args.hess1_resolution, config_args.hess1_resolution], np.float32),
                                              np.zeros([config_args.hess1_resolution, config_args.hess1_resolution], np.float32),
                                              np.zeros([config_args.hess1_resolution, config_args.hess1_resolution], np.float32),
                                              np.zeros([config_args.hess1_resolution, config_args.hess1_resolution], np.float32)])

                for i, tel in enumerate(participating_tels):
                    if tel != 0:
                        tel_image = rebin_img.rebin( event_pixel_ids[ i ], event_images[ i ], cam1_x, rebin_matrix, rebin_resolution, config_args.hess1_resolution )
                    else:
                        tel_image = np.zeros([config_args.hess1_resolution, config_args.hess1_resolution])
                    rebinned_tel_imgs[ i ] = tel_image

                if config_args.num_image_rotations > 0 and 'pointsource' in h5_file:
                    for _ in range( config_args.num_image_rotations ):
                        processed_image_rot, event_images, rot_src_alt, rot_src_az = rotate_event(config_args,
                                                                                                  processed_image,
                                                                                                  rebinned_tel_imgs,
                                                                                                  participating_tels,
                                                                                                  event_index,
                                                                                                  src_alt[event_index],
                                                                                                  src_az[event_index],
                                                                                                  obs_alt,
                                                                                                  obs_az)
                        event_params = get_event_params( processed_image_rot, event_images[0], event_images[1], event_images[2], event_images[3],
                                                         uid[event_index], primary_id[event_index], num_h1_images, energy[event_index],
                                                         ct1_loc_dis[event_index], ct2_loc_dis[event_index], ct3_loc_dis[event_index], ct4_loc_dis[event_index],
                                                         ct1_length[event_index], ct2_length[event_index], ct3_length[event_index], ct4_length[event_index],
                                                         ct1_width[event_index], ct2_width[event_index], ct3_width[event_index], ct4_width[event_index],
                                                         ct1_size[event_index], ct2_size[event_index], ct3_size[event_index], ct4_size[event_index],
                                                         rot_src_alt, rot_src_az, obs_alt, obs_az, obs_time[event_index],
                                                         core_x_gr[event_index], core_y_gr[event_index], tels_in_event[event_index] )
                        list_of_events.append( event_params )
                else:
                    event_params = get_event_params( processed_image, rebinned_tel_imgs[0], rebinned_tel_imgs[1], rebinned_tel_imgs[2], rebinned_tel_imgs[3],
                                                     uid[event_index], primary_id[event_index], num_h1_images, energy[event_index],
                                                     ct1_loc_dis[event_index], ct2_loc_dis[event_index], ct3_loc_dis[event_index], ct4_loc_dis[event_index],
                                                     ct1_length[event_index], ct2_length[event_index], ct3_length[event_index], ct4_length[event_index],
                                                     ct1_width[event_index], ct2_width[event_index], ct3_width[event_index], ct4_width[event_index],
                                                     ct1_size[event_index], ct2_size[event_index], ct3_size[event_index], ct4_size[event_index],
                                                     src_alt[event_index], src_az[event_index], obs_alt, obs_az, obs_time[event_index],
                                                     core_x_gr[event_index], core_y_gr[event_index], tels_in_event[event_index])
                    list_of_events.append( event_params )

                if config_args.show_images and event_index % 5000 == 0:
                    plt.imshow( processed_image )
                    plt.title( 'Primary ID = {0}'.format( primary_id[ event_index ] ) )
                    plt.pause( 1 )

    if do_analysis and config_args.data == 'real':
        print( 'Converting {0} events to tfrecords for real data analysis'.format( len( list_of_events ) ) )
        write_events_to_tfr( list_of_events, 'real data', config_args.analysis_dir, counter=0, events_per_file=len( list_of_events ), run_file=run_file )
        return run_file, len(list_of_events)

    elif do_analysis and config_args.data == 'mc':
        print( 'Converting {0} events to tfrecords for mc data analysis'.format( len( list_of_events ) ) )
        write_events_to_tfr( list_of_events, 'mc analysis', config_args.analysis_dir, counter=0, events_per_file=len( list_of_events ), run_file=run_file )
        return run_file, len(list_of_events)

    print('total for worker = ', len(list_of_events))
    return list_of_events


def get_parallel_list(folder_dsts, files_per_process, workers):
    """
    Prepare a list of lists: each sublist has a length equal to the number of workers that will be called each time a process pool is created.
    This sub-list is referred to as the pool_lists in this function.
    pool_lists contains the list of files - each with length files_per_process - that will be read by a single process in the pool.
    :param folder_dsts: the dsts in the folder
    :param files_per_process: number of files to be processed by each worker
    :param workers: number of workers that will be initiated in each process pool
    :return: for 2 pools of 3 workers each, parallel_list looks like this:
    [ [ ['files list 1 for worker 1'], ['files list 2 for worker 2'], ['files list 3 for worker 3']  ],
    [ ['files list 4 for worker 1'], ['files list 5 for worker 2'], ['files list 6 for worker 3'] ] ]
    """
    parallel_list = []
    pool_lists = []
    x = len( folder_dsts )
    if len( folder_dsts ) % files_per_process == 0:
        x += 1
    for i in range( files_per_process, x, files_per_process ):
        pool_lists.append( folder_dsts[ i - files_per_process: i ] )

        if i % (workers * files_per_process) == 0 or i == len( folder_dsts ):
            parallel_list.append( pool_lists )
            pool_lists = [ ]

    if len( folder_dsts ) % files_per_process != 0:
        pool_lists.append( folder_dsts[ -(len( folder_dsts ) % files_per_process): ] )
        parallel_list.append( pool_lists )
    return parallel_list


def create_dataset_files( config_args, folders_list, ds_dir ):
    # Calculate the number of required events for each set
    # Min length of the dataset to have a training set with max_events.
    # The validation is taken as valid_frac * max_events and test = test_frac * tot_num_of_events.
    # So: tot_num_of_events = (1-test_frac)*tot_num_of_events*(1-valid_frac) +
    # + (1-test_frac)*tot_num_of_events*valid_frac + test_frac*tot_num_of_events
    tot_num_of_events = ceil( config_args.train_events / ((1 - config_args.test_frac) * (1 - config_args.valid_frac)) )
    valid_len = int( tot_num_of_events * (1 - config_args.test_frac) * config_args.valid_frac )
    test_len = int( tot_num_of_events * config_args.test_frac )
    diff = tot_num_of_events - config_args.train_events - valid_len - test_len
    test_len += diff
    print( 'The total number of events needed to create your datasets is {0}: {1} for training, {2} for validation and {3} for testing.\n'.format( tot_num_of_events,
                                                                                                                                                   config_args.train_events,
                                                                                                                                                   valid_len, test_len ) )
    # Get the fractions of files to collect from each folder
    folder_fractions = get_folder_fractions( folders_list, cluster = config_args.cluster_bool )
    sets_count_dict = {'train': 0, 'valid': 0, 'test': 0}
    folder_req_dict = folder_fractions.copy()  # dict holding the number of required events from each folder
    folder_req_dict.update( (key, ceil(value * tot_num_of_events)) for key, value in folder_req_dict.items() )
    folder_sets_count = folder_fractions.copy()  # dict to count the subsets contribution from each folder
    folder_sets_count.update( (key, {'train': 0, 'valid': 0, 'test': 0}) for key, _ in folder_req_dict.items() )

    cam1_x, cam1_y = pixm.get_mc_cam1_xy()
    rebin_matrix, rebin_resolution = rebin_img.prepare_rebin_matrix(cam1_x, cam1_y, config_args.sampling)

    print('\n\n******************************************')
    print( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) +
           ': Starting image pre-processing for your datasets' )

    workers = config_args.workers
    files_per_process = config_args.files_per_process  # Not sure yet why, but this stops working at 4 (independent of the number of workers) when processing large files

    def log_result( worker_files ):
        # This is called whenever process_dsts_list() returns a result.
        # result_list is modified only by the main process, not the pool workers.
        results_list.append( worker_files )

    counter = 0
    for folder in folders_list:

        counter += 1

        s = 'h5_DSTs'
        h5_folder = folder[ folder.find( '/', folder.find( s ) + len( s ) ): ]  # The folder short name in which the file is saved
        print("Processing events from '{0}'".format(h5_folder))
        # Get the list of dst files to prepare dataset from
        folder_dsts = get_dsts_list( [folder] )
        print( 'Total number of files in folder: {0}'.format(len(folder_dsts)) )
        print('Requested number of events: {0}'.format(folder_req_dict[ h5_folder ]))

        parallel_list = get_parallel_list(folder_dsts, files_per_process, workers)

        if config_args.verbose:
            print('There will be {0} calls for parallel DSTs reading'.format( len(parallel_list) ))

        p_i = 0
        folder_events_list = []  # The list to save all events from current folder
        for part in parallel_list:
            p_i += 1
            results_list = []
            pool = Pool(processes=workers, maxtasksperchild=1)
            print( '\nStarting pool {0} of {1} with {2} workers:'.format( p_i, len(parallel_list), workers ) )

            for worker_list in part:
                pool.apply_async( process_dsts_list, args=(config_args, worker_list, cam1_x, rebin_matrix, rebin_resolution, ), callback=log_result )
            pool.close()
            pool.join()
            if config_args.debug:
                print( 'len_results_list = ', len( results_list ) )

            for events in results_list:
                folder_events_list.extend( events )

            gc.collect()  # collect garbage
            sleep(2.5)

            print('Collected so far: {0}'.format(len(folder_events_list)))
            if len(folder_events_list) >= folder_req_dict[h5_folder]:
                print( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) + ': Enough events ({0}) collected from folder {1} (required: {2})'.format(len(folder_events_list),
                                                                                                                                                 h5_folder,
                                                                                                                                                 folder_req_dict[ h5_folder ]) )
                break
        if len(folder_events_list) < folder_req_dict[h5_folder]:
            if len(folder_events_list) > config_args.train_events:
                print('The folder does not contain enough events to satisfy your requirements ({0} events needed).'.format(folder_req_dict[ h5_folder ]))
                utils.exit_point(25.0)
            else:
                print('Folder {0} does not contain enough events for your train set.'.format(h5_folder))
                utils.exit_point(25.0)

        shuffle(folder_events_list)
        if counter == len(folders_list):
            # ensure that the training will have exactly config_args.train_events examples
            train_index = config_args.train_events - sets_count_dict['train']
        else:
            train_index = ceil( config_args.train_events * folder_fractions[ h5_folder ] )
        valid_index = train_index + int( valid_len * folder_fractions[ h5_folder ] )
        #test_index =  valid_index + int(test_len * folder_fractions[h5_folder])
        # Matthias would prefer to have a pre-defined test set size like that... Idan doesen't...
        train_tuple = tuple(folder_events_list[: train_index])
        valid_tuple = tuple(folder_events_list[ train_index: valid_index ])
        #test_tuple = tuple(folder_events_list[ valid_index: test_index ])
        test_tuple = tuple(folder_events_list[valid_index:])

        folder_events_list = []
        del folder_events_list

        sets_count_dict['train'] += len( train_tuple )
        folder_sets_count[h5_folder]['train'] += len( train_tuple )
        print( 'Converting {0} events to tfrecords for training set'.format( len( train_tuple ) ) )
        write_events_to_tfr( train_tuple, 'train', ds_dir, counter=counter, events_per_file=config_args.events_per_file )
        del train_tuple

        if len(valid_tuple) > 0:
            sets_count_dict[ 'valid' ] += len( valid_tuple )
            folder_sets_count[ h5_folder ][ 'valid' ] += len( valid_tuple )
            print( 'Converting {0} events to tfrecords for validation set'.format( len( valid_tuple) ) )
            write_events_to_tfr( valid_tuple, 'valid', ds_dir, counter=counter, events_per_file=config_args.events_per_file )
        del valid_tuple

        if len( test_tuple ) > 0:
            sets_count_dict['test'] += len( test_tuple )
            folder_sets_count[ h5_folder ][ 'test' ] += len( test_tuple )
            print( 'Converting {0} events to tfrecords for test set'.format( len( test_tuple ) ) )
            write_events_to_tfr( test_tuple, 'test', ds_dir, counter=counter, events_per_file=config_args.events_per_file )
        del test_tuple

        gc.collect()
        sleep(2.5)
    print(sets_count_dict)

    if not os.path.exists( os.path.join( ds_dir, 'logfiles/' ) ):
        os.mkdir( os.path.join( ds_dir, 'logfiles/' ) )
        for subset, num in sets_count_dict.items():
            with open( os.path.join( ds_dir, 'logfiles/{0}_log.txt'.format(subset) ), 'w' ) as logfile:
                logfile.write( '{0} dataset was created at: '.format(subset) )
                logfile.write( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) )
                logfile.write( '\n' )
                logfile.write( 'The {0} dataset contains {1} events:\n'.format( subset, num ) )
                print( '\nThe {0} dataset contains {1} events\n'.format( subset, num ) )
                for key, value in folder_sets_count.items():
                    logfile.write( 'From folder {0}: {1} events were collected\n'.format(key, value[subset]) )
                    print( '{0} events were collected from folder {1}'.format(value[subset], key) )
