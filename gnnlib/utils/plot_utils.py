import numpy as np
import matplotlib.pyplot as plt
# import pandas as pd
# from sklearn import metrics


def get_zetas_from_df( df, label, param, min_val, max_val ):
    return df.loc[ (df[ 'true_label' ] == label) & (df[param ] > min_val)
                   & (df[param ] <= max_val) ][ 'zeta' ].values


def plot_histogram( title, data, num_of_bins, xlabel, xmin, xmax, label, weights_bool=False ):
    plt.figure( 'hist' )
    plt.xlim( [ xmin, xmax ] )
    if weights_bool:
        weights = np.ones_like( data ) / len( data )
    else:
        weights = None
    hist, bins, _ = plt.hist( data, weights = weights, bins = num_of_bins, histtype = 'step', alpha = 0.8, lw = 1.2, label = label )

    plt.title( title, fontsize = 14 )
    plt.xlabel( xlabel, fontsize = 12 )
    if weights_bool:
        plt.ylabel( 'Percentage %', fontsize = 12 )
    else:
        plt.ylabel( 'Num of Examples #', fontsize = 12 )
    plt.legend( loc = 'upper left' )
    plt.grid(True)
    return hist, bins

def plot_roc( fpr, tpr, roc_auc ):
    plt.figure('ROC_curve')
    lw = 2.0
    plt.plot( fpr, tpr, color='darkorange',
              lw=lw, label='CRNN (preselcted events); AUC = %0.4f' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate (1 - specitivity)', fontsize=12 )
    plt.ylabel('True Positive Rate (sensitivity)', fontsize=12 )
    #plt.title('ROC (base benchmark set)', fontsize=12 )
    plt.title( 'ROC', fontsize = 12 )
    plt.legend(loc="lower right", prop={'size': 12})
    plt.grid(True)


def plot_zeta_distribution( data, bin_size, color, title, label ):
    plt.figure( 'Zeta_distribution' )
    weights = np.ones_like(data) / len(data)
    bins = np.arange(0, 1.01, bin_size)  # fixed bin size
    plt.xlim([-0.01, 1.01])
    hist = plt.hist(data, bins=bins, weights=weights * 100, histtype='step', color=color, alpha=0.8, lw=2, label=label)
    plt.yscale('log', nonposy='clip')
    plt.title( title, fontsize=12 )
    plt.xlabel( r'$\zeta$', fontsize=12  )
    plt.ylabel( 'Percentage %', fontsize=12 )
    plt.legend( loc='upper center', prop={'size': 12} )
    plt.grid(True)
    print( '\n' + title + ' (' + label + ')' )
    for i in range( 10, 100, 10 ):
        integral = bin_size * sum( hist[ 0 ][ 0:i ] )
        print( 'zeta cut at %.2f \t--> Integral of particles %.4f %%' % ((i / 100.0), (1.0 - integral) * 100.0) )
    print( '---' )

    return hist

def scatter_plot( x, y, xlabel, ylabel, title, axes=None ):
    if axes == None:
        axes = plt.gca()

    axes.scatter( x, y, marker = 'o', s = 30 )

    axes.set_xlim( [ xmin, xmax ] )
    axes.set_ylim( [ ymin, ymax ] )

    axes.set_xlabel( xlabel, fontsize = 12 )
    axes.set_ylabel( ylabel, fontsize = 12 )
    axes.set_title( title, fotnsize=14 )