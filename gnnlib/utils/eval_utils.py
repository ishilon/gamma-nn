import os
from datetime import datetime
import math
import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sys import exit as sysex
from gnnlib.utils import plot_utils as pltutil
from gnnlib.utils import utils


def load_checkpoint(checkpoint_dir, sess, saver):
    ckpt = tf.train.get_checkpoint_state( checkpoint_dir )
    ckpt_model_name = checkpoint_dir + ckpt.model_checkpoint_path.split( '/' )[ -1 ]
    if ckpt and ckpt.model_checkpoint_path:
        # Restores from checkpoint
        saver.restore( sess, ckpt_model_name )
        # Assuming model_checkpoint_path looks something like:
        #   /my-favorite-path/model.ckpt-0,
        # extract global_step from it.
        global_step = ckpt.model_checkpoint_path.split( '/' )[ -1 ].split( '-' )[ -1 ]
        print( 'Loaded checkpoint with global_step = {0}'.format( global_step ) )
        return global_step
    else:
        print( 'No checkpoint file found' )
        return None


def print_loss( total_loss, num_iter, sess, summary_writer, global_step ):
    # Multiply by 1000 to avoid keeping track of tiny numbers
    norm_loss = total_loss / num_iter
    print( '{0}: The total loss at step {1} is {2}'.format( datetime.now(), int(global_step), round(total_loss, 8) ) )
    print( 'The scaled loss per batch at step {0} is {1}'.format( int( global_step ), round(norm_loss, 8) ) )

    sum1 = tf.summary.scalar( 'Loss_normalised', norm_loss )
    sum_op = tf.summary.merge( [ sum1 ] )
    summary = sess.run( sum_op )
    summary_writer.add_summary( summary, global_step )
    summary_writer.flush()
    # summary = tf.Summary()
    # summary.ParseFromString( sess.run(summary_op) )
    # summary.value.add( tag='Precision @ 1', simple_value=precision )
    # summary.value.add( tag='Normalised loss', simple_value=nor_loss )
    # summary_writer.add_summary( summary, global_step )


def eval_once_classification( checkpoint_dir, saver, summary_writer, loss, logits, true_labels, num_examples, batch_size ):
    """Run Eval once.
    Args:
        checkpoint_dir: checkpoint directory
        saver: Saver.
        summary_writer: Summary writer.
        loss:
        num_examples:
        batch_size:
    """
    # a TF op to get the softmax activations for each class
    sftmx = tf.nn.softmax( logits )
    # Calculate predictions. Says whether the targets (labels) are in the top K predictions
    k = 1
    t_p = tf.nn.in_top_k( sftmx, true_labels, k )

    config = tf.ConfigProto( allow_soft_placement=True )
    config.gpu_options.allow_growth = True
    # config.gpu_options.per_process_gpu_memory_fraction = 0.5
    with tf.Session(config=config) as sess:
        global_step = load_checkpoint( checkpoint_dir, sess, saver )
        # Start the queue runners.
        coord = tf.train.Coordinator()
        threads = []

        test_df = pd.DataFrame(columns=['zeta', 'true_label'])

        try:
            for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
                threads.extend( qr.create_threads( sess, coord=coord, daemon=True, start=True) )

            total_loss = 0
            step = 0
            true_positives = 0
            num = num_examples % batch_size
            num_iter = int(math.ceil(num_examples / batch_size))
            print('num_iter: ', num_iter, 'test_examples:', num_examples)
            while step < num_iter and not coord.should_stop():
                if step % 100 == 0:
                    print(step)
                #batch_loss, batch_true_positives, tp, tn, fp, fn = sess.run( [loss, t_p, tp, tn, fp, fn] )
                batch_loss, batch_true_positives, batch_sftmx, batch_true_labels = sess.run([loss, t_p, sftmx, true_labels])

                # Don't include events that are already listed
                # print(step)

                if step == num_iter - 1 and num != 0:
                    print( 'Evaluating last batch, containing {0} events'.format( num ) )
                    batch_true_positives = batch_true_positives[: num]
                    batch_sftmx = batch_sftmx[: num]
                    batch_true_labels = batch_true_labels[: num]

                batch_df = pd.DataFrame({'zeta': batch_sftmx[:, 0], 'true_label': batch_true_labels})
                test_df = test_df.append(batch_df, ignore_index=True)
                total_loss += batch_loss
                true_positives += np.sum(batch_true_positives)
                step += 1

            print_loss( total_loss, num_iter, sess, summary_writer, global_step )
            # Compute precision @ 1.
            precision = true_positives / num_examples
            print( '{0} images classified correctly, out of {1} images in the dataset.'.format( true_positives, num_examples ) )
            print( '{0}: Precision @ 1 = {1}%'.format( datetime.now(), round( precision * 100, 2 ) ) )

            zeta_photons = test_df.loc[ (test_df[ 'true_label' ] == 0.0)][ 'zeta' ].values
            zeta_protons = test_df.loc[ (test_df[ 'true_label' ] == 1.0)][ 'zeta' ].values

            eval_utils.print_clas_metrices(zeta_photons, zeta_protons)

        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join( threads, stop_grace_period_secs=10 )


def eval_once_regression( checkpoint_dir, saver, summary_writer, loss_op, num_examples, batch_size ):
    """Run Eval once.
    Args:
        checkpoint_dir: checkpoint directory
        saver: Saver.
        summary_writer: Summary writer.
        loss_op:
        num_examples:
        batch_size:
    """
    config = tf.ConfigProto( allow_soft_placement=True )
    with tf.Session(config=config) as sess:
        global_step = load_checkpoint( checkpoint_dir, sess, saver )
        # Start the queue runners.
        coord = tf.train.Coordinator()
        threads = []
        try:
            for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
                threads.extend( qr.create_threads( sess, coord=coord, daemon=True, start=True) )

            num_iter = int( math.ceil( num_examples / batch_size ) )
            print('num_iter: ', num_iter, 'test_examples:', num_examples)
            total_loss = 0
            step = 0
            while step < num_iter and not coord.should_stop():
                if step % 100 == 0:
                    print(step)
                batch_loss = sess.run( loss_op )
                # print(batch_loss)
                total_loss += batch_loss
                step += 1

            print_loss( total_loss, num_iter, sess, summary_writer, global_step )

        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join( threads, stop_grace_period_secs=10 )


def freeze_model(sess, output_graph_file, output_node_names):
    # [print(n.name) for n in tf.get_default_graph().as_graph_def().node]
    # We use a built-in TF helper to export variables to constants
    output_graph_def = tf.graph_util.convert_variables_to_constants(
        sess,  # The session is used to retrieve the weights
        tf.get_default_graph().as_graph_def(),  # The graph_def is used to retrieve the nodes
        output_node_names  # .split(",")  # The output node names are used to select the usefull nodes
    )

    # Finally we serialize and dump the output graph to the filesystem
    with tf.gfile.GFile(output_graph_file, "wb") as f:
        f.write(output_graph_def.SerializeToString())
    print("%d ops in the final graph." % len(output_graph_def.node))

    return output_graph_def


def eval_csv(args, data_dir, test_df=None):
    if test_df is None:
        sysex('Exiting: Could not load the test_df file!')


    if 'alt' in args.train_type:
        true_dalt_array = test_df['true_dalt'].values  # np.absolute(test_df['true_dalt'].values)
        pred_value_array = test_df['predicted_dalt'].values
        # biases
        err = (true_dalt_array - pred_value_array)  # / true_dalt_array
        x = err <= 0.01
        print('{0} events of {1} ({2}%) have true_dalt - pred_dalt value of less than 0.01'.format(sum(x),
                                                                                                   len(true_dalt_array),
                                                                                                   round(100 * sum(x) / len(true_dalt_array), 2)))
        print('err = ', err)

        pltutil.plot_histogram('Delta Alt Distribution', err, 1000,
                               'Delta Alt [Deg]', -1 * np.amax(err), np.amax(err), 'Delta alt', weights_bool=True)
        plt.show()

        plt.plot(true_dalt_array, err, 'o')
        plt.grid(True)
        plt.title('"True" delta alt bias (unaveraged)')
        plt.xlabel('True delta alt [deg]')
        plt.ylabel(r'(dalt$_{true}$ - dalt$_{cnn}$)')
        plt.show()

        nbins = 80
        n, _ = np.histogram(true_dalt_array, bins=nbins)
        sy, _ = np.histogram(true_dalt_array, bins=nbins, weights=err)
        sy2, _ = np.histogram(true_dalt_array, bins=nbins, weights=err * err)
        mean = sy / n
        std = np.sqrt(sy2 / n - mean * mean)
        plt.errorbar((_[1:] + _[:-1]) / 2, mean, yerr=std, fmt='r-')
        plt.grid(True)
        plt.title('Delta alt bias')
        plt.xlabel('True delta alt [deg]')
        plt.ylabel(r'<dalt$_{true}$ - dalt$_{cnn}$>')
        plt.show()

    if 'az' in args.train_type:
        true_daz_array = test_df['true_daz'].values
        pred_value_array = test_df['predicted_daz'].values
        # biases
        err = (true_daz_array - pred_value_array)  # / true_dalt_array
        x = err <= 0.01
        print('{0} events of {1} ({2}%) have true_daz - pred_daz value of less than 0.01'.format(sum(x),
                                                                                                 len(true_daz_array),
                                                                                                 round(
                                                                                                 100 * sum(x) / len(true_daz_array),
                                                                                                 2)))
        print('err = ', err)

        pltutil.plot_histogram('Delta Az Distribution', err, 1000,
                               'Delta Az [Deg]', -1 * np.amax(err), np.amax(err), 'Delta az', weights_bool=True)
        plt.show()

        plt.plot(true_daz_array, err, 'o')
        plt.grid(True)
        plt.title('"True" delta az bias (unaveraged)')
        plt.xlabel('True delta az [deg]')
        plt.ylabel(r'(daz$_{true}$ - daz$_{cnn}$)')
        plt.show()

        nbins = 80
        n, _ = np.histogram(true_daz_array, bins=nbins)
        sy, _ = np.histogram(true_daz_array, bins=nbins, weights=err)
        sy2, _ = np.histogram(true_daz_array, bins=nbins, weights=err * err)
        mean = sy / n
        std = np.sqrt(sy2 / n - mean * mean)
        plt.errorbar((_[1:] + _[:-1]) / 2, mean, yerr=std, fmt='r-')
        plt.grid(True)
        plt.title('Delta az bias')
        plt.xlabel('True delta az [deg]')
        plt.ylabel(r'<daz$_{true}$ - daz$_{cnn}$>/true_daz')
        plt.show()

    if "core" in args.train_type and "x" in args.train_type:
        pred_value_array = test_df['predicted_core_x'].values
        true_corex_array = test_df['true_core_x'].values  # np.absolute(test_df['true_dalt'].values)
        # biases
        err = (true_corex_array - pred_value_array)  # / true_corex_array
        x = err <= 0.01
        print(sum(x))
        print('err = ', err)
        plt.plot(true_corex_array, err, 'o')
        plt.grid(True)
        plt.title('"True" coreX bias (unaveraged)')
        plt.xlabel('True coreX [m]')
        plt.ylabel(r'(coreX$_{true}$ - coreX$_{cnn}$)')
        plt.show()

        nbins = 80
        n, _ = np.histogram(true_corex_array, bins=nbins)
        sy, _ = np.histogram(true_corex_array, bins=nbins, weights=err)
        sy2, _ = np.histogram(true_corex_array, bins=nbins, weights=err * err)
        mean = sy / n
        std = np.sqrt(sy2 / n - mean * mean)
        plt.errorbar((_[1:] + _[:-1]) / 2, mean, yerr=std, fmt='r-')
        plt.grid(True)
        plt.title('CoreX bias')
        plt.xlabel('True CoreX [m]')
        plt.ylabel(r'<coreX$_{true}$ - coreX$_{cnn}$>/coreX$_{true}$')
        plt.show()

    if "core" in args.train_type and "y" in args.train_type:
        pred_value_array = test_df['predicted_core_y'].values
        true_corey_array = test_df['true_core_y'].values  # np.absolute(test_df['true_dalt'].values)
        # biases
        err = (true_corey_array - pred_value_array)  # / true_corey_array
        x = err <= 0.01
        print(sum(x))
        print('err = ', err)
        plt.plot(true_corey_array, err, 'o')
        plt.grid(True)
        plt.title('"True" coreY bias (unaveraged)')
        plt.xlabel('True coreY [m]')
        plt.ylabel(r'(coreY$_{true}$ - coreY$_{cnn}$)')
        plt.show()

        nbins = 80
        n, _ = np.histogram(true_corey_array, bins=nbins)
        sy, _ = np.histogram(true_corey_array, bins=nbins, weights=err)
        sy2, _ = np.histogram(true_corey_array, bins=nbins, weights=err * err)
        mean = sy / n
        std = np.sqrt(sy2 / n - mean * mean)
        plt.errorbar((_[1:] + _[:-1]) / 2, mean, yerr=std, fmt='r-')
        plt.grid(True)
        plt.title('CoreY bias')
        plt.xlabel('True CoreY [m]')
        plt.ylabel(r'<coreY$_{true}$ - coreY$_{cnn}$>/coreY$_{true}$')
        plt.show()

    if "corexy" in args.train_type and "y" in args.train_type:
        true_corex = test_df['true_core_x'].values
        true_corey = test_df['true_core_y'].values
        true_r_array = np.sqrt(np.square(true_corex) + np.square(true_corey))

        pred_corex = test_df['predicted_core_x'].values
        pred_corey = test_df['predicted_core_y'].values
        pred_r_array = np.sqrt(np.square(pred_corex) + np.square(pred_corey))

        # biases
        err = np.abs(true_r_array - pred_r_array) / true_r_array
        x = err <= 0.01
        print(sum(x))
        print('err = ', err)
        plt.plot(true_r_array, err, 'o')
        plt.grid(True)
        plt.title('"True" R bias (unaveraged)')
        plt.xlabel('True R [m]')
        plt.ylabel(r'(R$_{true}$ - R$_{cnn}$)/R$_{true}$')
        plt.show()

        # true_energy_array = test_df['true_energy'].values
        # size_array = test_df['size'].values
        # avg_loc_dis_list = test_df['avg_loc_dis'].values

        # Plot Energy Distribution of the entire dataset
        # tf_plt.plot_histogram( 'Energy Distribution', true_energy_array, 100,
        #                'Energy [TeV]', 0, 150, 'energy')
        # plt.show()

        # Plot Energy Distribution of the entire dataset
        # tf_plt.plot_histogram( 'size Distribution', size_array, 100,
        #                'size [p.e.]', 0, 1152000, 'size' )
        # plt.show()

    if 'en' in args.train_type:    
        true_energy = test_df['true_energy'].values
        pred_energy = np.abs(test_df['predicted_en'].values)

        # Energy migration matrix
        plt.hexbin(true_energy, pred_energy, bins='log', cmap='inferno')
        plt.title('Energy migration matrix')
        plt.xlabel('True Energy [TeV]')
        plt.ylabel('Predicted Energy [TeV]')
        plt.show()
        
        # Energy biases unaveraged
        err = ( pred_energy - true_energy ) / true_energy
        plt.plot( true_energy, err, 'o' )
        plt.grid(True)
        plt.title('"True" energy bias (unaveraged)')
        plt.xlabel('Energy [TeV]')
        plt.xscale("log")
        plt.ylabel(r'(E$_{true}$ - E$_{cnn}$)/E$_{true}$')
        plt.show()

        # Energy biases averaged
        nbins = 80
        n, _ = np.histogram( true_energy, bins=np.logspace(np.log10(0.1),np.log10(150.0), 30) )
        sy, _ = np.histogram( true_energy, bins=np.logspace(np.log10(0.1),np.log10(150.0), 30), weights = err )
        sy2, _ = np.histogram( true_energy, bins=np.logspace(np.log10(0.1),np.log10(150.0), 30), weights = err * err )
        mean = sy / n
        std = np.sqrt( sy2 / n - mean * mean )
        plt.errorbar( (_[ 1: ] + _[ :-1 ]) / 2, mean, yerr = std, fmt = 'r-' )
        plt.grid(True)
        plt.title('Energy bias')
        plt.xlabel('Energy [TeV]')
        plt.xscale("log")
        plt.ylabel(r'<(E$_{true}$ - E$_{cnn}$)>/E$_{true}$')
        plt.show()

        # elif args.train_type == 'size-run':
        # size biases
        # err = (pred_value_array - size_array) / size_array
        # plt.plot( size_array, err, 'o' )
        # plt.grid( True )
        # plt.title( '"True" size bias (unaveraged)' )
        # plt.xlabel( 'size [p.e.]' )
        # plt.ylabel( r'(S$_{true}$ - S$_{cnn}$)/S$_{true}$' )
        # plt.show()

        # nbins = 80
        # n, _ = np.histogram( size_array, bins = nbins )
        # sy, _ = np.histogram( size_array, bins = nbins, weights = err )
        # sy2, _ = np.histogram( size_array, bins = nbins, weights = err * err )
        # mean = sy / n
        # std = np.sqrt( sy2 / n - mean * mean )
        # plt.errorbar( (_[ 1: ] + _[ :-1 ]) / 2, mean, yerr = std, fmt = 'r-' )
        # plt.grid( True )
        # plt.title( 'Size bias' )
        # plt.xlabel( 'Size [p.e.]' )
        # plt.ylabel( r'<(S$_{true}$ - S$_{cnn}$)>/S$_{true}$' )
        # plt.show()

        # Plot xmax Distribution of the entire dataset
        # xxmax = np.amax(xmax_list)
        # plot_histogram( 'xmax Distribution', xmax_list, 50,
        #                'xmax [m]', 0, xxmax, 'xmax' )
        # plt.show()

        # Plot loc_dis Distribution of the entire dataset
        # plot_histogram( 'Avg. Loc. Dis. Distribution', avg_loc_dis_list, 50,
        #                'Avg. Loc. Dis. [m]', 0, 0.8, 'avg. loc. dis.', weights_bool=True )
        # plt.show()

def print_clas_metrices(zeta_photons, zeta_protons):
    len_g = len(zeta_photons)
    correct_g = sum(zeta_photons > 0.5)  # True  Positiv
    incorrect_g = sum(zeta_photons < 0.5)  # False Positive

    len_p = len(zeta_protons)
    correct_p = sum(zeta_protons < 0.5)  # True  Negative
    incorrect_p = sum(zeta_protons > 0.5)  # False Negative

    acc = (correct_g + correct_p) / (len_g + len_p)
    precision = correct_g / (correct_g + incorrect_p)
    recall = correct_g / (correct_g + incorrect_g)
    specificity = correct_p / (correct_p + incorrect_p)
    print('Accuracy:\t', round(acc * 100, 2), '\t\t (how many right decisions were made)')
    print('Precission:\t', round(precision * 100, 2),
          '\t\t (how many true Class 1 are among the events labeled as 1)')
    print('Recall:\t\t', round(recall * 100, 2), '\t\t (how many of Class 0 were labeled as 0)')
    print('Specificity:\t', round(specificity * 100, 2), '\t\t (how many of Class 1 were labeled as 1)')


def get_number_existing_csv_files(csv_dir):
    if not os.path.exists( csv_dir ):
        os.mkdir( csv_dir )

    count = 0
    df_files = os.listdir(csv_dir)
    for f in df_files:
        if 'test_df' in f:
            count += 1
    return count

def choose_from_test_csv_file(model_dir):
    files = os.listdir(os.path.join(model_dir, 'csv_files/'))
    df_files = []
    for f in files:
        if 'test_df' in f:
            df_files.append(f)
    if len(df_files) > 1:
        print('\nThere are multiple csv files available.\n')
        for i, f in enumerate(df_files):
            print('%i) %s' % (i, f))
        n = utils.ask_user_for_number(30, nmax=len(df_files))
    elif len(df_files) == 1:
        n = 0
    else:
        raise SystemExit('We should not have ended here... ABORT!')
    print(n)
    df_file = df_files[n].strip()
    print('loaded csv from disc!', df_file)

    return df_file