from sys import exit as sysex
from sklearn import metrics
import matplotlib.pyplot as plt
import pandas as pd
from gnnlib.utils import plot_utils as pltutil
from gnnlib.utils import eval_utils
import numpy as np


def plot_roc(zeta, y_test):
    # Plot ROC curve
    # sklearn takes zeta as the probablity to be class 1 (gamma is class 0)
    zeta_sk = 1 - zeta
    fpr, tpr, _ = metrics.roc_curve( y_test, zeta_sk )
    roc_auc = metrics.auc( fpr, tpr )

    #df = pd.DataFrame({'x': fpr, 'y': tpr})
    #df.to_csv('/per/gnn_data/models/classification/hess1/konrad.phase1.gamma_diffuse-proton_diffuse.south.20deg.no_cuts/h1_sampling_rebin.h1_resolution_64.num_rotations_0.train_events_2048.0k/image_scale_std.method_clas_rnn.arc_clas_rnn/figs/no_cuts.csv')

    pltutil.plot_roc( fpr, tpr, roc_auc )
    '''
    df = pd.read_csv('/per/gnn_data/models/classification/hess1/konrad.phase1.gamma_diffuse-proton_diffuse.south.20deg.no_cuts/h1_sampling_rebin.h1_resolution_64.num_rotations_0.train_events_2048.0k/image_scale_std.method_clas_rnn.arc_clas_rnn/figs/no_cuts.csv')
    x1 = df['x'].values
    y1 = df['y'].values
    plt.plot(x1, y1, 'C2', linestyle='--', lw=2.0, label='CRNN (no cuts); AUC = %0.4f' %0.9875)

    if input('Plot tmva ROC aswell? [y/n]\n') == 'y':
        filepath = input('give path to the csv with tmva ROC values:\n')
        x, y = get_roc_tmva(filepath)
        from scipy.interpolate import interp1d
        xnew = np.linspace(x.min(), x.max(), 118)
        f = interp1d(x, y, kind='cubic')
        plt.scatter(x, y, s=16, label='BDT (preselcted events); AUC = %0.4f' %0.9642)  # without cuts = 0.9642, with cuts = 0.9642
        plt.legend(prop={'size': 10})
    '''
    plt.show()


def get_roc_tmva(filepath):
    df = pd.read_csv(filepath)
    return df['1-x'].values, df['y'].values


def eval_clas_csv(test_df, args):
    if test_df is None:
        sysex( 'Exiting: Could not load the test_df file!' )

    zeta = test_df[ 'zeta' ].values
    y_test = test_df[ 'true_label' ].values
    plot_roc(zeta, y_test)

    energy_0 = test_df.loc[ test_df[ 'true_label' ] == 0.0 ][ 'true_energy' ].values
    energy_1 = test_df.loc[ test_df[ 'true_label' ] == 1.0 ][ 'true_energy' ].values
    # avg_loc_dis_list = test_df[ 'avg_loc_dis' ].values

    num_0 = len( energy_0 )
    num_1 = len( energy_1 )
    total_num = num_0 + num_1
    print('Events in test set (signal, background):', num_0, num_1)
    if np.mean(energy_1) == -1:
        print('Background is real data!!! no Energy information available.')

        bin_size = 0.01

        zeta_photons = pltutil.get_zetas_from_df(test_df, 0.0, 'true_energy', -100.0, 1000.0)
        zeta_protons = pltutil.get_zetas_from_df(test_df, 1.0, 'true_energy', -100.0, 1000.0)

        eval_utils.print_clas_metrices(zeta_photons, zeta_protons )

        pltutil.plot_zeta_distribution(zeta_photons, bin_size,
                                       color='navy',
                                       title=r'$\zeta$ distribution for all events',
                                       label='Signal')
        pltutil.plot_zeta_distribution(zeta_protons, bin_size,
                                       color='darkorange',
                                       title=r'$\zeta$ distribution for all events',
                                       label='Background')
        plt.show()
    else:

        bin_size = 0.01

        zeta_photons = pltutil.get_zetas_from_df(test_df, 0.0, 'true_energy', -100.0, 1000.0)
        zeta_protons = pltutil.get_zetas_from_df(test_df, 1.0, 'true_energy', -100.0, 1000.0)

        eval_utils.print_clas_metrices(zeta_photons, zeta_protons)

        pltutil.plot_zeta_distribution(zeta_photons, bin_size,
                                       color='navy',
                                       title=r'$\zeta$ distribution for all events',
                                       label='Signal')
        pltutil.plot_zeta_distribution(zeta_protons, bin_size,
                                       color='darkorange',
                                       title=r'$\zeta$ distribution for all events',
                                       label='Background')
        plt.show()

        # Plot Energy Distribution of the entire dataset
        pltutil.plot_histogram( 'Gamma Energy Distribution ({0} events)'.format( num_0 ), energy_0, 100,
                                'Energy [TeV]', 0, args.max_energy * 1.1, 'gamma energy' )
        plt.show()

        # Plot Energy Distribution of the entire dataset
        pltutil.plot_histogram( 'Hadron Energy Distribution ({0} events)'.format( num_1 ), energy_1, 100,
                                'Energy [TeV]', 0, args.max_energy * 3.1, 'hadron energy' )
        plt.show()

        # Plot loc_dis Distribution of the entire dataset
        # pltutil.plot_histogram( 'Avg. Loc. Dis. Distribution', avg_loc_dis_list, 50,
        #                        'Avg. Loc. Dis. [m]', 0, 0.8, 'avg. loc. dis.', weights_bool = True )
        # plt.show()

        # fixed bin size for zeta plots
        bin_size = 0.01

        e_0 = 1
        zeta_photons_0 = pltutil.get_zetas_from_df( test_df, 0.0, 'true_energy', 0.0, e_0 )
        len_g0 = len(zeta_photons_0)
        correct_g0 = sum(zeta_photons_0 > 0.5)

        zeta_protons_0 = pltutil.get_zetas_from_df( test_df, 1.0, 'true_energy', 0.0, e_0 * 3 )
        len_p0 = len(zeta_protons_0)
        correct_p0 = sum(zeta_protons_0 < 0.5)
        acc_0 = (correct_g0 + correct_p0)/(len_g0 + len_p0)

        pltutil.plot_zeta_distribution( zeta_photons_0, bin_size,
                                        color='navy', title=(r'$\zeta$ distribution for {0} - {1} Tev'.format( args.min_energy,
                                                                                                               e_0) + '\nEvents = {0}, Acc. = {1}%'.format(len_g0 + len_p0,
                                                                                                                                                           round(acc_0*100, 2))),
                                        label='Signal' )
        pltutil.plot_zeta_distribution( zeta_protons_0, bin_size,
                                        color='darkorange', title=(r'$\zeta$ distribution for {0} - {1} Tev'.format( args.min_energy,
                                                                                                                     e_0) + '\nEvents = {0}, Acc. = {1}%'.format(len_g0 + len_p0,
                                                                                                                                                                 round(acc_0*100, 2))),
                                        label='Background' )
        plt.show()

        e_1 = 10
        zeta_photons_1 = pltutil.get_zetas_from_df( test_df, 0.0, 'true_energy', e_0, e_1 )
        len_g1 = len( zeta_photons_1 )
        correct_g1 = sum( zeta_photons_1 > 0.5 )

        zeta_protons_1 = pltutil.get_zetas_from_df( test_df, 1.0, 'true_energy', e_0 * 3, e_1 * 3 )
        len_p1 = len( zeta_protons_1 )
        correct_p1 = sum( zeta_protons_1 < 0.5 )
        acc_1 = (correct_g1 + correct_p1) / (len_g1 + len_p1)

        pltutil.plot_zeta_distribution( zeta_photons_1, bin_size,
                                        color = 'navy', title = (r'$\zeta$ distribution for {0} - {1} Tev'.format( e_0,
                                                                                                                   e_1) + '\nEvents = {0}, Acc. = {1}%'.format(len_g1 + len_p1,
                                                                                                                                                               round(acc_1*100, 2))),
                                        label = 'Signal' )
        pltutil.plot_zeta_distribution( zeta_protons_1, bin_size,
                                        color = 'darkorange', title = (r'$\zeta$ distribution for {0} - {1} Tev'.format( e_0,
                                                                                                                         e_1) + '\nEvents = {0}, Acc. = {1}%'.format(len_g1 + len_p1,
                                                                                                                                                                     round(acc_1*100, 2))),
                                        label = 'Background' )
        plt.show()

        e_2 = 60
        zeta_photons_2 = pltutil.get_zetas_from_df( test_df, 0.0, 'true_energy', e_1, e_2 )
        len_g2 = len( zeta_photons_2 )
        correct_g2 = sum( zeta_photons_2 > 0.5 )

        zeta_protons_2 = pltutil.get_zetas_from_df( test_df, 1.0, 'true_energy', e_1 * 3, e_2 * 3 )
        len_p2 = len( zeta_protons_2 )
        correct_p2 = sum( zeta_protons_2 < 0.5 )
        acc_2 = (correct_g2 + correct_p2) / (len_g2 + len_p2)

        pltutil.plot_zeta_distribution( zeta_photons_2, bin_size,
                                        color = 'navy', title = (r'$\zeta$ distribution for {0} - {1} Tev'.format( e_1,
                                                                                                                   e_2) + '\nEvents = {0}, Acc. = {1}%'.format(len_g2 + len_p2,
                                                                                                                                                               round(acc_2*100, 2))),
                                        label = 'Signal' )
        pltutil.plot_zeta_distribution( zeta_protons_2, bin_size,
                                        color = 'darkorange', title = (r'$\zeta$ distribution for {0} - {1} Tev'.format( e_1,
                                                                                                                         e_2) + '\nEvents = {0}, Acc. = {1}%'.format(len_g2 + len_p2,
                                                                                                                                                                     round(acc_2*100, 2))),
                                        label = 'Background' )
        plt.show()

        e_3 = int(args.max_energy)
        zeta_photons_3 = pltutil.get_zetas_from_df( test_df, 0.0, 'true_energy', e_2, e_3 )
        len_g3 = len( zeta_photons_3 )
        correct_g3 = sum( zeta_photons_3 > 0.5 )

        zeta_protons_3 = pltutil.get_zetas_from_df( test_df, 1.0, 'true_energy', e_2 * 3, e_3*3.5 )
        len_p3 = len( zeta_protons_3 )
        correct_p3 = sum( zeta_protons_3 < 0.5 )
        acc_3 = (correct_g3 + correct_p3) / (len_g3 + len_p3)

        pltutil.plot_zeta_distribution( zeta_photons_3, bin_size,
                                        color = 'navy', title = (r'$\zeta$ distribution for {0} - {1} (280) Tev'.format( e_2, e_3) + '\nEvents = {0}, Acc. = {1}%'.format(len_g3 + len_p3, round(acc_3*100, 2))),
                                        label = 'Signal' )
        pltutil.plot_zeta_distribution( zeta_protons_3, bin_size,
                                        color = 'darkorange', title = (r'$\zeta$ distribution for {0} - {1} (280) Tev'.format( e_2, e_3) + '\nEvents = {0}, Acc. = {1}%'.format(len_g3 + len_p3, round(acc_3*100, 2))),
                                        label = 'Background' )
        plt.show()

        tot_len = len_g0+len_p0+len_g1+len_p1+len_g2+len_p2+len_g3+len_p3
        tot_corr = correct_g0+correct_p0+correct_g1+correct_p1+correct_g2+correct_p2+correct_g3+correct_p3
        tot_acc = round(tot_corr*100/tot_len, 2)
        print('total events = ', tot_len)
        print('total_acc = ', tot_acc)

        # Plot zeta distribution
        zeta_photons = test_df.loc[ test_df[ 'true_label' ] == 0.0 ][ 'zeta' ].values
        zeta_protons = test_df.loc[ test_df[ 'true_label' ] == 1.0 ][ 'zeta' ].values

        total_num_round = total_num // 1000 + 1

        gamma_zeta_distribution = pltutil.plot_zeta_distribution( zeta_photons, bin_size,
                                                                  color = 'navy',
                                                                  title = r'$\zeta$ distribution (preselect benchmark Acc. = {1}%)'.format(
                                                                      total_num_round, tot_acc ), label = 'Signal' )
        proton_zeta_distribution = pltutil.plot_zeta_distribution( zeta_protons, bin_size,
                                                                   color = 'darkorange',
                                                                   title = r'$\zeta$ distribution (preselect benchmark Acc. = {1}%)'.format(
                                                                       total_num_round, tot_acc ), label = 'Background' )
        print( gamma_zeta_distribution[ 0 ].size, np.sum( gamma_zeta_distribution[ 0 ] ) )
        print( proton_zeta_distribution[ 0 ].size, np.sum( proton_zeta_distribution[ 0 ] ) )

        plt.show()

        # zeta_photons_presel = pltutil.get_zetas_from_df( test_df, 0.0, 'avg_loc_dis', 0.0, 0.525 )
        # len_g_presel = len( zeta_photons_presel )
        # correct_g_presel = sum( zeta_photons_presel > 0.5 )

        # zeta_protons_presel = pltutil.get_zetas_from_df( test_df, 1.0, 'avg_loc_dis', 0.0, 0.525 )
        # len_p_presel = len( zeta_protons_presel )
        # correct_p_presel = sum( zeta_protons_presel < 0.5 )
        # acc_presel = (correct_g_presel+correct_p_presel)/(len_g_presel+len_p_presel) * 100
        # print(acc_presel)

        # pltutil.plot_zeta_distribution( zeta_photons_presel, bin_size,
        #                                color = 'navy', title = r'$\zeta$ distribution for Loc. Dis. < 0.525 m' + '\nEvents = {1}, Acc. = {0}%'.format(round(acc_presel, 3), (len_g_presel+len_p_presel)),
        #                                label = 'Signal' )
        # pltutil.plot_zeta_distribution( zeta_protons_presel, bin_size,
        #                                color = 'darkorange', title = r'$\zeta$ distribution for Loc. Dis. < 0.525 m'  + '\nEvents = {1}, Acc. = {0}%'.format(round(acc_presel, 3), (len_g_presel+len_p_presel)),
        #                                label = 'Background' )
        # plt.show()

        # zeta_photons_nopresel = pltutil.get_zetas_from_df( test_df, 0.0, 'avg_loc_dis', 0.525, 0.8 )
        # len_g_nopresel = len( zeta_photons_nopresel )
        # correct_g_nopresel = sum( zeta_photons_nopresel > 0.5 )

        # zeta_protons_nopresel = pltutil.get_zetas_from_df( test_df, 1.0, 'avg_loc_dis', 0.525, 0.8 )
        # len_p_nopresel = len( zeta_protons_nopresel )
        # correct_p_nopresel = sum( zeta_protons_nopresel < 0.5 )
        # acc_nopresel = (correct_g_nopresel + correct_p_nopresel) / (len_g_nopresel + len_p_nopresel) * 100

        # pltutil.plot_zeta_distribution( zeta_photons_nopresel, bin_size,
        #                                color = 'navy', title = r'$\zeta$ distribution for Loc. Dis. > 0.525 m'  + '\nEvents = {1}, Acc. = {0}%'.format(round(acc_nopresel, 3), (len_g_nopresel+len_p_nopresel)),
        #                               label = 'Signal' )
        # pltutil.plot_zeta_distribution( zeta_protons_nopresel, bin_size,
        #                                color = 'darkorange', title = r'$\zeta$ distribution for Loc. Dis. > 0.525 m'  + '\nEvents = {1}, Acc. = {0}%'.format(round(acc_nopresel, 3), (len_g_nopresel+len_p_nopresel)),
        #                                label = 'Background' )
        # plt.show()
