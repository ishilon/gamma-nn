import os
import sys
import time
from select import select
import numpy as np


def exit_point( timeout=180.0 ):
    start_time = time.time()
    print( 'Do you wish to continue? [y/n]' )
    print( '(timeout = {0} seconds)'.format( timeout ) )
    rlist, _, _ = select( [sys.stdin], [], [], timeout )
    if rlist:
        ans = sys.stdin.readline().replace('\n', '')
        if ans == 'y' or ans == 'yes':
            print( 'Moving on :-)' )
            return
        elif ans == 'n' or ans == 'no':
            print( 'Exiting!' )
            sys.exit( 'User chose to stop and exit program.' )
        else:
            print( 'Please answer with y or n' )
            new_timeout = timeout - ( time.time() - start_time )
            exit_point( timeout=new_timeout  )
    else:
        print( 'No input provided. Continuing!' )


def ask_user_for_number( timeout=180.0, nmax=5 ):
    start_time = time.time()
    print( 'Which one do you want?' )
    print( '(timeout = {0} seconds)'.format( timeout ) )
    rlist, _,_ = select( [sys.stdin], [], [], timeout )
    if rlist:
        ans = sys.stdin.readline().replace('\n', '')
        try:
            if int(ans) < nmax:
                return int(ans)
            else:
                print('Not available! Please have another look!')
                new_timeout = timeout - (time.time() - start_time)
                ask_user_for_number(timeout=new_timeout, nmax=nmax)
        except:
            print( 'Please answer with an integer!' )
            new_timeout = timeout - ( time.time() - start_time )
            ask_user_for_number( timeout=new_timeout, nmax=nmax )
    else:
        print('No input provided. We take 0')
        return 0


def print_list_to_txt( l, ds_dir, set_num=1 ):
    file = ds_dir + 'folder_list_{0}.txt'.format(set_num)
    f = open( file, 'w+' )
    for item in l:
        f.write( '{0}\n'.format( item ) )


def get_tower_name():
    # If a model is trained with multiple GPUs, prefix all Op names with tower_name
    # to differentiate the operations. Note that this prefix is removed from the
    # names of the summaries when visualizing a model.
    tower_name = 'tower'
    return tower_name


def get_num_sets( ds_dir ):
    from datetime import datetime

    filelist = os.listdir( ds_dir )

    train_num = 0
    g_train = p_train = e_train = 0
    valid_num = 0
    g_valid = p_valid = e_valid = 0
    test_num = 0
    g_test = p_test = e_test = 0

    for file in filelist:
        if 'datasets_log' in file:
            print( file )
            with open( os.path.join(ds_dir, file), 'r' ) as f:
                lines = f.readlines()
                train_line = lines[1]
                valid_line = lines[2]
                test_line  = lines[3]
                train_num += int( train_line.split( ' ' )[ 4 ] )
                g_train += int( train_line.split( ' ' )[ 6 ] )
                p_train += int( train_line.split( ' ' )[ 8 ] )
                e_train += int( train_line.split( ' ' )[ 11 ] )

                valid_num += int( valid_line.split( ' ' )[ 4 ] )
                g_valid += int( valid_line.split( ' ' )[ 6 ] )
                p_valid += int( valid_line.split( ' ' )[ 8 ] )
                e_valid += int( valid_line.split( ' ' )[ 11 ] )

                test_num += int( test_line.split( ' ' )[ 4 ] )
                g_test += int( test_line.split( ' ' )[ 6 ] )
                p_test += int( test_line.split( ' ' )[ 8 ] )
                e_test += int( test_line.split( ' ' )[ 11 ] )

    with open( os.path.join( ds_dir, 'total_datasets_num.txt' ), 'w' ) as logfile:
        logfile.write( 'File created on: ' )
        logfile.write( datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) )
        logfile.write( '\n' )
        logfile.write( 'The training dataset contains {0} events: {1} gammas, '
                       '{2} protons and {3} electrons\n'.format( train_num, g_train, p_train, e_train ) )
        logfile.write( 'The validation dataset contains {0} events: {1} gammas, '
                       '{2} protons and {3} electrons\n'.format( valid_num, g_valid, p_valid, e_valid ) )
        logfile.write( 'The test dataset contains {0} events: {1} gammas, '
                       '{2} protons and {3} electrons\n'.format( test_num, g_test, p_test, e_test ) )

    return train_num, valid_num, test_num, g_train, p_train, e_train


def binary_find( l, target ):
    start = 0
    end = len( l ) - 1

    while start <= end:
        middle = ( start + end ) // 2
        midpoint = l[ middle ]
        if midpoint > target:
            end = middle - 1
        elif midpoint < target:
            start = middle + 1
        else:
            return midpoint


def get_env_variable(var_name):
    """
    Get the environment variable or return exception
    :param var_name: Environment Variable to lookup
    """
    try:
        return os.environ[var_name]
    except KeyError:
        import configparser
        config_file = os.path.expanduser('~/gnn_env.conf')
        config = configparser.ConfigParser()
        config.optionxform = str
        config.read( [ config_file ] )
        env_vars = {}
        env_vars.update( dict( config.items( 'DEFAULT' ) ) )
        try:
            return env_vars[ var_name ]
        except TypeError:
            import sys
            print( 'Please include an env. vars config file.' )
            sys.exit()
        except KeyError:
            import sys
            print( 'Please check your env. vars names in gnn_env.conf' )
            sys.exit()


def ry(phi):
    return np.array( [[np.cos(phi), 0, np.sin(phi)], [0, 1, 0], [-np.sin(phi), 0, np.cos(phi)]] )


def rz(x):
    return np.array( [[np.cos(x), -np.sin(x), 0], [np.sin(x), np.cos(x), 0], [0, 0, 1]] )


def tilt_to_ground( core_x, core_y, core_z, obs_alt, obs_az ):
    """
    This function takes the pointing position of the run and the core coordinates in the tilted hap system and returns the core coordinates in the ground system.
    :param core_x: x coordinate of the core position in the tilted system
    :param core_y: y coordinate of the core position in the tilted system
    :param core_z: z coordinate of the core position in the tilted system
    :param obs_alt: pointing altitude in degrees
    :param obs_az: pointing azimuth in degrees
    :return: two flattened arrays corresponding to the core (x, y) coordinates in the ground system
    """
    core_x = core_x.reshape( (len( core_x ), 1) )
    core_y = core_y.reshape( (len( core_y ), 1) )
    core_z = core_z.reshape( (len( core_z ), 1) )

    impact_vec_t = np.hstack( ( np.hstack( (core_x, core_y)  ), core_z) )

    theta = 90 - round(obs_alt)
    phi = -obs_az

    impact_vec_g = np.dot(rz(np.deg2rad(phi)), np.dot(ry(np.deg2rad(theta)), impact_vec_t.T)).T

    return impact_vec_g[:, 0], impact_vec_g[:, 1]


def great_circle_distance( alt1, alt2, az1, az2, angles='degrees' ):
    """
    :param alt1: numpy array with altitude of points 1 (in degrees)
    :param alt2: numpy array with altitude of points 2 (in degrees)
    :param az1: numpy array with azimuth of points 1 (in degrees)
    :param az2: numpy array with azimuth of points 2 (in degrees)
    :param angles: string stating degrees or radians
    :return: an array with the great circle distance in the requested angles system (default is degrees)
    """
    alt1_rad = np.deg2rad( alt1 )
    alt2_rad = np.deg2rad( alt2 )
    az1_rad = np.deg2rad( az1 )
    az2_rad = np.deg2rad( az2 )
    s1 = np.sin( alt1_rad )
    s2 = np.sin( alt2_rad )
    c1 = np.cos( alt1_rad )
    c2 = np.cos( alt2_rad )
    sz = np.sin( az1_rad - az2_rad )
    cz = np.cos( az1_rad - az2_rad )

    theta_rad = np.arctan2( np.sqrt( np.square( c2 * sz ) + np.square( c1 * s2 - s1 * c2 * cz ) ), (s1 * s2 + c1 * c2 * cz) )

    if angles == 'degrees':
        # M.B.: this should be a if args.verbose: ... nut no args here
        # print( 'The angular distance is given in degrees!' )
        return np.rad2deg( theta_rad )
    else:
        # M.B.: this should be a if args.verbose: ... nut no args here
        # print( 'The angular distance is given in radians!' )
        return theta_rad


def rodrigues_rotation( alt_vec, az_vec, alt_axis, az_axis, deg ):
    """
    Rodrigues rotation by degrees of a vector around an axis, both given in alt-az system.
    See: https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
    :param alt_vec:
    :param az_vec:
    :param alt_axis:
    :param az_axis:
    :param deg: angle of rotation, in degrees
    :return: alt and az of the rotated vector
    """
    ang = np.deg2rad(deg)
    axis_theta_rad = np.deg2rad( alt_axis )
    axis_phi_rad = np.deg2rad( az_axis )
    vec_theta_rad = np.deg2rad( alt_vec )
    vec_phi_rad = np.deg2rad( az_vec )

    axis_unit_vec = np.array([ np.cos(axis_theta_rad) * np.cos(axis_phi_rad), np.cos(axis_theta_rad) * np.sin(axis_phi_rad), np.sin( axis_theta_rad) ])
    v_unit_vec = np.array( [ np.cos( vec_theta_rad ) * np.cos( vec_phi_rad ), np.cos( vec_theta_rad ) * np.sin( vec_phi_rad ), np.sin( vec_theta_rad ) ] )

    k_mat = np.array([ [0, -axis_unit_vec[2], axis_unit_vec[1]],
                       [axis_unit_vec[2], 0, -axis_unit_vec[0]],
                       [-axis_unit_vec[1], axis_unit_vec[0], 0] ])

    rot_mat = np.identity(3) + np.sin(ang) * k_mat + (1 - np.cos(ang)) * np.dot( k_mat, k_mat )
    v_rot = np.dot(rot_mat, v_unit_vec)

    new_az_rad = np.arctan2( v_rot[1], v_rot[0] )
    if new_az_rad < 0:
        new_az_rad = 2 * np.pi + new_az_rad
    new_alt_rad = np.arcsin( v_rot[2] )

    return np.rad2deg(new_alt_rad), np.rad2deg(new_az_rad)


def get_delta_dir( src_alt, src_az, obs_alt, obs_az ):
    return src_alt - obs_alt, src_az - obs_az
