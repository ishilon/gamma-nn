import argparse
import configparser
import sys

from gnnlib.utils import utils


def str_to_bool(c, s):
    if s == 'True' or s == 'true' or s == 'yes' or s == 'y' or s == 'Y':
        return True
    elif s == 'False' or s == 'false' or s == 'no' or s == 'n' or 'N' == s:
        return False
    else:
        raise ValueError( 'The config file value for {0} is {1}. Please fix that.'.format( c, s ) )


def main():
    """
    A config file parser that can receive arguments from the
    command line to override the defaults given in the config file
    """
    gnn = utils.get_env_variable( 'GNN' )
    config_dir = gnn + '/configs/'
    conf_parser = argparse.ArgumentParser(description='A config file parser that can receive arguments from the\
                                          command line to override the defaults given in the config file', add_help=False)

    conf_parser.add_argument("-c", "--conf", dest='conf_file', help="Specify config file name (without .conf)", metavar="FILE")
    args, remaining_argv = conf_parser.parse_known_args()
    assert isinstance( args.conf_file, str ), "args.conf_file is not a string"
    args.conf_file = args.conf_file + '.conf'
    conf_filename = config_dir + args.conf_file

    default_args = {}
    dataset_args = {}
    image_process_args = {}
    training_args = {}
    evaluation_args = {}

    if args.conf_file:
        config = configparser.ConfigParser()
        config.read( [conf_filename] )
        default_args.update( dict( config.items( 'DEFAULT' ) ) )
        dataset_args.update( dict( config.items( 'DATASET' ) ) )
        image_process_args.update( dict( config.items('IMAGEPROCESS') ) )
        training_args.update( dict( config.items('TRAINING') ) )
        evaluation_args.update( dict( config.items('EVALUATION') ) )

        default_args['test_set_bool'] = str_to_bool( 'test_set', default_args['test_set'] )
        default_args['redo_test_bool'] = str_to_bool( 'redo_test', default_args[ 'redo_test' ] )
        default_args['cluster_bool'] = str_to_bool( 'cluster', default_args['cluster'] )
        default_args['new_records_bool'] = str_to_bool('new_records', default_args['new_records'])

        dataset_args['train_events'] = int(dataset_args['train_events'])
        dataset_args['events_per_file'] = int( dataset_args['events_per_file'] )
        dataset_args['workers'] = int( dataset_args['workers'] )
        dataset_args['files_per_process'] = int( dataset_args['files_per_process'] )

        dataset_args['h1_size_cut'] = float( dataset_args['h1_size_cut'] )
        dataset_args['h2_size_cut'] = float( dataset_args['h2_size_cut'] )
        dataset_args['h1_loc_dis_cut'] = float( dataset_args['h1_loc_dis_cut'] )
        dataset_args['h2_loc_dis_cut'] = float( dataset_args['h2_loc_dis_cut'] )
        dataset_args['min_energy'] = float( dataset_args['min_energy'] )
        dataset_args['max_energy'] = float( dataset_args['max_energy'] )
        dataset_args['max_offset'] = float( dataset_args['max_offset'] )
        dataset_args['alt_scale'] = float( dataset_args['alt_scale'] )
        dataset_args['az_scale'] = float( dataset_args['az_scale'] )
        dataset_args['core_scale'] = float( dataset_args['core_scale'] )
        dataset_args['energy_scale'] = float( dataset_args['energy_scale'] )
        dataset_args['min_h1_images_multiplicity'] = int( dataset_args['min_h1_images_multiplicity'] )

        image_process_args['hess1_resolution'] = int( image_process_args['hess1_resolution'] )
        image_process_args['ct5_resolution'] = int( image_process_args['ct5_resolution'] )
        image_process_args['sampling'] = float( image_process_args['sampling'] )
        image_process_args['num_image_rotations'] = int( image_process_args['num_image_rotations'] )

        training_args['num_gpus'] = int( training_args['num_gpus'] )
        training_args['batch_size'] = int( training_args['batch_size'] )
        training_args['log_device_placement_bool'] = str_to_bool( 'log_device_placement', training_args['log_device_placement'] )
        training_args['load_checkpoint_bool'] = str_to_bool( 'load_checkpoint', training_args['load_checkpoint'] )

        evaluation_args['zeta'] = float(evaluation_args['zeta'])


    # Parse rest of arguments
    # Don't suppress add_help here so it will handle -h
    parser = argparse.ArgumentParser(
        # Inherit options from config_parser
        parents=[conf_parser]
    )
    parser.set_defaults( **default_args )
    parser.set_defaults( **dataset_args )
    parser.set_defaults( **image_process_args )
    parser.set_defaults( **training_args )
    parser.set_defaults( **evaluation_args )

    parser.add_argument( '-q', '--quiet', action="store_false", dest="verbose",
                         default=False, help="Don't print out status messages." )
    parser.add_argument( '-v', '--verbose', action="store_true", dest="verbose",
                         help='Print out status messages.' )
    parser.add_argument( '-d', '--debug', action="store_true", dest="debug",
                         default=False, help='Print out status messages for debugging.')
    parser.add_argument( '--plt', action = "store_true", dest = "show_images",
                         help = 'Plot event images.' )

    parser.add_argument( '--test', '--test_set', action="store_true", dest="test_set_bool",
                         help='Run test set evaluation.' )

    parser.add_argument( '--redo', '--redo_test', action = "store_true", dest = "redo_test_bool",
                         help = 'Create new test_df if exists.' )
    parser.add_argument( '--model_dir', action = "store_true", dest = "model_dir_bool",
                         help = 'Give the model directory path explicitly.' )

    parser.add_argument( '--cluster', action = "store_true", dest = "cluster_bool",
                         help = 'Indicate that job will run on the cluster. Avoids exit_point().' )

    parser.add_argument( '--new', action="store_true", dest="new_dataset_bool",
                         help="Create a new dataset and tfrecord files." )
    parser.add_argument( '--new_recs', action = "store_true", dest = "new_records_bool",
                         help = "Create new tfrecord files from dataset files. The existence "
                                "of dataset files is assumed. If you wish to create new record "
                                "files from a new dataset - do not set this argument to true." )


    parser.add_argument( '--events_per_file', action = "store", type = int, dest = "events_per_file",
                         help = "Maximum number of files in each .tfrecord file" )

    parser.add_argument( '--min_en', action = "store", type = float, dest = "min_energy",
                         help = 'Add dataset lowest energy in TeV (e.g. 0.05).' )
    parser.add_argument( '--max_en', action = "store", type = float, dest = "max_energy",
                         help = 'Add dataset highest energy in TeV (e.g. 100).' )
    parser.add_argument( '--scl_alt', action = "store", type = float, dest = "alt_scale",
                         help = 'Add delta alt scale' )
    parser.add_argument( '--scl_az', action = "store", type = float, dest = "az_scale",
                         help = 'Add delta az scale' )

    parser.add_argument( '-a', '--combine', action="store", type=str, dest="combine_method",
                         help='Combine method of the images' )
    parser.add_argument( '-r', '--res', action="store", type=int, dest="hess1_resolution",
                         help='HESS 1 image resolution after resampling' )
    parser.add_argument( '-g', '--sigma', action="store", type=float, dest="sampling",
                         help='sampling ratio for rebinnning' )
    parser.add_argument( '--norm', action = "store", type=str, dest = "scale_images",
                         help = 'normalize the pixels in the image (none, normalize or std).' )

    parser.add_argument( '--num_gpus', action="store", type=int, dest="num_gpus",
                         help='Number of GPUs in the machine.' )
    parser.add_argument( '--batch', action="store", type=int, dest="batch_size",
                         help='Batch size.' )
    parser.add_argument( '--steps', '--max_steps', action="store", type=int, dest="max_steps",
                         help='Maximum training steps.' )
    parser.add_argument( '--placement', action="store_true", dest="log_device_placement_bool",
                         help="Should a new dataset be created with the specified parameters?" )
    parser.add_argument( '--no_ckpt', action="store_false", dest="load_checkpoint_bool",
                         help="Start training from latest checkpoint." )
    parser.add_argument( '--model', action = "store", type = str, dest = "model",
                         help = 'Network model to be used' )
    parser.add_argument( '--opt', action="store", type=str, dest="optimizer",
                         help='The optimizer algorithm. Options are: adam, grad_des.' )

    args = parser.parse_args(remaining_argv)

    return args, conf_filename


if __name__ == "__main__":
    sys.exit(main())
