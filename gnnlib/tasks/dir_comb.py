import sys
import re
import numpy as np
import multiprocessing
import tensorflow as tf
from gnnlib.data import read_tfr as tfr
from gnnlib.utils import utils


def get_train_input( tfrecord_features, config_args ):
    # Convert from string tensor to 'dtype' tensor (float64 in this case).
    image = tf.decode_raw( tfrecord_features['combined_image'], tf.float32 )
    src_alt = tfrecord_features['src_alt']
    src_az = tfrecord_features['src_az']
    obs_alt = tfrecord_features['obs_alt']
    obs_az = tfrecord_features['obs_az']
    image = tfr.handle_image( image, config_args )

    true_dalt, true_daz = utils.get_delta_dir( src_alt, src_az, obs_alt, obs_az )


    scaled_labels = np.array( [true_dalt / config_args.alt_scale, true_daz / config_args.az_scale] )


    return image, scaled_labels


def get_training_batch( config_args, ds_dir, eval_data=None ):
    tfrecord_features = tfr.get_tfr_features( ds_dir, eval_data=eval_data )

    image, scaled_labels = get_train_input( tfrecord_features, config_args )

    # Ensure that the random shuffling has good mixing properties.
    min_fraction_of_examples_in_queue = 0.1
    min_queue_examples = int( config_args.train_events *
                              min_fraction_of_examples_in_queue )

    # Create a queue that shuffles the examples, and then
    # read 'batch_size' images + labels from the example queue.
    num_preprocess_threads = int( multiprocessing.cpu_count() * 0.7 )

    print( 'Filling queue with {0} event images.\nThis may take a few moments...'.format( min_queue_examples ) )
    batch_images, batch_true_dalt, batch_true_daz = tf.train.shuffle_batch([image, scaled_labels[0], scaled_labels[1] ],
                                                        batch_size = config_args.batch_size,
                                                        num_threads = num_preprocess_threads,
                                                        capacity = min_queue_examples * 2.0,
                                                        min_after_dequeue = min_queue_examples)
    batch_true_dalt = tf.reshape( batch_true_dalt, [config_args.batch_size] )
    batch_true_daz = tf.reshape( batch_true_daz, [config_args.batch_size] )
    batch_labels = tf.stack([batch_true_dalt,batch_true_daz])
    batch_labels = tf.transpose(batch_labels, [1,0])

    # Display the training images in the visualizer.
    tf.summary.image( 'images', batch_images )

    return batch_images, batch_labels


def get_test_batch( config_args, ds_dir ):
    tfrecord_features = tfr.get_tfr_features( ds_dir, eval_data='test', data_type=config_args.data )

    image, scaled_labels = get_train_input( tfrecord_features, config_args )

    (uid, particle, energy,
     ct1_loc_dis, ct2_loc_dis, ct3_loc_dis, ct4_loc_dis,
     ct1_length, ct2_length, ct3_length, ct4_length,
     ct1_width, ct2_width, ct3_width, ct4_width,
     ct1_size, ct2_size, ct3_size, ct4_size,
     src_alt, src_az, obs_alt, obs_az,
     obs_time, core_x, core_y) = tfr.get_test_example( tfrecord_features )

    # Ensure that the random shuffling has good mixing properties.
    min_fraction_of_examples_in_queue = 0.2
    min_queue_examples = int( config_args.train_events *
                              min_fraction_of_examples_in_queue )

    # Create a queue that shuffles the examples, and then
    # read 'batch_size' images + labels from the example queue.
    num_preprocess_threads = int( multiprocessing.cpu_count() * 0.7 )

    print( 'Filling queue with {0} event images.\nThis may take a few moments...'.format( min_queue_examples ) )

    (batch_images, batch_true_dalt, batch_true_daz,
     batch_uid, batch_particle, batch_energy,
     batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,
     batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,
     batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,
     batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,
     batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az,
     batch_obs_time, batch_core_x, batch_core_y) = tf.train.batch([image, scaled_labels[0], scaled_labels[1],
                                                                   uid, particle, energy,
                                                                   ct1_loc_dis, ct2_loc_dis, ct3_loc_dis, ct4_loc_dis,
                                                                   ct1_length, ct2_length, ct3_length, ct4_length,
                                                                   ct1_width, ct2_width, ct3_width, ct4_width,
                                                                   ct1_size, ct2_size, ct3_size, ct4_size,
                                                                   src_alt, src_az, obs_alt, obs_az,
                                                                   obs_time, core_x, core_y],
                                                                  batch_size=config_args.batch_size,
                                                                  num_threads=num_preprocess_threads,
                                                                  capacity=min_queue_examples * 2 )

    batch_true_dalt = tf.reshape(batch_true_dalt, [config_args.batch_size])
    batch_true_daz = tf.reshape(batch_true_daz, [config_args.batch_size])
    batch_labels = tf.stack([batch_true_dalt, batch_true_daz])
    batch_labels = tf.transpose(batch_labels, [1, 0])

    return (batch_images, batch_labels,
            tf.reshape( batch_uid, [config_args.batch_size] ),
            tf.reshape( batch_particle, [config_args.batch_size] ),
            tf.reshape( batch_energy, [config_args.batch_size] ),
            tf.reshape( batch_ct1_loc_dis, [config_args.batch_size] ),
            tf.reshape( batch_ct2_loc_dis, [config_args.batch_size] ),
            tf.reshape( batch_ct3_loc_dis, [config_args.batch_size] ),
            tf.reshape( batch_ct4_loc_dis, [config_args.batch_size] ),
            tf.reshape( batch_ct1_length, [config_args.batch_size] ),
            tf.reshape( batch_ct2_length, [config_args.batch_size] ),
            tf.reshape( batch_ct3_length, [config_args.batch_size] ),
            tf.reshape( batch_ct4_length, [config_args.batch_size] ),
            tf.reshape( batch_ct1_width, [config_args.batch_size] ),
            tf.reshape( batch_ct2_width, [config_args.batch_size] ),
            tf.reshape( batch_ct3_width, [config_args.batch_size] ),
            tf.reshape( batch_ct4_width, [config_args.batch_size] ),
            tf.reshape( batch_ct1_size, [config_args.batch_size] ),
            tf.reshape( batch_ct2_size, [config_args.batch_size] ),
            tf.reshape( batch_ct3_size, [config_args.batch_size] ),
            tf.reshape( batch_ct4_size, [config_args.batch_size] ),
            tf.reshape( batch_src_alt, [config_args.batch_size] ),
            tf.reshape( batch_src_az, [config_args.batch_size] ),
            tf.reshape( batch_obs_alt, [config_args.batch_size] ),
            tf.reshape( batch_obs_az, [config_args.batch_size] ),
            tf.reshape( batch_obs_time, [config_args.batch_size] ),
            tf.reshape( batch_core_x, [config_args.batch_size] ),
            tf.reshape( batch_core_y, [config_args.batch_size] ) )


def tower_loss( config_args, scope, model, ds_dir, tower_name, eval_data=None ):
    """Calculate the total loss on a single tower running the GammaNN model.
    Placing operations and variables on devices requires some special abstractions.

    The first abstraction we require is a function for computing inference and gradients
    for a single model replica. In the code we term this abstraction a "tower".
    We must set two attributes for each tower:
    1. A unique name for all operations within a tower. tf.name_scope provides
    this unique name by prepending a scope.
    For instance, all operations in the first tower are prepended with tower_0, e.g. tower_0/conv1/Conv2D.
    2. A preferred hardware device to run the operation within a tower. tf.device specifies this.
    For instance, all operations in the first tower reside within device('/gpu:0') scope
    indicating that they should be run on the first GPU.

    Args:
        config_args: The input arguments from the config file and the command line
        scope: unique prefix string identifying the HESSnn tower, e.g. 'tower_0'
        model: the model script in gnnlib
        ds_dir: the directory containing the tfrecord files and events and checkpoint folders
        tower_name: TOWER_NAME
        eval_data: data to evaluate
    Returns:
        Tensor of shape [] containing the total loss for a batch of data
    """
    logits = None
    batch_uid = batch_particle = batch_energy = None
    batch_ct1_loc_dis = batch_ct2_loc_dis = batch_ct3_loc_dis = batch_ct4_loc_dis = None
    batch_ct1_length = batch_ct2_length = batch_ct3_length = batch_ct4_length = None
    batch_ct1_width = batch_ct2_width = batch_ct3_width = batch_ct4_width = None
    batch_ct1_size = batch_ct2_size = batch_ct3_size = batch_ct4_size = None
    batch_src_alt = batch_src_az = batch_obs_alt = batch_obs_az = None
    batch_obs_time = batch_core_x = batch_core_y = None
    # Get event parameters
    if eval_data == 'test':
        (batch_images, batch_scaled_labels,
         batch_uid, batch_particle, batch_energy,
         batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,
         batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,
         batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,
         batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,
         batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az,
         batch_obs_time, batch_core_x, batch_core_y) = get_test_batch( config_args, ds_dir )
    else:
        batch_images, batch_scaled_labels = get_training_batch( config_args, ds_dir, eval_data )

    model_func = config_args.train_type + '_model'
    logits = getattr( model, model_func )( batch_images, config_args.batch_size, tower_name, pkeep = config_args.dropout )

    # Build the portion of the Graph calculating the losses. Note that we will
    # assemble the total_loss using a custom function below.
    _ = model.regression_loss( logits, batch_scaled_labels )

    # Assemble all of the losses for the current tower only.
    losses = tf.get_collection( 'losses', scope )

    # Calculate the total loss for the current tower.
    total_loss = tf.add_n( losses, name='total_loss' )

    # Attach a scalar summary to all individual losses and the total loss; do the
    # same for the averaged version of the losses.
    for l in losses + [ total_loss ]:
        # Remove 'tower_[0-9]/' from the name in case this is a multi-GPU training
        # session. This helps the clarity of presentation on tensorboard.
        loss_name = re.sub( '%s_[0-9]*/' % tower_name, '', l.op.name )
        tf.summary.scalar( loss_name, l )
    if eval_data == 'test':
        return (total_loss, logits, batch_scaled_labels, batch_uid, batch_energy,  # 0, 1, 2, 3, 4
                batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,  # 5, 6, 7, 8
                batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,  # 9, 10, 11, 12
                batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,  # 13, 14, 15, 16
                batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,  # 17, 18, 19, 20
                batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az,  # 21, 22, 23, 24
                batch_obs_time, batch_core_x, batch_core_y)  # 25, 26, 27
    return total_loss, logits, batch_scaled_labels  # , batch_scaled_labels
