import sys
import re
import multiprocessing
import tensorflow as tf
from gnnlib.data import read_tfr as tfr
from gnnlib.utils import utils


def get_train_input( tfrecord_features, config_args ):
    combined_image = tf.decode_raw( tfrecord_features['combined_image'], tf.float32 )
    ct1_image = tf.decode_raw( tfrecord_features['ct1_image'], tf.float32 )
    ct2_image = tf.decode_raw( tfrecord_features['ct2_image'], tf.float32 )
    ct3_image = tf.decode_raw( tfrecord_features['ct3_image'], tf.float32 )
    ct4_image = tf.decode_raw( tfrecord_features['ct4_image'], tf.float32 )
    tels_in_event = tfrecord_features['tels_in_event']
    ct1_size = tfrecord_features['ct1_size']
    ct2_size = tfrecord_features['ct2_size']
    ct3_size = tfrecord_features['ct3_size']
    ct4_size = tfrecord_features['ct4_size']
    src_alt = tfrecord_features['src_alt']
    src_az = tfrecord_features['src_az']
    obs_alt = tfrecord_features['obs_alt']
    obs_az = tfrecord_features['obs_az']
    core_x_ground = tfrecord_features[ 'core_x' ]
    core_y_ground = tfrecord_features[ 'core_y' ]
    num_images = tfrecord_features['num_images']
    ct1_image = tfr.handle_image( ct1_image, config_args )
    ct2_image = tfr.handle_image( ct2_image, config_args )
    ct3_image = tfr.handle_image( ct3_image, config_args )
    ct4_image = tfr.handle_image( ct4_image, config_args )
    combined_image = tfr.handle_image( combined_image, config_args )

    true_dalt, true_daz = utils.get_delta_dir( src_alt, src_az, obs_alt, obs_az )
    scaled_true_dalt = true_dalt / config_args.alt_scale
    scaled_true_daz = true_daz / config_args.az_scale
    core_x_ground = core_x_ground / config_args.core_scale
    core_y_ground = core_y_ground / config_args.core_scale

    return (scaled_true_dalt, scaled_true_daz, core_x_ground, core_y_ground, combined_image, ct1_image,
            ct2_image, ct3_image, ct4_image, ct1_size, ct2_size, ct3_size, ct4_size, num_images, tels_in_event)


def get_training_batch( config_args, ds_dir, eval_data=None ):
    tfrecord_features = tfr.get_tfr_features( ds_dir, eval_data=eval_data )
    
    (true_dalt, true_daz, core_x, core_y, combined_image, ct1_image, ct2_image, ct3_image, ct4_image,
     ct1_size, ct2_size, ct3_size, ct4_size, num_of_images, tels_in_evt) = get_train_input( tfrecord_features, config_args )
    
    # Stack sizes to make batch creation simple
    stacked_images = tf.stack([ct1_image, ct2_image, ct3_image, ct4_image], axis = 3)
    stacked_sizes = tf.stack([ct1_size, ct2_size, ct3_size, ct4_size])

    # Ensure that the random shuffling has good mixing properties.
    min_fraction_of_examples_in_queue = 0.02
    min_queue_examples = int( config_args.train_events * min_fraction_of_examples_in_queue )

    # Create a queue that shuffles the examples, and then
    # read 'batch_size' images + labels from the example queue.
    num_preprocess_threads = int( multiprocessing.cpu_count() * 0.7 )

    print( 'Filling  queue with {0} event images.\nThis may take a few moments...'.format( min_queue_examples ) )
    batch_combined_images, batch_stacked_images, batch_stacked_sizes,\
    batch_true_dalt, batch_true_daz, batch_core_x,\
    batch_core_y, batch_numtels, batch_pattern =                        tf.train.shuffle_batch([combined_image, stacked_images,
                                                                                                stacked_sizes, true_dalt, true_daz,
                                                                                                core_x, core_y,
                                                                                                num_of_images, tels_in_evt],
                                                                                                batch_size = config_args.batch_size,
                                                                                                num_threads = num_preprocess_threads,
                                                                                                capacity = min_queue_examples * 2.0,
                                                                                                min_after_dequeue = min_queue_examples)
    # Set sizes to zero for non-participating telescopes
    batch_pattern = tf.cast(batch_pattern[:, 0:4], tf.float32)
    bool_ind = tf.equal(batch_pattern, tf.zeros_like(batch_pattern))
    batch_pattern = tf.where(bool_ind, tf.zeros_like(batch_pattern), tf.ones_like(batch_pattern))
    batch_stacked_sizes = tf.multiply(batch_stacked_sizes, batch_pattern)

    # Sort images according to size
    # Additionally set images to zero that are not participating
    values, sort_indices = tf.nn.top_k(batch_stacked_sizes, k=4)
    ct1_im, ct2_im, ct3_im, ct4_im = tf.unstack(batch_stacked_images, axis=4)
    batch_sorted_images = []
    for evt in range(0, config_args.batch_size):
        image_list = [ct1_im[evt]*batch_pattern[evt, 0], ct2_im[evt]*batch_pattern[evt, 1], ct3_im[evt]*batch_pattern[evt, 2], ct4_im[evt]*batch_pattern[evt, 3]]
        image_list = tf.convert_to_tensor(image_list, dtype=tf.float32)
        sorted_image_list = tf.gather(image_list, sort_indices[evt])
        batch_sorted_images.append(sorted_image_list)

    batch_sorted_images = tf.convert_to_tensor(batch_sorted_images, dtype=tf.float32)  # Here we have gives (batch_size, channels, length, width, 1 (from stacking) )
    batch_sorted_images = tf.squeeze(tf.transpose( batch_sorted_images, [ 0, 2, 3, 1, 4 ] ))  # This gives (batch_size, length, width, channels)
    # Add images to summary
    tf.summary.image( 'combined_images', batch_combined_images )
    tf.summary.image( 'ct1_images', ct1_im )
    tf.summary.image( 'ct2_images', ct2_im )
    tf.summary.image( 'ct3_images', ct3_im )
    tf.summary.image( 'ct4_images', ct4_im )

    batch_true_dalt = tf.reshape( batch_true_dalt, [config_args.batch_size] )
    batch_true_daz = tf.reshape( batch_true_daz, [config_args.batch_size] )
    batch_core_x = tf.reshape( batch_core_x, [config_args.batch_size] )
    batch_core_y = tf.reshape( batch_core_y, [config_args.batch_size] )
    batch_labels = tf.stack([batch_true_dalt, batch_true_daz, batch_core_x, batch_core_y])
    batch_labels = tf.transpose(batch_labels, [1, 0])

    return batch_sorted_images, batch_labels, tf.reshape( batch_numtels, [config_args.batch_size] )


def get_test_batch( config_args, ds_dir ):
    tfrecord_features = tfr.get_tfr_features( ds_dir, eval_data='test', data_type=config_args.data )

    (true_dalt, true_daz, scaled_core_x, scaled_core_y, combined_image, ct1_image, ct2_image, ct3_image, ct4_image,
     ct1_size, ct2_size, ct3_size, ct4_size, num_of_images, tels_in_evt) = get_train_input( tfrecord_features, config_args )

    # Stack sizes to make batch creation simple
    stacked_images = tf.stack( [ ct1_image, ct2_image, ct3_image, ct4_image ], axis = 3 )
    stacked_sizes = tf.stack( [ ct1_size, ct2_size, ct3_size, ct4_size ] )

    (uid, particle, energy,
     ct1_loc_dis, ct2_loc_dis, ct3_loc_dis, ct4_loc_dis,
     ct1_length, ct2_length, ct3_length, ct4_length,
     ct1_width, ct2_width, ct3_width, ct4_width,
     ct1_size, ct2_size, ct3_size, ct4_size,
     src_alt, src_az, obs_alt, obs_az,
     obs_time, core_x, core_y) = tfr.get_test_example( tfrecord_features )

    # Ensure that the random shuffling has good mixing properties.
    min_fraction_of_examples_in_queue = 0.02
    min_queue_examples = int( config_args.train_events * min_fraction_of_examples_in_queue )

    # Create a queue that shuffles the examples, and then
    # read 'batch_size' images + labels from the example queue.
    num_preprocess_threads = int( multiprocessing.cpu_count() * 0.7 )

    print( 'Filling queue with {0} event images.\nThis may take a few moments...'.format( min_queue_examples ) )

    (batch_stacked_images, batch_true_dalt, batch_true_daz,
     batch_scaled_core_x, batch_scaled_core_y,
     batch_numtels, batch_pattern,
     batch_uid, batch_energy,
     batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,
     batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,
     batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,
     batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,
     batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az,
     batch_obs_time, batch_core_x, batch_core_y, batch_stacked_sizes) = tf.train.batch([stacked_images, true_dalt, true_daz,
                                                                                        scaled_core_x, scaled_core_y,
                                                                                        num_of_images, tels_in_evt,
                                                                                        uid, energy,
                                                                                        ct1_loc_dis, ct2_loc_dis, ct3_loc_dis, ct4_loc_dis,
                                                                                        ct1_length, ct2_length, ct3_length, ct4_length,
                                                                                        ct1_width, ct2_width, ct3_width, ct4_width,
                                                                                        ct1_size, ct2_size, ct3_size, ct4_size,
                                                                                        src_alt, src_az, obs_alt, obs_az,
                                                                                        obs_time, core_x, core_y, stacked_sizes],
                                                                                       batch_size=config_args.batch_size,
                                                                                       num_threads=num_preprocess_threads,
                                                                                       capacity=min_queue_examples * 2)

    # Set sizes to zero for non-participating telescopes
    batch_pattern = tf.cast(batch_pattern[:, 0:4], tf.float32)
    bool_ind = tf.equal(batch_pattern, tf.zeros_like(batch_pattern))
    batch_pattern = tf.where(bool_ind, tf.zeros_like(batch_pattern), tf.ones_like(batch_pattern))
    batch_stacked_sizes = tf.multiply(batch_stacked_sizes, batch_pattern)

    # Sort images according to size
    # Additionally set images to zero that are not participating
    # Really ugly, is there a better way?
    values, sort_indices = tf.nn.top_k(batch_stacked_sizes, k=4)
    ct1_im, ct2_im, ct3_im, ct4_im = tf.unstack(batch_stacked_images, axis=4)
    batch_sorted_images = []
    for evt in range(0, config_args.batch_size):
        image_list = [ct1_im[evt] * batch_pattern[evt, 0], ct2_im[evt] * batch_pattern[evt, 1],
                      ct3_im[evt] * batch_pattern[evt, 2], ct4_im[evt] * batch_pattern[evt, 3]]
        image_list = tf.convert_to_tensor(image_list, dtype=tf.float32)
        sorted_image_list = tf.gather(image_list, sort_indices[evt])
        batch_sorted_images.append(sorted_image_list)

    batch_sorted_images = tf.convert_to_tensor( batch_sorted_images, dtype = tf.float32 )  # Here we have gives (batch_size, channels, length, width, 1 (from stacking) )
    batch_sorted_images = tf.squeeze( tf.transpose( batch_sorted_images, [ 0, 2, 3, 1, 4 ] ) )  # This gives (batch_size, length, width, channels)

    batch_true_dalt = tf.reshape( batch_true_dalt, [config_args.batch_size] )
    batch_true_daz = tf.reshape( batch_true_daz, [config_args.batch_size] )
    batch_scaled_core_x = tf.reshape( batch_scaled_core_x, [ config_args.batch_size ] )
    batch_scaled_core_y = tf.reshape( batch_scaled_core_y, [ config_args.batch_size ] )
    batch_labels = tf.stack( [ batch_true_dalt, batch_true_daz, batch_scaled_core_x, batch_scaled_core_y ] )
    batch_labels = tf.transpose( batch_labels, [ 1, 0 ] )

    return (batch_sorted_images, batch_labels,
            tf.reshape(batch_numtels, [config_args.batch_size]),
            tf.reshape(batch_uid, [config_args.batch_size]),
            tf.reshape(batch_energy, [config_args.batch_size]),
            tf.reshape(batch_ct1_loc_dis, [config_args.batch_size]),
            tf.reshape(batch_ct2_loc_dis, [config_args.batch_size]),
            tf.reshape(batch_ct3_loc_dis, [config_args.batch_size]),
            tf.reshape(batch_ct4_loc_dis, [config_args.batch_size]),
            tf.reshape(batch_ct1_length, [config_args.batch_size]),
            tf.reshape(batch_ct2_length, [config_args.batch_size]),
            tf.reshape(batch_ct3_length, [config_args.batch_size]),
            tf.reshape(batch_ct4_length, [config_args.batch_size]),
            tf.reshape(batch_ct1_width, [config_args.batch_size]),
            tf.reshape(batch_ct2_width, [config_args.batch_size]),
            tf.reshape(batch_ct3_width, [config_args.batch_size]),
            tf.reshape(batch_ct4_width, [config_args.batch_size]),
            tf.reshape( batch_ct1_size, [ config_args.batch_size ] ), tf.reshape( batch_ct2_size, [ config_args.batch_size ] ),
            tf.reshape( batch_ct3_size, [ config_args.batch_size ] ), tf.reshape( batch_ct4_size, [ config_args.batch_size ] ),
            tf.reshape(batch_src_alt, [config_args.batch_size]), tf.reshape(batch_src_az, [config_args.batch_size]),
            tf.reshape(batch_obs_alt, [config_args.batch_size]), tf.reshape(batch_obs_az, [config_args.batch_size]),
            tf.reshape(batch_obs_time, [config_args.batch_size]), tf.reshape(batch_core_x, [config_args.batch_size]),
            tf.reshape(batch_core_y, [config_args.batch_size]),
            batch_stacked_sizes )


def tower_loss( config_args, scope, model, ds_dir, tower_name, eval_data=None ):
    """Calculate the total loss on a single tower running the HESSnn model.
    Placing operations and variables on devices requires some special abstractions.

    The first abstraction we require is a function for computing inference and gradients
    for a single model replica. In the code we term this abstraction a "tower".
    We must set two attributes for each tower:
    1. A unique name for all operations within a tower. tf.name_scope provides
    this unique name by prepending a scope.
    For instance, all operations in the first tower are prepended with tower_0, e.g. tower_0/conv1/Conv2D.
    2. A preferred hardware device to run the operation within a tower. tf.device specifies this.
    For instance, all operations in the first tower reside within device('/gpu:0') scope
    indicating that they should be run on the first GPU.

    Args:
        config_args: The input arguments from the config file and the command line
        scope: unique prefix string identifying the HESSnn tower, e.g. 'tower_0'
        model: the model script in gnnlib
        ds_dir: the directory containing the tfrecord files and events and checkpoint folders
        tower_name: TOWER_NAME
        eval_data: data to evaluate
    Returns:
        Tensor of shape [] containing the total loss for a batch of data
    """
    batch_uid = batch_energy = None
    batch_ct1_loc_dis = batch_ct2_loc_dis = batch_ct3_loc_dis = batch_ct4_loc_dis = None
    batch_ct1_length = batch_ct2_length = batch_ct3_length = batch_ct4_length = None
    batch_ct1_width = batch_ct2_width = batch_ct3_width = batch_ct4_width = None
    batch_ct1_size = batch_ct2_size = batch_ct3_size = batch_ct4_size = None
    batch_src_alt = batch_src_az = batch_obs_alt = batch_obs_az = None
    batch_obs_time = batch_core_x = batch_core_y = None

    # Get images and labels
    if eval_data == 'test' or config_args.data == 'real':
        config_args.dropout = 1.0
        (batch_sorted_images, batch_labels,
         batch_numtels,
         batch_uid, batch_energy,
         batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,
         batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,
         batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,
         batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,
         batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az,
         batch_obs_time, batch_core_x, batch_core_y, batch_stacked_sizes) = get_test_batch(config_args, ds_dir)
    else:
        batch_sorted_images, batch_labels, batch_numtels = get_training_batch( config_args, ds_dir, eval_data )

    # logits = model.class_model( batch_images, config_args.batch_size, tower_name, pkeep=config_args.dropout )
    model_func = config_args.train_type + '_model'
    if eval_data:
        is_training_bool = False
        logits = getattr( model, model_func )( batch_sorted_images, config_args.batch_size, tower_name, batch_numtels, is_training_bool,  pkeep=1.0 )
    else:
        is_training_bool = True
        logits = getattr( model, model_func )( batch_sorted_images, config_args.batch_size, tower_name, batch_numtels, is_training_bool, pkeep=config_args.dropout )

    # Build the portion of the Graph calculating the losses. Note that we will
    # assemble the total_loss using a custom function below.
    _ = model.loss( logits, batch_labels )

    # Assemble all of the losses for the current tower only.
    losses = tf.get_collection( 'losses', scope )

    # Calculate the total loss for the current tower.
    total_loss = tf.add_n( losses, name='total_loss' )

    # Attach a scalar summary to all individual losses and the total loss; do the
    # same for the averaged version of the losses.
    for l in losses + [ total_loss ]:
        # Remove 'tower_[0-9]/' from the name in case this is a multi-GPU training
        # session. This helps the clarity of presentation on tensorboard.
        loss_name = re.sub( '%s_[0-9]*/' % tower_name, '', l.op.name )
        tf.summary.scalar( loss_name, l )
    if eval_data == 'test':
        return (total_loss, logits, batch_labels, batch_uid, batch_energy,  # 0, 1, 2, 3, 4
                batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,  # 5, 6, 7, 8
                batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,  # 9, 10, 11, 12
                batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,  # 13, 14, 15, 16
                batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,  # 17, 18, 19, 20
                batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az,  # 21, 22, 23, 24
                batch_obs_time, batch_core_x, batch_core_y)  # 25, 26, 27
    return total_loss, logits, batch_labels  # , batch_labels
