import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord, ICRS
from astropy.time import Time
import scipy.ndimage as sp
from astropy.convolution import convolve
from astropy.convolution.kernels import Gaussian2DKernel, Tophat2DKernel
from scipy import optimize
from datetime import datetime

from gnnlib.utils import plot_utils as pltutil
from gnnlib.utils import utils

tar_ra = 329.7208
tar_dec = -30.2217

def calculateRaDec(df, df_path):

    event_time = df['obs_time'].values
    event_time = np.float64(event_time)

    #  HESS Lon = 16.5028 Lat = -23.27280 elevation = 1800 m
    hess_location = EarthLocation(lat=-23.27280 * u.deg, lon=16.5028 * u.deg, height=1800 * u.m)
    observation_time = Time(event_time, format='unix')

    pos_alt = df['pointing_alt'].values  # + 0.0078
    pos_az = df['pointing_az'].values

    print('%s: transforming pointing coordinates...' % datetime.now())
    pos_altaz_skycoord = SkyCoord(alt=pos_alt * u.deg, az=pos_az * u.deg, frame='altaz',
                                  obstime=observation_time, location=hess_location,
                                  pressure=1013 * u.hPa)

    pos_radec = pos_altaz_skycoord.transform_to(ICRS)
    df['pointing_Ra'] = pos_radec.ra.degree
    df['pointing_Dec'] = pos_radec.dec.degree

    print('%s: calculating the delta alt/az...' % datetime.now())
    pred_dalt = df['predicted_alt'].values - pos_alt
    pred_daz = df['predicted_az'].values - pos_az


    pred_tar_alt = pos_alt + pred_dalt
    pred_tar_az = pos_az + pred_daz

    print('%s: calculating the RaDec of the predictions...' % datetime.now())
    pred_tar_altaz = SkyCoord(alt=pred_tar_alt * u.deg, az=pred_tar_az * u.deg, frame='altaz',
                              obstime=observation_time, location=hess_location,
                              pressure=1013 * u.hPa)

    pred_tar_radec = pred_tar_altaz.transform_to(ICRS)

    true_tar_radec = SkyCoord(tar_ra * u.deg, tar_dec * u.deg, frame=ICRS,
                              obstime=observation_time, location=hess_location,
                              pressure=1013 * u.hPa)

    df['predicted_Ra'] = pred_tar_radec.ra.degree
    df['predicted_Dec'] = pred_tar_radec.dec.degree

    print('%s: calculating theta...' % datetime.now())
    theta = true_tar_radec.separation(pred_tar_radec)

    df['theta'] = theta.degree

    print('%s: writing extended csv to disc...' % datetime.now())
    df.to_csv(df_path)
    print('%s: done.' % datetime.now())
    return df




def skymap( df_path ):
    df = pd.read_csv( df_path )#
    #columns = df.columns.values.tolist()

    print('loaded ', df_path, '\nwith dimensions ', df.shape)

    if 'theta' in df.columns:
        print('RaDec and theta already calculated and read from csv!')
    else:
        print('%s: This is a fresh direction csv!\nWill now calculate the RaDec stuff... please be patient :)\n' % datetime.now())
        df = calculateRaDec(df, df_path)

    pred_ra = df['predicted_Ra']
    pred_dec = df['predicted_Dec']
    pointing_ra = df['pointing_Ra']
    pointing_dec = df['pointing_Dec']
    theta = df['theta']

    print('true source position',tar_ra, tar_dec)
    mean_predicted_ra = np.mean(pred_ra)
    mean_predicted_dec = np.mean(pred_dec)
    delta_ra = tar_ra - mean_predicted_ra
    delta_dec = tar_dec - mean_predicted_dec
    print('Predicted mean source position', mean_predicted_ra, mean_predicted_dec)
    print('Delta source position', delta_ra, delta_dec)



    print('Calculating r68...')
    theta_sorted = np.sort(theta)
    if len(theta_sorted) > 1000:
        r68 = theta_sorted[int(len(theta_sorted) * 0.68)]
        print('r68 = ', r68)
    else:
        raise SystemExit('less then 1000 events!\ntheta calculation not safe!\nAborting!\n')


    '''
    plt.hist(pred_tar_radec.ra.degree, bins=30)
    plt.title( 'PKS-2155 Count map. R.A. projection'.format( round( r68, 5 ) ) )
    plt.xlabel( 'R.A. [deg]' )
    plt.show()

    plt.hist( pred_tar_radec.dec.degree, bins = 30 )
    plt.title( 'PKS-2155 Count map. Dec. projection'.format( round( r68, 5 ) ) )
    plt.xlabel( 'Dec. [deg]' )
    plt.show()

    plt.hist(np.square(theta_deg), bins=200, range=(0.0, 0.5))
    plt.xlabel('Theta^2 [deg^2]')
    plt.show()
    '''

    #plt.scatter( pred_tar_radec.ra, pred_tar_radec.dec,  s=0.6 )
    #plt.scatter( 329.7208, -30.2217 , c='r', s=30, marker='x' )
    #axes = plt.gca()
    #axes.set_title('PKS-2155 skymap. R68 = {0} deg'.format(r68))
    #axes.set_xlim( [ tar_ra - 1, tar_ra + 1 ] )
    #axes.set_ylim( [ tar_dec - 1, tar_dec + 1 ] )
    #plt.show()
    #fig1 = plt.subplot( 2, 2, 1 )

    print('Plotting Skymaps...')
    plt.hist2d( pred_ra, pred_dec, bins=600, range=np.array( [ (329.7208 - 3, 329.7208 + 3), (-30.2217 - 3, -30.2217 + 3) ] ) )
    plt.title( 'PKS-2155 Count map. R68 = {0} deg'.format(round(r68, 5)) )
    plt.xlabel( 'R.A. [deg]' )
    plt.ylabel( ('Dec. [deg]') )
    cb = plt.colorbar()
    cb.set_label( 'Counts' )
    #plt.clim( 0, 8 )
    plt.scatter( 329.7208, -30.2217, c = 'r', s = 200, marker = 'x',alpha=0.4, facecolors='none' )
    plt.show()

    #X = sp.filters.gaussian_filter( pred_tar_radec.ra, sigma = 2, order = 0 )
    #Y = sp.filters.gaussian_filter( pred_tar_radec.dec, sigma = 2, order = 0 )
    heatmap, yedges, xedges, = np.histogram2d( pred_dec, pred_ra, bins = 600, range=np.array( [ (-30.2217 - 3, -30.2217 + 3), (329.7208 - 3, 329.7208 + 3) ] ) )
    extent = [ xedges[ 0 ], xedges[ -1 ], yedges[ 0 ], yedges[ -1 ] ]

    #fig1 = plt.subplot( 2, 2, 2 )
    stddev = 3.5
    radius = int(stddev)
    kernel = Gaussian2DKernel(stddev=stddev)
    tophat = Tophat2DKernel( radius=3 )
    gauss_image = convolve(heatmap, kernel)
    print(gauss_image.shape)
    params = fitgaussian( gauss_image )
    fit = gaussian( *params )
    (height, x, y, width_x, width_y) = params
    print(height, x, y, width_x, width_y)
    print('Ra = ', (xedges[ -1 ] - xedges[0])*x/600 + xedges[0], 'Dec = ', (yedges[-1] - yedges[0])*y/600 + yedges[0])
    plt.imshow(gauss_image, interpolation='none', extent=extent)
    plt.title( 'PKS-2155 Count map. Gaussian filter std = {0}'.format(stddev) )
    plt.xlabel('R.A. [deg]')
    plt.ylabel(( 'Dec. [deg]' ))
    cb = plt.colorbar()
    cb.set_label( 'Counts' )
    plt.scatter( 329.7208, -30.2217, c = 'r', s = 200, marker = 'x',alpha=0.4, facecolors='none' )
    plt.show()

    plt.imshow( convolve( heatmap, tophat ), interpolation = 'none', extent = extent )
    plt.title( 'PKS-2155 Count map. TopHat filter radius = {0}'.format( radius ) )
    plt.xlabel( 'R.A. [deg]' )
    plt.ylabel( ('Dec. [deg]') )
    cb = plt.colorbar()
    cb.set_label( 'Counts' )
    plt.scatter( 329.7208, -30.2217, c = 'r', s = 200, marker = 'x',alpha=0.4, facecolors='none' )
    plt.show()


def gaussian(height, center_x, center_y, width_x, width_y):
    """Returns a gaussian function with the given parameters"""
    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*np.exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)


def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    total = data.sum()
    X, Y = np.indices(data.shape)
    x = (X*data).sum()/total
    y = (Y*data).sum()/total
    col = data[:, int(y)]
    width_x = np.sqrt(np.abs((np.arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = np.sqrt(np.abs((np.arange(row.size)-x)**2*row).sum()/row.sum())
    height = data.max()
    return height, x, y, width_x, width_y


def fitgaussian(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: np.ravel(gaussian(*p)(*np.indices(data.shape)) -
                                 data)
    p, success = optimize.leastsq(errorfunction, params)
    return p