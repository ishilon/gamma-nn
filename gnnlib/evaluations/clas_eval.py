import math
import os
from datetime import datetime
import numpy as np
import pandas as pd
import tensorflow as tf

from gnnlib.nn import clas_model as model
from gnnlib.tasks import clas as task
from gnnlib.utils import utils
from gnnlib.utils import eval_utils as evu
from gnnlib.utils import eval_clas_utils as plot_evu


def test_eval(config_args,
              ckpt_dir,
              saver,
              summary_writer,
              test_tuple,
              test_examples):
    """
    Run test set evaluation
    :param config_args:
    :param ckpt_dir:
    :param saver:
    :param test_examples:
    :param summary_writer:
    :return:
    """
    # a TF op to get the softmax activations for each class
    sftmx = tf.nn.softmax( test_tuple[1] )
    # Calculate predictions. Says whether the targets (labels) are in the top K predictions
    k = 1
    t_p = tf.nn.in_top_k( sftmx, test_tuple[2], k )

    config = tf.ConfigProto( allow_soft_placement = True )
    with tf.Session( config = config ) as sess:
        global_step = evu.load_checkpoint( ckpt_dir, sess, saver )
        # Start the queue runners.
        coord = tf.train.Coordinator()
        threads = []

        col_name = 'zeta'
        test_df = pd.DataFrame( columns = [ 'uid', 'true_energy', col_name, 'top_1', 'true_label' ] )

        try:
            for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
                threads.extend( qr.create_threads( sess, coord=coord, daemon=True, start=True) )

            num_iter = int( math.ceil( test_examples / config_args.batch_size ) )
            print( 'Number of batches: ', num_iter, 'Total number of test_examples:', test_examples )
            total_loss = 0
            step = 0
            true_positives = 0
            num = test_examples % config_args.batch_size

            while step < num_iter and not coord.should_stop():
                (batch_loss, batch_top_1, batch_sftmx, batch_true_labels, batch_uid,  batch_energy) = sess.run( [ test_tuple[0], t_p, sftmx, test_tuple[2],
                                                                                                                  test_tuple[3], test_tuple[4] ] )

                # Don't include events that are already listed
                # print(step)
                if step == num_iter - 1 and num != 0:
                    num = test_examples % config_args.batch_size
                    print( 'Evaluating last batch, containing {0} events'.format(num) )
                    batch_top_1 = batch_top_1[: num]
                    batch_sftmx = batch_sftmx[: num]
                    batch_true_labels = batch_true_labels[: num]
                    batch_uid = batch_uid[: num]
                    batch_energy = batch_energy[: num]

                batch_df = pd.DataFrame( { 'uid': batch_uid, 'true_energy': batch_energy, col_name: batch_sftmx[:, 0], 'top_1': batch_top_1, 'true_label': batch_true_labels } )
                test_df = test_df.append( batch_df, ignore_index = True )
                total_loss += batch_loss
                true_positives += np.sum( batch_top_1 )
                step += 1

            evu.print_loss( total_loss, num_iter, sess, summary_writer, global_step )
            # Compute precision @ 1.
            precision = true_positives / test_examples
            print( '{0} images classified correctly, out of {1} images in the dataset.'.format( true_positives, test_examples ) )
            print( '{0}: Precision @ 1 = {1}%'.format( datetime.now(), round( precision * 100, 2 ) ) )

        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join( threads, stop_grace_period_secs=10 )

        # print('freezing your model...')
        # evu.freeze_model( sess=sess, output_graph_file=os.path.join( ckpt_dir, 'frozen_model.pb' ), output_node_names=[ 'logits_scope/output_node' ] )

        return test_df


def main(config_args,
         eval_data,
         model_dir,
         ds_dir,
         eval_dir,
         ckpt_dir,
         num_examples
         ):
    """
    different Evals for the direction reco steps the HESSnn network for a specific chckpt file.
    :param config_args:
    :param eval_data:
    :param model_dir:
    :param ds_dir:
    :param eval_dir:
    :param ckpt_dir:
    :param num_examples:
    :return:
    """
    tmp_dict = model.get_meta_consts()
    moving_average_decay = tmp_dict[ 'moving_average_decay' ]
    tower_name = utils.get_tower_name()

    run_type = config_args.train_type
    csv_dir = model_dir + 'csv_files/'
    if not os.path.exists( csv_dir ):
        os.mkdir( csv_dir )
    df_file_path = os.path.join( model_dir, 'csv_files/', run_type + '_test_df.csv' )
    test_df = None

    run_test = False
    if eval_data == 'test':
        if not os.path.isfile( df_file_path ) or config_args.redo_test_bool:
            run_test = True

    print( '\n***********************************' )
    print( 'Starting {0} {1} data evaluation'.format( config_args.train_type, eval_data ) )
    print( '***********************************\n' )

    if eval_data != 'test' or run_test:
        loss = logits = test_tuple = None
        true_labels = None
        with tf.Graph().as_default() as graph:
            # Calculate loss
            with tf.variable_scope( tf.get_variable_scope() ):
                for i in range( config_args.num_gpus ):
                    with tf.device( '/gpu:%d' % i ):
                        with tf.name_scope( '%s_%d' % (tower_name, i) ) as scope:
                            # Calculate the loss for one tower of the HESSnn model. This function
                            # constructs the entire HESSnn model but shares the variables across
                            # all towers.
                            if eval_data == 'test':
                                test_tuple = task.tower_loss( config_args, scope, model, ds_dir, tower_name, eval_data )
                            else:
                                loss, logits, true_labels = task.tower_loss( config_args, scope, model, ds_dir, tower_name, eval_data )
                            # Reuse variables for the next tower.
                            # This function lets get_variable() use an existing variable,
                            # rather than initialising a new one, with the name Variable_scope/Variable_name.
                            tf.get_variable_scope().reuse_variables()

            # Restore the moving average version of the learned variables for eval.
            variable_averages = tf.train.ExponentialMovingAverage( moving_average_decay )
            variables_to_restore = variable_averages.variables_to_restore()
            saver = tf.train.Saver( variables_to_restore )

            # Build the summary operation based on the TF collection of Summaries.
            summary_writer = tf.summary.FileWriter( eval_dir, graph )

            if eval_data == 'test':
                test_df = test_eval(config_args, ckpt_dir, saver, summary_writer, test_tuple, num_examples)

                test_df = test_df.sort_values( by = [ 'true_energy', 'uid' ], ascending = [ True, True ] ).reset_index( drop=True )
                print('Writing test data to csv file.')
                test_df.to_csv( df_file_path )
            else:
                evu.eval_once_classification( ckpt_dir, saver, summary_writer, loss, logits, true_labels, num_examples, config_args.batch_size )

    else:
        test_df = pd.read_csv( df_file_path )

    if eval_data == 'test':
        plot_evu.eval_clas_csv( test_df, config_args )

if __name__ == '__main__':
    tf.app.run()
