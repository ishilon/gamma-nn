import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from gnnlib.utils import plot_utils as pltutil
from gnnlib.utils import utils


def integrate_hist( hist, bins, containment_r, bin1=0 ):
    # Assumes equally binned histogram
    integral = 0
    i = 0
    while integral < containment_r:
        integral += hist[i]
        print(i, hist[i], integral)
        i += 1
    print( 'Integral = ', integral )
    bin_width = bins[ 2 ] - bins[ 1 ]
    r2 = bin_width * (i) - bin_width * 0.5
    print( 'r2 = ', r2)
    r68 = np.sqrt( r2 )
    print( 'R{0} = {1} deg'.format( str( int( containment_r * 100 ) ), r68 ) )
    return integral, r68

def histedges_equalN( col, nbin ):
    '''
    Get histogram with nbin bins with equal number of entries in each bin
    :param col: Numpy array to use for equal binning
    :param nbin: Number of bins
    :return: A numpy array with the bins edges
    '''
    npt = len( col )
    return np.interp( np.linspace( 0, npt, nbin + 1 ),
                      np.arange( npt ),
                      np.sort( col ) )

def theta_sqr( alt_df_path, az_df_path ):
    if az_df_path == None:
        df = pd.read_csv( alt_df_path )
    else:
        merged_path = '/data/hessnn_data/models/direction-reco/konrad.mc_dst_hd_hap15-12.corsika74100_qgs2-04_urqmd1.3cr/phase2b5/gamma_pointsource.south.20deg.0.5deg.0.005-150tev/add_images_14.h1_sampling_rebin.h1_resolution_100.ct5_sampling_none.ct5_resolution_0.model_inference/2000.0k'
        if os.path.exists( merged_path + '/merged_test_eval.csv' ):
            df = pd.read_csv( merged_path + '/merged_test_eval.csv' )
            print('Loaded merged file!')
        else:
            alt_df = pd.read_csv( alt_df_path )
            az_df = pd.read_csv( az_df_path )
            #length = len(alt_df['true_energy'].values)
            #z = np.zeros(length)
            #az_df = pd.DataFrame({'predicted_daz': z})

            df = pd.concat( [alt_df, az_df['predicted_daz'] ], axis=1 )
            df['predicted_daz'] = np.zeros_like(df['predicted_dalt'])
            total = 0
            broken = 0
            fixed = 0
            for index, uid in enumerate(df['uid']):
                total += 1
                if uid == az_df['uid'][index]:
                    df.loc[index, 'predicted_daz'] = az_df['predicted_daz'][index]
                elif df['uid'][index + 1] == az_df['uid'][index]:
                    df.loc[index + 1, 'predicted_daz'] = az_df['predicted_daz'][index]
                    #print('event mixed +1 but could be fixed')
                    broken += 1
                    fixed +=1
                elif df['uid'][index - 1] == az_df['uid'][index]:
                    df.loc[index - 1, 'predicted_daz'] = az_df['predicted_daz'][index]
                    #print('event mixed -1 but could be fixed')
                    broken += 1
                    fixed +=1
                else:
                    #print('could not fix event', uid)
                    broken +=1
            print('Out of %i events: %i were broken, but %i could be easily fixed' % (total, broken, fixed))
            if float(fixed)/broken < 0.9:
                print('Thats less than 90%% fixed...')
                utils.exit_point(30.0)

    true_dalt = df['true_dalt'].values
    #true_dalt = np.ones(df.shape[0])
    true_daz = df['true_daz'].values
    #true_daz = np.zeros(df.shape[0])

    offs = utils.great_circle_distance( true_dalt + 69.995209, 69.995209, true_daz + 180.0, 180.0 )
    print('OFFSET')
    #print(offs)
    print('mean_os = ', np.mean(offs))
    print('max os = ', np.amax(offs))
    print('min os = ', np.amin(offs))

    print('max alt = ', np.amax(true_dalt ))
    print( 'min alt = ', np.amin( true_dalt  ) )
    print( 'max az = ', np.amax( true_daz  ) )
    print( 'min az = ', np.amin( true_daz  ) )

    #pred_dalt = true_dalt +0.1
    #pred_daz = true_daz
    pred_dalt = df[ 'predicted_dalt' ].values
    pred_daz = df[ 'predicted_daz' ].values

    #print( "true_dalt - pred_dalt: ", true_dalt - pred_dalt )
    #print( "true_daz - pred_daz: ",  true_daz - pred_daz)

    f, (ax1, ax2) = plt.subplots(1,2)
    ax1.hist2d(true_daz, true_dalt, bins=300, range=np.array( [ (-3, 3), (-1, 1) ] ) )
    ax2.hist2d(pred_daz, pred_dalt, bins=300, range=np.array( [ (-3, 3), (-1, 1) ] ) )
    plt.show()


    offset = input( 'Please enter the offset of the test set as x.x (if diffuse, use 0.0): ' )
    theta = None
    if offset == '1.0':
        theta = utils.great_circle_distance( true_dalt + 68.994942, pred_dalt + 68.994942, true_daz + 180.0, pred_daz + 180.0 ) #68.994942
    elif offset == '0.0':
        theta = utils.great_circle_distance( true_dalt + 69.995209, pred_dalt + 69.995209, true_daz + 180.0, pred_daz + 180.0 )
    elif offset =='0.5':
        theta = utils.great_circle_distance( true_dalt + 69.495079, pred_dalt + 69.495079, true_daz + 180.0, pred_daz + 180.0 )
    elif offset =='1.5':
        theta = utils.great_circle_distance( true_dalt + 68.494812, pred_dalt + 68.494812, true_daz + 180.0, pred_daz + 180.0 )
    elif offset =='0.7':
        theta = utils.great_circle_distance( true_dalt + 69.295029, pred_dalt + 69.295029, true_daz + 180.0, pred_daz + 180.0 )


    theta2 = np.square( theta )
    df['theta2'] = theta2
    tdf = pd.DataFrame({'theta': theta})
    print(tdf.describe(percentiles=[.01, .05, .50, .68, .95, .99]))

    theta2_srt = np.sort(theta2)
    r68 = np.sqrt(theta2_srt[int(len(theta2_srt) * 0.68)])
    print('R68 = ', r68, '\n')

    hist, bins = pltutil.plot_histogram( 'theta2 - all events', theta2, 1500, 'theta2 [deg^2]', 0.0, 2.5, 'theta2', weights_bool = True )
    #_, _ = integrate_hist( hist, bins, 0.68 )
    plt.show()


    '''
    theta2_presel = df.loc[ (df['true_energy'] > 1) & (df['true_energy'] < 30.0) ]['theta2'].values

    print( 'theta2_presel = ', theta2_presel )
    print( 'mean_theta2 = ', np.mean( theta2_presel ) )
    print( 'std_theta2 = ', np.std( theta2_presel ) )
    print( 'median theta2 = ', np.median( theta2_presel ) )
    theta2_presel_srt = np.sort( theta2_presel )
    r68_presel = np.sqrt(theta2_presel_srt[ int( len( theta2_presel_srt ) * 0.68 ) ])
    print( 'Presel R68 = ', r68_presel, '\n' )

    hist_presel , bins_presel = pltutil.plot_histogram( 'theta2 - presel events', theta2_presel, 1500, 'theta2 [deg^2]', 0, 2.5, 'theta2', weights_bool = True )
    #_, _ = integrate_hist( hist_presel, bins_presel, 0.68 )
    plt.show()
    '''

    hillas_std_en = [0.1250963772, 0.1987088363, 0.3144625137, 0.4995067128, 0.7904839033, 1.2509637717,
                    1.9870883626, 3.1446251369, 5.0137430682, 7.9938366131, 12.6504789761, 20.0197509744,
                    31.6818382793, 50.1374306823, 79.048390333]
    hillas_std_r68 = [0.1476467619, 0.1248878969, 0.1091959964, 0.0963125561, 0.0898618787, 0.0857668072,
                      0.0792253827, 0.0718687031, 0.0614313221, 0.0550709512, 0.0520630824, 0.0516826202,
                      0.0500337548, 0.0486566901, 0.0557055933]
    '''
    hillas_hard_en = [0.1982080437, 0.3149473948, 0.4982876059, 0.7912048935, 1.2513239776, 1.9860403279,
                      3.1406356332, 4.9868304312, 7.8883475043, 12.5214793477, 19.8756611352, 31.4334022647,
                      49.8941929808, 78.5904437315]
    hillas_hard_r68 = [0.1100298153, 0.0868179361, 0.0762902584, 0.0682992452, 0.0657444021, 0.0671758296,
                       0.0671577975, 0.0592573846, 0.0527161243, 0.0516107408, 0.0506865582, 0.0486753174,
                       0.0481135364, 0.0526256705]
    '''
    impact_en = [0.0994743631, 0.1249825345, 0.198470832, 0.3139893603, 0.4986725221, 0.7917258769,  1.2568219806,
                1.9875241807, 3.1549273173, 4.9905562983, 7.9235752778, 12.5788307575, 19.8958832593, 31.4624627919,
                 49.7514466064, 78.9567368484]
    impact_r68 = [0.1128655489, 0.0914749136, 0.081581292, 0.0722314195, 0.0597103866, 0.0541655839, 0.0516105942,
                  0.0511395602, 0.0496717752, 0.0432211152, 0.0370421098, 0.0334905157, 0.0288518636, 0.0288338315,
                  0.0296312029, 0.0327840379]

    berlin_cnn_en = [0.1265425034, 0.199673067, 0.316227766, 0.5026641996, 0.7990167995, 1.2561500893, 1.9967306701,
                     3.1622776602, 5.0081867073, 7.9608321374, 12.6542503352, 19.8939968449, 31.5066737523, 50.2664199556,
                     79.6083213737]
    berlin_cnn_r68 = [0.1117180617, 0.0934801762, 0.081938326, 0.07030837, 0.0637885463, 0.062907489, 0.0607048458,
                      0.0616740088, 0.0562114537, 0.0464317181, 0.0416740088, 0.0391189427, 0.0415859031, 0.0488986784,
                      0.0617621145]

    ecap_benchmark_r68 = [0.11796658,  0.09424156,  0.08067457,  0.06782155,  0.06074481,  0.05844301,
                          0.05839948,  0.0565086,   0.05121413,  0.04813277,  0.04405219,  0.0402832,
                          0.0425284,   0.04686736,  0.04619445]





    e = df['true_energy'].values
    e = np.sort(e)
    #_,bins,_ = plt.hist(e, histedges_equalN(e, 15))

    _ , bins = np.histogram(np.log10(e), 17)
    bins = np.power(10, bins)
    '''
    equal distance 18 points
    bins = [3.47359590e-02 ,  5.40695740e-02 ,  8.41640453e-02  , 1.31008736e-01,
            2.03926614e-01 ,  3.17429703e-01 ,  4.94107238e-01 ,  7.69121355e-01,
            1.19720500e+00  , 1.86355484e+00 ,  2.90078694e+00 ,  4.51532988e+00,
            7.02850792e+00 ,  1.09404905e+01  , 1.70298352e+01 ,  2.65084357e+01,
            4.12627105e+01  , 6.42290365e+01  , 9.99781418e+01]
    17 points
    bins = [3.47359590e-02   5.54954376e-02   8.86615392e-02   1.41648915e-01
   2.26303483e-01   3.61550715e-01   5.77626636e-01   9.22837423e-01
   1.47435879e+00   2.35548949e+00   3.76321609e+00   6.01225155e+00
   9.60539279e+00   1.53459266e+01   2.45172131e+01   3.91695956e+01
   6.25787774e+01   9.99781418e+01]
    '''

    bins = [0.1, 0.16, 0.25,   0.40 ,   5.77626636e-01,   9.22837423e-01,
            1.47435879e+00,   2.35548949e+00,   3.76321609e+00,   6.01225155e+00,
            9.60539279e+00,   1.53459266e+01,   2.45172131e+01,   3.91695956e+01,
            6.25787774e+01,   70]


    true_off = utils.great_circle_distance(69.995209, true_dalt + 69.995209, 180.0, true_daz + 180.0)
    df['true_offset'] = true_off

    plt.close()
    plt.figure()

    r68_e = []
    en = []
    for i in range(len(bins) - 1):
        en.append(bins[i] + (bins[i + 1] - bins[i]) / 2)
        theta2_e = df.loc[(df['true_energy'] > bins[i]) & (df['true_energy'] < bins[i + 1])]['theta2'].values
        theta2_e_sorted = np.sort(theta2_e, kind='heapsort')
        r68 = np.sqrt(theta2_e_sorted[int(theta2_e_sorted.size * 0.68)]) if len(theta2_e_sorted) > 3 else None
        r68_e.append(r68)

        print('E %.2f TeV: #Events in bin: %i' %(en[i], len(theta2_e_sorted)))

    r68_e = np.asarray(r68_e)
    en = np.sort(np.asarray(en))
    plt.plot(en, r68_e, "x-", label='CNN')
    #plt.plot(berlin_cnn_en, berlin_cnn_r68, "o-", label='CNN_berlin')
    plt.plot(en, ecap_benchmark_r68, "-", label='CNN_old_benchmark')
    plt.plot(hillas_std_en, hillas_std_r68, label='hillas_std')
    #plt.plot(hillas_hard_en, hillas_hard_r68, label='hillas_hard')
    plt.plot(impact_en, impact_r68, label='ImPACT')
    plt.title('R68(E) preselected events - pointlike', fontsize = 12 )
    plt.xlabel('E [TeV]', fontsize = 12 )
    plt.xscale("log")
    plt.ylabel(r"R$_{68}$ [deg]", fontsize = 12 )
    plt.legend(loc="upper right", prop={'size': 12})
    plt.show()
    #print('r68_e = ', r68_e)
    #print('energy = ', en)


    plt.close()
    plt.figure()
    offsets = [0.1, 0.5, 1.0, 1.5]
    for ofs in offsets:
        r68_e = []
        en = []
        for i in range(len(bins) - 1):
            en.append(bins[i] + (bins[i + 1] - bins[i]) / 2)
            if ofs < 0.5:
                theta2_e = df.loc[(df['true_energy'] > bins[i]) & (df['true_energy'] < bins[i + 1]) & (
                    df['true_offset'] > (0) ) & (df['true_offset'] < (0.3) )]['theta2'].values
                label = r"$\theta$ = 0 to 0.3 deg"
            else:
                theta2_e = df.loc[(df['true_energy'] > bins[i]) & (df['true_energy'] < bins[i + 1]) & (
                    df['true_offset'] > (ofs - 0.1)) & (df['true_offset'] < (ofs + 0.1))]['theta2'].values
                label = r"$\theta$ = %.1f +/- 0.2 deg" % ofs
            theta2_e_sorted = np.sort(theta2_e, kind='heapsort')
            r68 = np.sqrt(theta2_e_sorted[int(theta2_e_sorted.size * 0.68)]) if len(theta2_e_sorted)>3 else None
            r68_e.append(r68)
        r68_e = np.asarray(r68_e)
        en = np.sort(np.asarray(en))
        plt.plot(en, r68_e, "x-", label= label )
        print('offset', ofs, '\nr68_e =\n', r68_e)
    #plt.plot(berlin_cnn_en, berlin_cnn_r68, "o", label='CNN_berlin')
    plt.plot(hillas_std_en, hillas_std_r68, label='hillas_std')
    #plt.plot(hillas_hard_en, hillas_hard_r68, label='hillas_hard')
    plt.plot(impact_en, impact_r68, label='ImPACT')
    plt.title('R68(E)')
    plt.xlabel('E [TeV]')
    plt.xscale("log")
    plt.ylabel(r"R$_{68}$ [deg]")
    plt.legend()
    plt.show()


    '''
    'ct1_loc_dis', 'ct2_loc_dis', 'ct3_loc_dis', 'ct4_loc_dis',
                     'ct1_size', 'ct2_size', 'ct3_size', 'ct4_size'
    '''
    plt.close()
    plt.figure()


    for ld_cut in [10, 0.7, 0.65, 0.625, 0.6, 0.575, 0.55, 0.525, 0.5, 0.475, 0.45, 0.4, 0.35, 0.3, 0.25, 0.2, 0.15]:

        df['ct1'] = np.zeros_like(df['uid'])
        df['ct2'] = np.zeros_like(df['uid'])
        df['ct3'] = np.zeros_like(df['uid'])
        df['ct4'] = np.zeros_like(df['uid'])
        df['tels'] = np.zeros_like(df['uid'])

        df.loc[(df.ct1_loc_dis < ld_cut) & (df.ct1_loc_dis > 0), 'ct1'] = 1
        df.loc[(df.ct2_loc_dis < ld_cut) & (df.ct2_loc_dis > 0), 'ct2'] = 1
        df.loc[(df.ct3_loc_dis < ld_cut) & (df.ct3_loc_dis > 0), 'ct3'] = 1
        df.loc[(df.ct4_loc_dis < ld_cut) & (df.ct4_loc_dis > 0), 'ct4'] = 1

        df['tels'] = df['ct1'] + df['ct2'] + df['ct3'] + df['ct4']

        print(df.shape)
        df = df[df.tels >=2]


        r68_e = []
        en = []
        for i in range(len(bins) - 1):
            en.append(bins[i] + (bins[i + 1] - bins[i]) / 2)
            theta2_e = df.loc[(df['true_energy'] > bins[i]) & (df['true_energy'] < bins[i + 1])]['theta2'].values
            theta2_e_sorted = np.sort(theta2_e, kind='heapsort')
            r68 = np.sqrt(theta2_e_sorted[int(theta2_e_sorted.size * 0.68)])  if len(theta2_e_sorted)>3 else None
            r68_e.append(r68)
        r68_e = np.asarray(r68_e)
        en = np.sort(np.asarray(en))
        plt.plot(en, r68_e, "x-", label=str(ld_cut))
        print('ld cut', ld_cut, '\nr68_e =\n', r68_e)
    #plt.plot(berlin_cnn_en, berlin_cnn_r68, "o", label='CNN_berlin')
    plt.plot(hillas_std_en, hillas_std_r68, label='hillas_std')
    #plt.plot(hillas_hard_en, hillas_hard_r68, label='hillas_hard')
    plt.plot(impact_en, impact_r68, label='ImPACT')
    plt.title('R68(E) local distance cuts')
    plt.xlabel('E [TeV]')
    plt.xscale("log")
    plt.ylabel(r"R$_{68}$ [deg]")
    plt.legend()
    plt.show()


    he_lim = 5.0
    he = df.loc[ df['true_energy'] > he_lim ]['true_energy'].values
    print( 'Ratio of events above {0} TeV out of {1} events = {2}%'.format( he_lim, len(theta2), round(len(he)*100/len(theta2), 2) ) )
