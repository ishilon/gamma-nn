import os
import shutil
import re
from hnnlib.utils import utils

'''
This script is used to help creating specific test sets from a folderof h5DSTs where some of the files have been used for training.
The list of DSTs used for training is taken from a dst_list.txt file in the model folder.
The script will create a new directory, called "hide", in the h5DST folder and move to it the files that should be excluded from the test set.
'''

model_dir = input( 'Please enter model directory:' )
l = input( 'Please enter file numbers:' )
print(model_dir)

for i in l:
    if i == ' ' or i == ',':
        continue
    with open( model_dir + '/dsts_list_{0}.txt'.format(i), 'r' ) as f:
        print( '\nprocessing dst_list_{0}.txt'.format(i) )
        dst_list = f.read().splitlines()
        if 'pointsource' in dst_list[2]:
            ind = re.search( r'\d{1}.\d{1}deg', dst_list[2] ).end()
            directory = dst_list[2][:ind] + '/'
        elif 'diffuse' in dst_list[2]:
            ind = re.search( r'\d{2}deg' ).end()
            directory = dst_list[2][:ind] + '/'
        hide_dir = directory + 'hide/'
        if os.path.exists( hide_dir ):
            print('\n\nthe hide folder already exists in {0}'.format(directory))
            utils.exit_point(300)
        else:
            os.makedirs( directory + 'hide' )
        for file in dst_list[2:]:
            shutil.move( file, os.path.join( hide_dir, file[ind+1:] ) )
