import os
import h5py
import matplotlib.pyplot as plt
from matplotlib.pyplot import text as txt
from tqdm import tqdm
import numpy as np

dirs = [input("Please give a path to directory containing the *.h5 file(s)\n").strip()]
# dirs = ['/home/vault/caph/mppi007h/simulation/mcdst_exporter']

# HAP presel cuts
h1_loc_dis = 0.525
h1_size_cut = 60
energy_cuts = [0.005, 300]

energy_histo, bins_en = np.histogram([], bins=200, range=(0.005, 150))
altitude_histo, bins_alt = np.histogram([], bins=3600, range=(0, 360))
azimuth_histo, bins_az = np.histogram([], bins=3600, range=(0, 360))

filelist = []
total_event_number = 0
after_cuts = 0
ct5only = 0
tel4 = 0
tel3 = 0
tel2 = 0
tel1 = 0

energies = []
offsets = []
count = 0


def reject_event(ct1_loc_dis, ct1_size, ct2_loc_dis, ct2_size, ct3_loc_dis, ct3_size, ct4_loc_dis, ct4_size, energy):
    if energy < energy_cuts[0] or energy > energy_cuts[1]:
        return 0

    num_h1_images = 4
    if ct1_loc_dis > h1_loc_dis or ct1_loc_dis < 0 or ct1_size < h1_size_cut:
        num_h1_images -= 1

    if ct2_loc_dis > h1_loc_dis or ct2_loc_dis < 0 or ct2_size < h1_size_cut:
        num_h1_images -= 1

    if ct3_loc_dis > h1_loc_dis or ct3_loc_dis < 0 or ct3_size < h1_size_cut:
        num_h1_images -= 1

    if ct4_loc_dis > h1_loc_dis or ct4_loc_dis < 0 or ct4_size < h1_size_cut:
        num_h1_images -= 1

    return num_h1_images


def great_circle_distance(alt1, alt2, az1, az2, angles='degrees'):
    """
    :param alt1: numpy array with altitude of points 1 (in degrees)
    :param alt2: numpy array with altitude of points 2 (in degrees)
    :param az1: numpy array with azimuth of points 1 (in degrees)
    :param az2: numpy array with azimuth of points 2 (in degrees)
    :param angles: string stating degrees or radians
    :return: an array with the great circle distance in the requested angles system (default is degrees)
    """
    alt1_rad = np.deg2rad(alt1)
    alt2_rad = np.deg2rad(alt2)
    az1_rad = np.deg2rad(az1)
    az2_rad = np.deg2rad(az2)
    s1 = np.sin(alt1_rad)
    s2 = np.sin(alt2_rad)
    c1 = np.cos(alt1_rad)
    c2 = np.cos(alt2_rad)
    sz = np.sin(az1_rad - az2_rad)
    cz = np.cos(az1_rad - az2_rad)

    theta_rad = np.arctan2(np.sqrt(np.square(c2 * sz) + np.square(c1 * s2 - s1 * c2 * cz)), (s1 * s2 + c1 * c2 * cz))

    if angles == 'degrees':
        return np.rad2deg(theta_rad)
    else:
        return theta_rad



for directory in dirs:
    dirlist = os.listdir(directory)
    print("Found %i file(s), now processing" % (len(dirlist)))
    for filename in tqdm(dirlist):
    #for filename in dirlist:
        count += 1
        #if count > 10:
        #    break
        if '.dst.h5' in filename:
            filepath = os.path.join(directory, filename)
            filelist.append(filepath)
            with h5py.File(filepath, 'r') as dst:
                events_list = dst['Events_List']
                run_data = dst['Run_Data']
                obs_az = run_data['ObsAz'][0]
                obs_alt = run_data['ObsAlt'][0]
                alt = events_list['Altitude']
                az = events_list['Azimuth']
                CT1_loc_dis = events_list['CT1_loc_dis']
                CT1_size = events_list['CT1_size']
                CT2_loc_dis = events_list['CT2_loc_dis']
                CT2_size = events_list['CT2_size']
                CT3_loc_dis = events_list['CT3_loc_dis']
                CT3_size = events_list['CT3_size']
                CT4_loc_dis = events_list['CT4_loc_dis']
                CT4_size = events_list['CT4_size']

                total_event_number += len(events_list)
                en = events_list['Energy']
                energies += en.tolist()
                hist_en, _ = np.histogram(en, bins=200, range=(0.005, 150))
                hist_alt, _ = np.histogram(alt, bins=3600, range=(0, 360))
                hist_az, _ = np.histogram(az, bins=3600, range=(0, 360))
                energy_histo += hist_en
                altitude_histo += hist_alt
                azimuth_histo += hist_az

                for i in range(len(events_list)):
                    offsets.append(great_circle_distance(alt[i], obs_alt, az[i], obs_az))
                    multi = reject_event(CT1_loc_dis[i], CT1_size[i], CT2_loc_dis[i], CT2_size[i], CT3_loc_dis[i],
                                         CT3_size[i], CT4_loc_dis[i], CT4_size[i], en[i])
                    if multi > 1:
                        after_cuts += 1

                tels_in_event = events_list['Tels_in_event'].tolist()

                for x in tels_in_event:
                    i = 0
                    for tel in x[0:4]:
                        if tel != 0:
                            i += 1
                    if i == 0 and x[4] != 0:
                        ct5only += 1
                    elif i == 0:
                        print('empty event')

                    if i == 1: tel1 += 1
                    if i == 2: tel2 += 1
                    if i == 3: tel3 += 1
                    if i == 4: tel4 += 1

offsets = np.array(offsets)
energies = np.array(energies)
min_e = np.min(energies)
max_e = np.max(energies)
he_lim = 5.0
he = np.nonzero(energies > he_lim)[0]

text = """
SUMMARY:

%i events in %i files
%.1f GeV < E < %.1f TeV

CT5only %i
single: %i
double: %i
triple: %i
quad:   %i
(%.2f events/file)

HESS1:
%i

HESS1 after cuts 
(size> %.1f && loc_dis < %.3f && E [%.1f GeV, %.1f TeV]):
%i

Ratio of events above %.1f TeV out of %i events:
%.2f%%
""" % (total_event_number, len(filelist), min_e*1000, max_e, ct5only, tel1, tel2, tel3, tel4, total_event_number / len(filelist), tel2 + tel3 + tel4,
       h1_size_cut, h1_loc_dis, energy_cuts[0]*1000, energy_cuts[1], after_cuts, he_lim, total_event_number, len(he) * 100 / total_event_number)

print(text)

plt.figure(figsize=(8, 10))
plt.subplot(3, 2, 1)
plt.bar(bins_en[:-1], energy_histo)
ax = plt.gca()
ax.set_yscale("log", nonposy='clip')
plt.title('Energy')

plt.subplot(3, 2, 2)
plt.bar(bins_en[:-1], energy_histo)
ax = plt.gca()
ax.set_yscale("log", nonposy='clip')
ax.set_xscale("log", nonposx='clip')
plt.title('Energy')

plt.subplot(3, 2, 3)
plt.title('Alt')
ax = plt.gca()
ax.set_xlim(60, 80)
plt.bar(bins_alt[:-1], altitude_histo)

plt.subplot(3, 2, 4)
plt.title('Az')
ax = plt.gca()
ax.set_xlim(160, 200)
plt.bar(bins_az[:-1], azimuth_histo)

plt.subplot(3, 2, 5)
plt.title('Offsets')
plt.hist(offsets, bins=200)
max_offset = np.max(offsets)
txt(max_offset * 1.1, 2.0, text, fontsize=8)

plt.tight_layout()
fig = plt.gcf()
plt.show()

figurename = os.path.join(dirs[0], 'h5_check.png')
input('Save figure as \n%s ?\n[Enter] : yes \t Ctr+C : abort.\n'% figurename)
fig.savefig(figurename)
print('done.')
