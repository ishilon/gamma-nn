
import numpy as np
import tensorflow as tf

filename = '/media/data/gammanndata/models/classification/hess1/extended.phase2b5.gamma_diffuse-pks_diffuse.south.20deg./h1_sampling_linear_int.h1_resolution_64.num_rotations_0.train_events_128.0k/dataset/valid_file_f2_1.tfrecord'

filename_queue = tf.train.string_input_producer( [filename], num_epochs=1 )
# Uncomment to use compressed tfr
options = tf.python_io.TFRecordOptions(tf.python_io.TFRecordCompressionType.GZIP)
# Reader for compressed tfrs
reader = tf.TFRecordReader(options=options)
_, serialized_example = reader.read( filename_queue )


tfrecord_features = tf.parse_single_example( serialized_example,
                                                 features = {'combined_image': tf.FixedLenFeature( [], tf.string ),
                                                             'uid': tf.FixedLenFeature( [], tf.string ),
                                                             'type': tf.FixedLenFeature( [], tf.int64 ),
                                                             'num_images': tf.FixedLenFeature( [], tf.int64 ),
                                                             'energy': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct2_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct3_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct4_image': tf.FixedLenFeature( [ ], tf.string ),
                                                             'ct1_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_loc_dis': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_length': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_width': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct1_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct2_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct3_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'ct4_size': tf.FixedLenFeature( [], tf.float32 ),
                                                             'src_alt': tf.FixedLenFeature( [], tf.float32 ),
                                                             'src_az': tf.FixedLenFeature( [], tf.float32 ),
                                                             'obs_alt': tf.FixedLenFeature( [], tf.float32 ),
                                                             'obs_az': tf.FixedLenFeature( [], tf.float32 ),
                                                             'obs_time': tf.FixedLenFeature( [], tf.string ),
                                                             'core_x': tf.FixedLenFeature( [], tf.float32 ),
                                                             'core_y': tf.FixedLenFeature( [], tf.float32 ),
                                                             'tels_in_event': tf.FixedLenFeature( [5], tf.int64 )
                                                             },
                                                 name = 'features' )

type = []

sess = tf.Session()
sess.run(tf.global_variables_initializer())
sess.run(tf.local_variables_initializer())
coord = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coord)

try:
    k=0
    while not coord.should_stop():
        k+=1
        if not k%1000:
            print("\r",k, end='')
        type.append(sess.run(tfrecord_features)['type'])
except tf.errors.OutOfRangeError:
    print('Done iterating!')
finally:
    coord.request_stop()
coord.join(threads)
sess.close()

print(type)
print('done.')

