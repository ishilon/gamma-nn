# This is a simple script that plots shower images in the original hexagonal grids of HESS I and II
# It plots by default all telescopes (i.e. 5) even if the telescope did not trigger

import numpy as np
from gnnlib.data import minidst as dst
from gnnlib.data import pixel_maps as pixm
from matplotlib.patches import Polygon
import matplotlib.pyplot as plt
from gnnlib.data import rebin_img


# A function that plots the camera's plane and colors the pixels with signal
def plot_hex( centers, xc_image, yc_image, intensity, lim_neg=-0.85, lim_pos=0.85, edge=0.0242, c=(0.0, 0.0, 1.0), axess=None ):
    if axess is None:
        axess = plt.gca()
    cmap = plt.get_cmap('rainbow')

    normalized_intensity = np.log10( 2.0 + (intensity - np.amin(intensity))*8.0/(np.amax(intensity) - np.amin(intensity)) )
    counter = 0
    for xc, yc in centers:
        hex_xy = np.array( [np.cos(np.linspace(np.pi/6, np.pi*330/180, 6)) * edge, np.sin(np.linspace(np.pi/6, np.pi*330/180, 6)) * edge] ).transpose()
        hex_xy[:, 0] = hex_xy[:, 0] + xc
        hex_xy[:, 1] = hex_xy[:, 1] + yc
        if tuple( [xc, yc] ) in zip( xc_image, yc_image ):
            axess.add_patch( Polygon( hex_xy, color = cmap( normalized_intensity[counter ] ), ec = (0, 0, 0) ) )
            counter += 1
        else:
            axess.add_patch( Polygon( hex_xy, color = c, ec = (0, 0, 0) ) )
    axess.set_xlim( [ lim_neg, lim_pos ] )
    axess.set_ylim( [ lim_neg, lim_pos ] )
# -----------------------------------------------------------------------------


#h5_file = '/media/data/gammanndata/h5_DSTs/extended/phase2b5/gamma_diffuse/south/20deg/gamma_20deg_180deg_run210000___phase2bphase2b5_hess_cone5.dst.h5'
#h5_file = '/media/data/gammanndata/h5_DSTs/extended/phase2b5/proton_diffuse/south/20deg/proton_20deg_180deg_run82815___phase2b5_desert-ws0.dst.h5'
#h5_file = '/media/data/gammanndata/h5_DSTs/konrad/phase2b5/gamma_diffuse/south/20deg/gamma_20deg_180deg_run210000___phase2bphase2b5_hess_cone5.dst.h5'
h5_file = '/media/data/gammanndata/h5_DSTs/extended/phase2b5/pks_diffuse/south/20deg/run_034855_DST_001.h5'
f = dst.H5DST(h5_file)

uid = f.uid
tels_in_event = f.tels_in_event
#num_of_images = f.num_of_images
energy = f.energy

images = f.images
pixel_ids = f.pixel_ids

if np.mean(energy) == -1:
    cam1_xc, cam1_yc = pixm.get_real_cam1_xy()
    print('real Data!')
else:
    cam1_xc, cam1_yc = pixm.get_mc_cam1_xy()
    cam2_xc, cam2_yc = pixm.get_mc_cam2_xy()
    print('MC')

#rebin_matrix, rebin_resolution = rebin_img.prepare_rebin_matrix(cam1_xc, cam1_yc, 1.01)

# print(tels_in_event[0:36])
# print(energy[0:36])

num_of_events = len(uid)
print( 'number of events in file = ', num_of_events )

while True:
    j = np.random.randint(low=0, high=num_of_events)
    if tels_in_event[j][0] !=0 and tels_in_event[j][1] !=0 and tels_in_event[j][2] !=0 and tels_in_event[j][3] !=0:
        #print( 'j = ', j, 'energy = ', energy[j] )
        fig, axes = plt.subplots(2, 2, figsize = (12, 12))
        #fig2, axes2 = plt.subplots(figsize = (8, 8))
        fig.suptitle("Event #{0}, E = {1:.3f}  TeV, CT1-4".format( uid[j], round(energy[j], 3) ), fontsize = 14)
        #fig2.suptitle("Event #{0}, E = {1:.3f} TeV, CT5".format( uid[j], round(energy[j], 3) ), fontsize = 14)

        titles = [ 'CT 1', 'CT 2', 'CT 3', 'CT 4' ]

        # iterator over the axes
        it = np.nditer(axes, flags = ['multi_index', 'refs_ok'])

        i = 0
        while not it.finished:
            end_ = np.where(images[j][i] == 0)[0]
            end = end_[0] if len(end_)>0 else -1
            image_intensity = images[j][i][:end]
            image_pixels = pixel_ids[j][i][:end]
            xc_pix_in_image = cam1_xc[image_pixels][:end]
            yc_pix_in_image = cam1_yc[image_pixels][:end]
            plot_hex( zip(cam1_xc, cam1_yc), xc_pix_in_image, yc_pix_in_image, image_intensity, axess = axes[it.multi_index ] )
            axes[it.multi_index].set_title(titles[i])# + '(' + str(np.amin(image_intensity)) + ', ' + str(np.amax(image_intensity)) + ')')
            it.iternext()
            i += 1
        plt.show()
        '''
        image_intensity2 = images[j][4]
        image_pixels2 = pixel_ids[j][4]
        xc_pix_in_image2 = cam2_xc[image_pixels2]
        yc_pix_in_image2 = cam2_yc[image_pixels2]
        plot_hex( zip(cam2_xc, cam2_yc), xc_pix_in_image2, yc_pix_in_image2, image_intensity2,
                  lim_neg = -1.2, lim_pos = 1.2, edge = 0.0242, axess = axes2 )
        plt.show()

        for i in range(4):
            image_intensity = images[ j ][ i ]
            image_pixels = pixel_ids[ j ][ i ]
            xc_pix_in_image = cam1_xc[ image_pixels ]
            yc_pix_in_image = cam1_yc[ image_pixels ]
            plot_hex( zip( cam1_xc, cam1_yc ), xc_pix_in_image, yc_pix_in_image, image_intensity )
            plt.show()

            tel_image = rebin_img.rebin( pixel_ids[j][ i ], images[j][ i ], cam1_xc, rebin_matrix, rebin_resolution, 64 )
            print(tel_image)
            tel_image = np.log10( 2.0 + (tel_image - np.amin( tel_image )) * 8.0 / (np.amax( tel_image ) - np.amin( tel_image )) )
            cmap = plt.get_cmap( 'rainbow' )
            cmap = cmap(tel_image)
            img = plt.imshow( tel_image )
            img.set_cmap( cmap )
            plt.show()
        '''
# plt.show()
