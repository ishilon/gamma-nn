/*
This script converts MC root files to HDF5 files, utilising variable length variables in the HDF5 file.
Due to the HDF5 H5F class limitations, the h5 files will be created in the same directory
in which the script was compiled and the executable was running.
The input directory, containing the rot files to be converted, is given as a command line input.
All .root files in the input directory will be converted.
*/

// STL
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>
#include "dirent.h"

// HAP
#include "sash/DataSet.hh"
#include "sash/EventHeader.hh"
#include "sash/EventSelection.hh"
#include "sash/HESSArray.hh"
#include "sash/IntensityData.hh"
#include "sash/PointerSet.hh"
#include "sash/PointerSetIterator.hh"
#include "sash/RunHeader.hh"
#include "sash/Telescope.hh"
#include "sash/TelescopeConfig.hh"
#include "sash/Pixel.hh"
#include "sash/List.hh"
#include "sash/ListIterator.hh"
#include "sash/PixelConfig.hh"
#include "montecarlo/MCTrueShower.hh"
#include "montecarlo/MCRunHeader.hh"
#include "muon/MuonParameters.hh"
#include "reco/HillasParameters.hh"
#include "reco/Shower.hh"
#include "stash/Coordinate.hh"

#include <sash/PointingCorrection.hh>




// ROOT
#include <TFile.h>

// HDF5
#include "H5Cpp.h"

typedef struct {
    // Header
    unsigned long eventID = -1;
    unsigned long bunchID = -1;
    int tels_in_event[5] = {};
    std::string event_tels_str;
    int num_of_images;
    // images_data
    std::vector<float> tel1_image, tel2_image, tel3_image, tel4_image, tel5_image;
    std::vector<float> tel1_pixloc_telnom_x, tel2_pixloc_telnom_x, tel3_pixloc_telnom_x, tel4_pixloc_telnom_x, tel5_pixloc_telnom_x;  
    std::vector<float> tel1_pixloc_nom_x, tel2_pixloc_nom_x, tel3_pixloc_nom_x, tel4_pixloc_nom_x, tel5_pixloc_nom_x;
    std::vector<float> tel1_pixloc_telnom_y, tel2_pixloc_telnom_y, tel3_pixloc_telnom_y, tel4_pixloc_telnom_y, tel5_pixloc_telnom_y;  
    std::vector<float> tel1_pixloc_nom_y, tel2_pixloc_nom_y, tel3_pixloc_nom_y, tel4_pixloc_nom_y, tel5_pixloc_nom_y;  
    std::vector<int> tel1_pixel_ID, tel2_pixel_ID, tel3_pixel_ID, tel4_pixel_ID, tel5_pixel_ID;
    hvl_t tel1_image_Handle, tel2_image_Handle, tel3_image_Handle, tel4_image_Handle, tel5_image_Handle;
    hvl_t tel1_pixel_ID_Handle, tel2_pixel_ID_Handle, tel3_pixel_ID_Handle, tel4_pixel_ID_Handle, tel5_pixel_ID_Handle;
    hvl_t tel1_pixloc_nom_x_Handle, tel2_pixloc_nom_x_Handle, tel3_pixloc_nom_x_Handle, tel4_pixloc_nom_x_Handle, tel5_pixloc_nom_x_Handle; 
    hvl_t tel1_pixloc_nom_y_Handle, tel2_pixloc_nom_y_Handle, tel3_pixloc_nom_y_Handle, tel4_pixloc_nom_y_Handle, tel5_pixloc_nom_y_Handle;
    hvl_t tel1_pixloc_telnom_x_Handle, tel2_pixloc_telnom_x_Handle, tel3_pixloc_telnom_x_Handle, tel4_pixloc_telnom_x_Handle, tel5_pixloc_telnom_x_Handle; 
    hvl_t tel1_pixloc_telnom_y_Handle, tel2_pixloc_telnom_y_Handle, tel3_pixloc_telnom_y_Handle, tel4_pixloc_telnom_y_Handle, tel5_pixloc_telnom_y_Handle; 

    double time 					= 0;

    // MC_data
    unsigned long mcID          = -1;
    unsigned long showerID      = -1;
    int primaryID               = -1;
    float energy                = -1.0;
    float alt                   = -1.0;
    float az                    = -1.0;
    float DirX_Nom              = -1.0;
    float DirY_Nom              = -1.0;
    float ra                    = -1.0;
    float dec                   = -1.0;
    float CoreX_Ground          = -1.0;
    float CoreY_Ground          = -1.0;
    float CoreZ_Ground          = -1.0;
    float CoreX                 = -1.0;
    float CoreY                 = -1.0;
    float CoreZ                 = -1.0;
    float xStart                = -1.0;
    float xMax                  = -1.0;
    float firstInteractionAlt   = -1.0;
    // Hillas_data
    float tel1_width    = -1.0;
    float tel1_length   = -1.0;
    float tel1_size     = -1.0;
    float tel1_loc_dis  = -1.0;
    float tel1_phi      = -1.0;
    float tel1_cog_x    = -1.0;
    float tel1_cog_y    = -1.0;
    float tel1_skewness = -1.0;
    float tel1_kurtosis = -1.0;

    float tel2_width    = -1.0;
    float tel2_length   = -1.0;
    float tel2_size     = -1.0;
    float tel2_loc_dis  = -1.0;
    float tel2_phi      = -1.0;
    float tel2_cog_x    = -1.0;
    float tel2_cog_y    = -1.0;
    float tel2_skewness = -1.0;
    float tel2_kurtosis = -1.0;

    float tel3_width    = -1.0;
    float tel3_length   = -1.0;
    float tel3_size     = -1.0;
    float tel3_loc_dis  = -1.0;
    float tel3_phi      = -1.0;
    float tel3_cog_x    = -1.0;
    float tel3_cog_y    = -1.0;
    float tel3_skewness = -1.0;
    float tel3_kurtosis = -1.0;

    float tel4_width    = -1.0;
    float tel4_length   = -1.0;
    float tel4_size     = -1.0;
    float tel4_loc_dis  = -1.0;
    float tel4_phi      = -1.0;
    float tel4_cog_x    = -1.0;
    float tel4_cog_y    = -1.0;
    float tel4_skewness = -1.0;
    float tel4_kurtosis = -1.0;

    float tel5_width    = -1.0;
    float tel5_length   = -1.0;
    float tel5_size     = -1.0;
    float tel5_loc_dis  = -1.0;
    float tel5_phi      = -1.0;
    float tel5_cog_x    = -1.0;
    float tel5_cog_y    = -1.0;
    float tel5_skewness = -1.0;
    float tel5_kurtosis = -1.0;

} event_data_struct;

typedef struct {
    unsigned int run_number;
    std::string tels_str;
    int ntels        = -1;
    float obspos_alt = -1.0;
    float obspos_az  = -1.0;
    float obspos_ra  = -1.0;
    float obspos_dec = -1.0;
    int duration     = -1.0;
    long time_stamp = -1;
} run_data_struct;

typedef struct {
    float spectral_index    = -1.0;
    float max_e             = -1.0;
    float min_e             = -1.0;
    float max_cone          = -1.0;
    float min_cone          = -1.0;
    int   num_sim_showers   = -1.0;
    int   times_shower_used = -1.0;
    int   core_pos_mode     = -1.0;
    float max_core_range    = -1.0;
    float min_core_range    = -1.0;
} mc_run_data_struct;

void fill_data( event_data_struct &event_data, Sash::HESSArray* hess ){

    const Sash::MCTrueShower* mcshower = hess->Get<Sash::MCTrueShower>();
    if (mcshower != NULL){
        event_data.mcID                 = mcshower->GetGenEventNum();
        event_data.showerID             = mcshower->GetShowerNum();
        event_data.primaryID            = mcshower->GetPrimaryID();
        event_data.energy               = mcshower->GetEnergy();

        Stash::Coordinate mcaltaz       = mcshower->GetDirection().GetCoordinate(*hess->GetAltAzSystem());
        event_data.az                   = mcaltaz.GetLambda().GetDegrees();
        event_data.alt                  = mcaltaz.GetBeta().GetDegrees();

	Stash::Coordinate mcdir_nom     = mcshower->GetDirection().GetCoordinate(*hess->GetNominalSystem());
        event_data.DirX_Nom             = mcdir_nom.GetX();
        event_data.DirY_Nom             = mcdir_nom.GetY();

        Stash::Coordinate mcradec       = mcshower->GetDirection().GetCoordinate(*hess->GetRADecJ2000System());
        event_data.ra                   = mcradec.GetLambda().GetDegrees();
        event_data.dec                  = mcradec.GetBeta().GetDegrees();

        event_data.CoreX                = mcshower->GetCore().GetCoordinate(*hess->GetTiltedSystem()).GetX();
        event_data.CoreY                = mcshower->GetCore().GetCoordinate(*hess->GetTiltedSystem()).GetY();
        event_data.CoreZ                = mcshower->GetCore().GetCoordinate(*hess->GetTiltedSystem()).GetZ();
        event_data.CoreX_Ground         = mcshower->GetCore().GetCoordinate(*hess->GetGroundSystem()).GetX();
        event_data.CoreY_Ground         = mcshower->GetCore().GetCoordinate(*hess->GetGroundSystem()).GetY();
        event_data.CoreZ_Ground         = mcshower->GetCore().GetCoordinate(*hess->GetGroundSystem()).GetZ();

        event_data.firstInteractionAlt  = mcshower->GetIniHeight();
        event_data.xStart               = mcshower->GetDepthStart();
        event_data.xMax                 = mcshower->GetShowerMax();
    }
    else {
    	const Sash::RunHeader* fRunHeader = hess->Get<Sash::RunHeader>();
    	Stash::Coordinate obs = fRunHeader->GetObservationPosition();
    	//Stash::Coordinate target = fRunHeader->GetTargetPosition();

    	event_data.alt                 = obs.GetCoordinate(*hess->GetAltAzSystem()).GetTheta().GetDegrees();
    	event_data.az                  = obs.GetCoordinate(*hess->GetAltAzSystem()).GetLambda().GetDegrees();
    	event_data.ra 				   = obs.GetCoordinate(*hess->GetRADecJ2000System()).GetLambda().GetDegrees();
    	event_data.dec 				   = obs.GetCoordinate(*hess->GetRADecJ2000System()).GetBeta().GetDegrees();

    	//float targetalt                 = target.GetCoordinate(*hess->GetAltAzSystem()).GetTheta().GetDegrees();
    	//float targetaz                  = target.GetCoordinate(*hess->GetAltAzSystem()).GetLambda().GetDegrees();

    }
}// end fill_data

void write_DSTevent_to_event_struct( event_data_struct &event_data, Sash::HESSArray* hess, int a )
{
    fill_data( event_data, hess );

    const Sash::EventHeader* event_header = hess->Get<Sash::EventHeader>();

    event_data.eventID  =  event_header->GetGlobalEvtNum();
    event_data.bunchID  =  event_header->GetGlobalBunchNum();
    event_data.time		= event_header->GetGPSTime().GetTimeDouble();
    //std::cout << "Event alt \t" << event_data.alt << " \taz " << event_data.az << " Target alt\t" << targetalt << " az \t" << targetaz <<" Timestamp \t" << event_data.time << std::endl;
    //std::cout << event_header->GetGPSTime() << "\t" << event_data.time - 1125870000 << std::endl;

    if( !event_header ) {
        std::cout << "Error: cannot access event header for event # " << a << std::endl;
        return;
    }

    const Sash::EventSelection* event_sel = hess->Get<Sash::EventSelection>();
    const Sash::PointerSet<Sash::Telescope> &tels_with_data = (event_sel) ? event_sel->GetTelSelected() : event_header->GetTelWData();

    for( Sash::PointerSet<Sash::Telescope>::const_iterator tel_it = tels_with_data.begin(); tel_it != tels_with_data.end(); ++tel_it ) {

        std::vector<float>* pixel_intensity       = NULL;
        std::vector<int>* pixel_ID                = NULL;
        std::vector<float>* pixel_loc_telnom_x    = NULL;
        std::vector<float>* pixel_loc_telnom_y    = NULL;
        std::vector<float>* pixel_loc_nom_x       = NULL;
        std::vector<float>* pixel_loc_nom_y       = NULL;
        float width;//    = NULL;
        float length;//   = NULL;
        float size;//     = NULL;
        float loc_dis;//  = NULL;
        float phi;//      = NULL;
        float cog_x;//    = NULL;
        float cog_y;//    = NULL;
        float skewness;// = NULL;
        float kurtosis;// = NULL;

        const Reco::HillasParameters* hillas = (*tel_it)->Get<Reco::HillasParameters>("Hillas0510"); //WHY Hillas0510?
        if( hillas ){
            width    = hillas->GetWidth();
            length   = hillas->GetLength();
            size     = hillas->GetImageAmplitude();
            loc_dis  = hillas->GetLocalDistance();
            phi      = hillas->GetPhi().GetDegrees();
            cog_x    = hillas->GetCenterOfGravity().GetX();
            cog_y    = hillas->GetCenterOfGravity().GetY();
            skewness = hillas->GetSkewness();
            kurtosis = hillas->GetKurtosis();
        }
        else std::cout << "No Hillas Parameters object found" << std::endl;

        int telescope_number = (*tel_it)->GetId();

        switch( telescope_number ){
                case 1 : pixel_intensity                = &(event_data.tel1_image);
                         pixel_ID                       = &(event_data.tel1_pixel_ID);
                         pixel_loc_telnom_x             = &(event_data.tel1_pixloc_telnom_x);
                         pixel_loc_nom_x                = &(event_data.tel1_pixloc_nom_x);
			 pixel_loc_telnom_y             = &(event_data.tel1_pixloc_telnom_y);
                         pixel_loc_nom_y                = &(event_data.tel1_pixloc_nom_y);
                         (event_data.tel1_width)        = width;
                         (event_data.tel1_length)       = length;
                         (event_data.tel1_size)         = size;
                         (event_data.tel1_loc_dis)      = loc_dis;
                         (event_data.tel1_phi)          = phi;
                         (event_data.tel1_cog_x)        = cog_x;
                         (event_data.tel1_cog_y)        = cog_y;
                         (event_data.tel1_skewness)     = skewness;
                         (event_data.tel1_kurtosis)     = kurtosis;
                         event_data.tels_in_event[0]    = 1;
                         break;
                case 2 : pixel_intensity                = &(event_data.tel2_image);
                         pixel_ID                       = &(event_data.tel2_pixel_ID);
			 pixel_loc_telnom_x             = &(event_data.tel2_pixloc_telnom_x);
                         pixel_loc_nom_x                = &(event_data.tel2_pixloc_nom_x);
			 pixel_loc_telnom_y             = &(event_data.tel2_pixloc_telnom_y);
                         pixel_loc_nom_y                = &(event_data.tel2_pixloc_nom_y);
                         (event_data.tel2_width)        = width;
                         (event_data.tel2_length)       = length;
                         (event_data.tel2_size)         = size;
                         (event_data.tel2_loc_dis)      = loc_dis;
                         (event_data.tel2_phi)          = phi;
                         (event_data.tel2_cog_x)        = cog_x;
                         (event_data.tel2_cog_y)        = cog_y;
                         (event_data.tel2_skewness)     = skewness;
                         (event_data.tel2_kurtosis)     = kurtosis;
                         event_data.tels_in_event[1]    = 2;
                         break;
                case 3 : pixel_intensity                = &(event_data.tel3_image);
                         pixel_ID                       = &(event_data.tel3_pixel_ID);
			 pixel_loc_telnom_x             = &(event_data.tel3_pixloc_telnom_x);
                         pixel_loc_nom_x                = &(event_data.tel3_pixloc_nom_x);
			 pixel_loc_telnom_y             = &(event_data.tel3_pixloc_telnom_y);
                         pixel_loc_nom_y                = &(event_data.tel3_pixloc_nom_y);
                         (event_data.tel3_width)        = width;
                         (event_data.tel3_length)       = length;
                         (event_data.tel3_size)         = size;
                         (event_data.tel3_loc_dis)      = loc_dis;
                         (event_data.tel3_phi)          = phi;
                         (event_data.tel3_cog_x)        = cog_x;
                         (event_data.tel3_cog_y)        = cog_y;
                         (event_data.tel3_skewness)     = skewness;
                         (event_data.tel3_kurtosis)     = kurtosis;
                         event_data.tels_in_event[2]    = 3;
                         break;
                case 4 : pixel_intensity                = &(event_data.tel4_image);
                         pixel_ID                       = &(event_data.tel4_pixel_ID);
			 pixel_loc_telnom_x             = &(event_data.tel4_pixloc_telnom_x);
                         pixel_loc_nom_x                = &(event_data.tel4_pixloc_nom_x);
			 pixel_loc_telnom_y             = &(event_data.tel4_pixloc_telnom_y);
                         pixel_loc_nom_y                = &(event_data.tel4_pixloc_nom_y);
                         (event_data.tel4_width)        = width;
                         (event_data.tel4_length)       = length;
                         (event_data.tel4_size)         = size;
                         (event_data.tel4_loc_dis)      = loc_dis;
                         (event_data.tel4_phi)          = phi;
                         (event_data.tel4_cog_x)        = cog_x;
                         (event_data.tel4_cog_y)        = cog_y;
                         (event_data.tel4_skewness)     = skewness;
                         (event_data.tel4_kurtosis)     = kurtosis;
                         event_data.tels_in_event[3]    = 4;
                         break;
                case 5 : pixel_intensity                = &(event_data.tel5_image);
                         pixel_ID                       = &(event_data.tel5_pixel_ID);
			 pixel_loc_telnom_x             = &(event_data.tel5_pixloc_telnom_x);
                         pixel_loc_nom_x                = &(event_data.tel5_pixloc_nom_x);
			 pixel_loc_telnom_y             = &(event_data.tel5_pixloc_telnom_y);
                         pixel_loc_nom_y                = &(event_data.tel5_pixloc_nom_y);
                         (event_data.tel5_width)        = width;
                         (event_data.tel5_length)       = length;
                         (event_data.tel5_size)         = size;
                         (event_data.tel5_loc_dis)      = loc_dis;
                         (event_data.tel5_phi)          = phi;
                         (event_data.tel5_cog_x)        = cog_x;
                         (event_data.tel5_cog_y)        = cog_y;
                         (event_data.tel5_skewness)     = skewness;
                         (event_data.tel5_kurtosis)     = kurtosis;
                         event_data.tels_in_event[4]    = 5;
                         break;
            }// end switch

	
        const Sash::IntensityData* intensityData = (*tel_it)->Get<Sash::IntensityData>("Clean0407");
        const Sash::PointerSet<Sash::Pixel>& imagelist = intensityData->GetDataMode() == Sash::IntensityData::ZeroSup ?
                                                         intensityData->GetNonZero() : (*tel_it)->GetConfig()->GetPixelsInTel();

        const Stash::SystemRef& nominal = (*tel_it)->GetHESSArray()->GetNominalSystem();
        const Stash::SystemRef& telnom =  (*tel_it)->GetNominalTelescopeSystem();
	
	//Added only for testing purposes
	/*
	const Sash::PointerSet<Sash::Pixel>& Pixellist = (*tel_it)->GetConfig()->GetPixelsInTel();
	double max_x_nom = 10;
	double max_y_nom = 10;
	int counter = 0;
	if(telescope_number == 5) continue;
	for( Sash::PointerSet<Sash::Pixel>::const_iterator pix_it = Pixellist.begin(); pix_it != Pixellist.end(); ++pix_it ) {
	  const Stash::Coordinate PixPos_Nom	= (*pix_it)->GetConfig()->GetPos().GetCoordinate(*nominal);
	  const Stash::Coordinate PixPos_TelNom	= (*pix_it)->GetConfig()->GetPos().GetCoordinate(*telnom);
	  double pix_x_nom = PixPos_Nom.GetX();
	  double pix_x_telnom = PixPos_TelNom.GetX();	  
	  double pix_y_telnom = PixPos_TelNom.GetY();	  
	  double pix_y_nom = PixPos_Nom.GetY();
	  if(counter == 0 && telescope_number ==1 )std::cout<<pix_x_nom<<std::endl;
	  if(pix_x_nom < max_x_nom) max_x_nom = pix_x_nom;
	  if(pix_y_nom < max_y_nom) max_y_nom = pix_y_nom;
	  //std::cout<<pix_y_nom<<" "<<pix_y_telnom<<std::endl;
	  counter+=1;
	}
	std::cout<<max_x_nom<<" "<<max_y_nom<<std::endl;
	*/
	
	// Scaling factor for real data
	double f = 1.0;
        const Sash::PointingCorrection* pc =  (*tel_it)->Get<Sash::PointingCorrection>("Pointing::NullModel_Camera");
        if(!pc){
	    pc = (*tel_it)->Get<Sash::PointingCorrection>("Pointing::MechanicalModel_Camera");
	    f = 1.0; //pc->GetFocalLength()/15.0;
	}
	
        for( Sash::PointerSet<Sash::Pixel>::const_iterator pix_it = imagelist.begin(); pix_it != imagelist.end(); ++pix_it ) {
	    const Stash::Coordinate PixPos_TelNom	= (*pix_it)->GetConfig()->GetPos().GetCoordinate(*telnom);
	    const Stash::Coordinate PixPos_Nom	= (*pix_it)->GetConfig()->GetPos().GetCoordinate(*nominal);
            pixel_intensity->push_back( intensityData->GetIntensity(*pix_it)->fIntensity );
            pixel_ID->push_back( ( *pix_it )->GetConfig()->GetPixelID() );
	    pixel_loc_telnom_x->push_back( PixPos_TelNom.GetX()*f );
	    pixel_loc_telnom_y->push_back( PixPos_TelNom.GetY()*f );
	    pixel_loc_nom_x->push_back( PixPos_Nom.GetX() );
	    pixel_loc_nom_y->push_back( PixPos_Nom.GetY() );	    
	    
        }//end main pixel loop
	// Also include neighbour pixels to make linear interpolation method posssible
	for( Sash::PointerSet<Sash::Pixel>::const_iterator pix_it = imagelist.begin(); pix_it != imagelist.end(); ++pix_it ) {
	    //Get List of Neighbour Pixels
	    const Sash::List<Sash::Pixel> Neighbours = (*pix_it)->GetNeighbourList();
	    //Loop over Neighbour Pixels
	    for( Sash::ListIterator<Sash::Pixel> pix_next = Neighbours.begin(); pix_next != Neighbours.end(); ++pix_next ){
	        //Check if pixel ID is already there
	        int PixID = ( *pix_next )->GetConfig()->GetPixelID() ;
	        if (std::find(pixel_ID->begin(), pixel_ID->end(),PixID)!=pixel_ID->end()) ;
	        else{
		    const Stash::Coordinate PixPos_TelNom	= (*pix_next)->GetConfig()->GetPos().GetCoordinate(*telnom);
		    const Stash::Coordinate PixPos_Nom	= (*pix_next)->GetConfig()->GetPos().GetCoordinate(*nominal);
		    pixel_intensity->push_back( intensityData->GetIntensity(*pix_next)->fIntensity );
		    pixel_ID->push_back( ( *pix_next )->GetConfig()->GetPixelID() );
		    pixel_loc_telnom_x->push_back( PixPos_TelNom.GetX()*f );
		    pixel_loc_telnom_y->push_back( PixPos_TelNom.GetY()*f );
		    pixel_loc_nom_x->push_back( PixPos_Nom.GetX() );
		    pixel_loc_nom_y->push_back( PixPos_Nom.GetY() );
		}	      
	    }
	  	    
        }//end second pixel loop

    }// end telescopes loop

    bool one    = ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 1 ) );
    bool two    = ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 2 ) );
    bool three  = ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 3 ) );
    bool four   = ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 4 ) );
    bool five   = ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 5 ) );

    event_data.num_of_images = one + two + three + four + five;

    std::stringstream ss;
    ss << "[";
    for(size_t i = 0; i < 5; ++i){
        if(i != 0)
            ss << ",";
        ss << event_data.tels_in_event[i];
    }
    ss << "]";
    event_data.event_tels_str = ss.str();

    if( !one ){
        event_data.tel1_image           = {std::nan("1")};
        event_data.tel1_pixel_ID        = {-1};
	event_data.tel1_pixloc_telnom_x = {std::nan("1")};
	event_data.tel1_pixloc_telnom_y = {std::nan("1")};
	event_data.tel1_pixloc_nom_x    = {std::nan("1")};
	event_data.tel1_pixloc_nom_y    = {std::nan("1")};
    }

    if( !two ){
        event_data.tel2_image           = {std::nan("1")};
        event_data.tel2_pixel_ID        = {-1};
	event_data.tel2_pixloc_telnom_x = {std::nan("1")};
	event_data.tel2_pixloc_telnom_y = {std::nan("1")};
	event_data.tel2_pixloc_nom_x    = {std::nan("1")};
	event_data.tel2_pixloc_nom_y    = {std::nan("1")};
    }

    if( !three ){
        event_data.tel3_image           = {std::nan("1")};
        event_data.tel3_pixel_ID        = {-1};
	event_data.tel3_pixloc_telnom_x = {std::nan("1")};
	event_data.tel3_pixloc_telnom_y = {std::nan("1")};
	event_data.tel3_pixloc_nom_x    = {std::nan("1")};
	event_data.tel3_pixloc_nom_y    = {std::nan("1")};
    }

    if( !four ){
        event_data.tel4_image           = {std::nan("1")};
        event_data.tel4_pixel_ID        = {-1};
	event_data.tel4_pixloc_telnom_x = {std::nan("1")};
	event_data.tel4_pixloc_telnom_y = {std::nan("1")};
	event_data.tel4_pixloc_nom_x    = {std::nan("1")};
	event_data.tel4_pixloc_nom_y    = {std::nan("1")};
    }

    if( !five ){
        event_data.tel5_image           = {std::nan("1")};
        event_data.tel5_pixel_ID        = {-1};
	event_data.tel5_pixloc_telnom_x = {std::nan("1")};
	event_data.tel5_pixloc_telnom_y = {std::nan("1")};
	event_data.tel5_pixloc_nom_x    = {std::nan("1")};
	event_data.tel5_pixloc_nom_y    = {std::nan("1")};
    }
}// end write_MCDSTevent_to_event_struct

void write_run_header_data( run_data_struct &run_data, Sash::HESSArray* hess, const Sash::RunHeader* runheader )
{
    run_data.run_number = (*runheader).GetRunNum();
    run_data.duration   = (*runheader).GetDuration();
    run_data.time_stamp = (*runheader).GetTimeStamp().GetTime();
    //std::cout << "Timestamp " << run_data.time_stamp << std::endl;
    // =============================================================
    // tel info (count tels in run and make list):
    // =============================================================
    const Sash::PointerSet<Sash::Telescope> &telsinrun=runheader->GetTelsInRun();
    Sash::PointerSet<Sash::Telescope>::iterator tel = telsinrun.begin();
    int ntels = 0;
    std::vector<int> tels;
    std::ostringstream ost;
    std::map<int,int>  telIdToIndex; //! maps telescope id to index for array-based parameters
    int idx=0;
    for(; tel != telsinrun.end();++tel) {
        ntels++;
        int id = (*tel)->HandleConfig()->GetTelId();
        tels.push_back( id  );
        telIdToIndex[id] = idx;
        idx++;
    }

    std::stringstream ss;
    ss << "[";
    for(size_t i = 0; i < tels.size(); ++i){
        if(i != 0)
            ss << ",";
        ss << tels[i];
    }
    ss << "]";
    run_data.tels_str = ss.str();
    run_data.ntels = ntels;

    Stash::Coordinate obs = runheader->GetObservationPosition();

    Stash::Coordinate altaz = obs.GetCoordinate(*hess->GetAltAzSystem());
    run_data.obspos_alt = altaz.GetBeta().GetDegrees();
    run_data.obspos_az  = altaz.GetLambda().GetDegrees();
    	std::cout<<obs.GetCoordinate(*hess->GetAltAzSystem()).GetTheta().GetDegrees()<< " "<<obs.GetCoordinate(*hess->GetHorizonSystem()).GetTheta().GetDegrees()<<std::endl;

    Stash::Coordinate radec = obs.GetCoordinate(*hess->GetRADecJ2000System());
    run_data.obspos_dec  = radec.GetBeta().GetDegrees();
    run_data.obspos_ra = radec.GetLambda().GetDegrees();
}


void write_mc_run_header_data( mc_run_data_struct &mc_run_data, Sash::HESSArray* hess, const Sash::MCRunHeader* mcrunheader)
{
    mc_run_data.spectral_index    = mcrunheader->GetSpectralIndex();
    mc_run_data.max_e             = mcrunheader->GetEnergyRange().GetHi();
    mc_run_data.min_e             = mcrunheader->GetEnergyRange().GetLo();
    mc_run_data.max_cone          = mcrunheader->GetViewConeRange().GetHi();
    mc_run_data.min_cone          = mcrunheader->GetViewConeRange().GetLo();
    mc_run_data.core_pos_mode     = mcrunheader->GetCorePosMode();
    mc_run_data.max_core_range    = mcrunheader->GetCoreRange().GetHi();
    mc_run_data.min_core_range    = mcrunheader->GetCoreRange().GetLo();
    mc_run_data.num_sim_showers   = mcrunheader->GetNSimShowers();
    mc_run_data.times_shower_used = mcrunheader->GetNTimesShowerUsed();
}


// Get list of files from a directory
std::vector<std::string> get_dst_list(std::string path, std::string extension){
    DIR* dir;
    struct dirent* entry;
    std::vector<std::string> dst_list;
    if ( ( dir = opendir ( path.c_str() ) ) != NULL ) {
        /* print all the files and directories within directory */
        while ((entry = readdir (dir)) != NULL) {
            if ( entry->d_type == DT_REG ){         // check for a regular file
                std::string fname = entry->d_name;
                if ( fname.find(extension) != std::string::npos ){
                    dst_list.push_back(entry->d_name);
                }
            }
	sleep(0.1);
        }
        closedir (dir);
        return(dst_list);
    }
    else {
      /* could not open directory */
      perror ("Could not open directory");
      exit(1);  // Should exit more carefully (or gently...)
    }
}


TFile* open_dst_file( char const * dstfile )
{
    TFile* f = new TFile( dstfile );
    if( !f->IsOpen() ) {
        std::cerr << "Error: could not open MCDST file " << dstfile << std::endl;
        return 0;
    }
    else if( f->IsOpen() ){
        std::cout << "Opened MCDST file: " << dstfile << std::endl;
    }
    return f;
}// end open_dst_file


/*
********************************************************
Main Function
********************************************************
*/
int main()
{
    std::string extension = ".root";
    std::string input_dir; // = "/d22/hin/ishilon/data/mc_dst_hd_hap15-12/ct1-4/corsika74100_qgs2-04_urqmd1.3cr/phase1b/gamma_pointsource/south/0.0deg/";

    std::cout << "Please enter the directory which contains the files you would like to convert:" << std::endl;
    std::cin >> input_dir;

    // Check that path includes the last back slash
    const char * back = &input_dir[ input_dir.length() ];
    if( strcmp( back, "/" ) != 0 ){
        input_dir = input_dir.append("/");
    }

    std::vector<std::string> dst_list;
    dst_list = get_dst_list(input_dir, extension); // or pass which dir to open

    int const num_of_files = dst_list.size();
    std::cout << "Number of files in directory: " << num_of_files << std::endl;
    int file_count = 0;

    for(auto d : dst_list){
        ++file_count;
        std::string file_name = d;

    //char const * dstfile = "/lfs/l2/hess/users/phwagner/tim/mc_runs/gamma/rootfiles/gamma_20deg_180deg_run70450_3_112_phase1b-tanya_hess-r400m_cone3.root";

    TFile* f = open_dst_file( (input_dir + file_name).c_str() );

    Sash::HESSArray* hess = &Sash::HESSArray::GetHESSArray();
    if (hess == NULL){
        std::cout << "Could not initialise an HESSArray" << std::endl;
        return 0;
    }

    Sash::DataSet* sash_data = (Sash::DataSet*)f->Get("run");
    sash_data->GetEntry(0);

    // data run header
    const Sash::RunHeader* runheader = hess->Get<Sash::RunHeader>();
    // monte-carlo data run header
    const Sash::MCRunHeader* mcrunheader= hess->Get<Sash::MCRunHeader>();

    Sash::DataSet* events = (Sash::DataSet*)f->Get("DST");

    if( !events ) {
        std::cerr << "Error: could not access events" << std::endl;
        return 0;
    }

    run_data_struct run_data;
    mc_run_data_struct mc_run_data;
    std::vector<event_data_struct> events_array;
    write_run_header_data( run_data, hess, runheader );

    if (mcrunheader){
        write_mc_run_header_data( mc_run_data, hess, mcrunheader );
    }

    std::cout << std::endl << "reading events using Sash/DataSetIterator..." << std::endl;
    int i = 0;
    for( Sash::DataSetIterator it = events->begin(); it != events->end(); ++i ){
        it.Process(0,0);
        event_data_struct event;
        write_DSTevent_to_event_struct( event, hess, i );
        events_array.push_back(event);

        events_array[i].tel1_image_Handle.len           = events_array[i].tel1_image.size();
        events_array[i].tel1_image_Handle.p             = events_array[i].tel1_image.data();
        events_array[i].tel2_image_Handle.len           = events_array[i].tel2_image.size();
        events_array[i].tel2_image_Handle.p             = events_array[i].tel2_image.data();
        events_array[i].tel3_image_Handle.len           = events_array[i].tel3_image.size();
        events_array[i].tel3_image_Handle.p             = events_array[i].tel3_image.data();
        events_array[i].tel4_image_Handle.len           = events_array[i].tel4_image.size();
        events_array[i].tel4_image_Handle.p             = events_array[i].tel4_image.data();
        events_array[i].tel5_image_Handle.len           = events_array[i].tel5_image.size();
        events_array[i].tel5_image_Handle.p             = events_array[i].tel5_image.data();

        events_array[i].tel1_pixel_ID_Handle.len        = events_array[i].tel1_pixel_ID.size();
        events_array[i].tel1_pixel_ID_Handle.p          = events_array[i].tel1_pixel_ID.data();
        events_array[i].tel2_pixel_ID_Handle.len        = events_array[i].tel2_pixel_ID.size();
        events_array[i].tel2_pixel_ID_Handle.p          = events_array[i].tel2_pixel_ID.data();
        events_array[i].tel3_pixel_ID_Handle.len        = events_array[i].tel3_pixel_ID.size();
        events_array[i].tel3_pixel_ID_Handle.p          = events_array[i].tel3_pixel_ID.data();
        events_array[i].tel4_pixel_ID_Handle.len        = events_array[i].tel4_pixel_ID.size();
        events_array[i].tel4_pixel_ID_Handle.p          = events_array[i].tel4_pixel_ID.data();
        events_array[i].tel5_pixel_ID_Handle.len        = events_array[i].tel5_pixel_ID.size();
        events_array[i].tel5_pixel_ID_Handle.p          = events_array[i].tel5_pixel_ID.data();

	events_array[i].tel1_pixloc_telnom_x_Handle.len = events_array[i].tel1_pixloc_telnom_x.size();	
	events_array[i].tel1_pixloc_telnom_x_Handle.p   = events_array[i].tel1_pixloc_telnom_x.data();
	events_array[i].tel2_pixloc_telnom_x_Handle.len = events_array[i].tel2_pixloc_telnom_x.size();	
	events_array[i].tel2_pixloc_telnom_x_Handle.p   = events_array[i].tel2_pixloc_telnom_x.data();
	events_array[i].tel3_pixloc_telnom_x_Handle.len = events_array[i].tel3_pixloc_telnom_x.size();	
	events_array[i].tel3_pixloc_telnom_x_Handle.p   = events_array[i].tel3_pixloc_telnom_x.data();
	events_array[i].tel4_pixloc_telnom_x_Handle.len = events_array[i].tel4_pixloc_telnom_x.size();	
	events_array[i].tel4_pixloc_telnom_x_Handle.p   = events_array[i].tel4_pixloc_telnom_x.data();
	events_array[i].tel5_pixloc_telnom_x_Handle.len = events_array[i].tel5_pixloc_telnom_x.size();	
	events_array[i].tel5_pixloc_telnom_x_Handle.p   = events_array[i].tel5_pixloc_telnom_x.data();

	events_array[i].tel1_pixloc_telnom_y_Handle.len = events_array[i].tel1_pixloc_telnom_y.size();	
	events_array[i].tel1_pixloc_telnom_y_Handle.p   = events_array[i].tel1_pixloc_telnom_y.data();
	events_array[i].tel2_pixloc_telnom_y_Handle.len = events_array[i].tel2_pixloc_telnom_y.size();	
	events_array[i].tel2_pixloc_telnom_y_Handle.p   = events_array[i].tel2_pixloc_telnom_y.data();
	events_array[i].tel3_pixloc_telnom_y_Handle.len = events_array[i].tel3_pixloc_telnom_y.size();	
	events_array[i].tel3_pixloc_telnom_y_Handle.p   = events_array[i].tel3_pixloc_telnom_y.data();
	events_array[i].tel4_pixloc_telnom_y_Handle.len = events_array[i].tel4_pixloc_telnom_y.size();	
	events_array[i].tel4_pixloc_telnom_y_Handle.p   = events_array[i].tel4_pixloc_telnom_y.data();
	events_array[i].tel5_pixloc_telnom_y_Handle.len = events_array[i].tel5_pixloc_telnom_y.size();	
	events_array[i].tel5_pixloc_telnom_y_Handle.p   = events_array[i].tel5_pixloc_telnom_y.data();

	events_array[i].tel1_pixloc_nom_x_Handle.len    = events_array[i].tel1_pixloc_nom_x.size();	
	events_array[i].tel1_pixloc_nom_x_Handle.p      = events_array[i].tel1_pixloc_nom_x.data();
	events_array[i].tel2_pixloc_nom_x_Handle.len    = events_array[i].tel2_pixloc_nom_x.size();	
	events_array[i].tel2_pixloc_nom_x_Handle.p      = events_array[i].tel2_pixloc_nom_x.data();
	events_array[i].tel3_pixloc_nom_x_Handle.len    = events_array[i].tel3_pixloc_nom_x.size();	
	events_array[i].tel3_pixloc_nom_x_Handle.p      = events_array[i].tel3_pixloc_nom_x.data();
	events_array[i].tel4_pixloc_nom_x_Handle.len    = events_array[i].tel4_pixloc_nom_x.size();	
	events_array[i].tel4_pixloc_nom_x_Handle.p      = events_array[i].tel4_pixloc_nom_x.data();
	events_array[i].tel5_pixloc_nom_x_Handle.len    = events_array[i].tel5_pixloc_nom_x.size();	
	events_array[i].tel5_pixloc_nom_x_Handle.p      = events_array[i].tel5_pixloc_nom_x.data();

	events_array[i].tel1_pixloc_nom_y_Handle.len    = events_array[i].tel1_pixloc_nom_y.size();	
	events_array[i].tel1_pixloc_nom_y_Handle.p      = events_array[i].tel1_pixloc_nom_y.data();
	events_array[i].tel2_pixloc_nom_y_Handle.len    = events_array[i].tel2_pixloc_nom_y.size();	
	events_array[i].tel2_pixloc_nom_y_Handle.p      = events_array[i].tel2_pixloc_nom_y.data();
	events_array[i].tel3_pixloc_nom_y_Handle.len    = events_array[i].tel3_pixloc_nom_y.size();	
	events_array[i].tel3_pixloc_nom_y_Handle.p      = events_array[i].tel3_pixloc_nom_y.data();
	events_array[i].tel4_pixloc_nom_y_Handle.len    = events_array[i].tel4_pixloc_nom_y.size();	
	events_array[i].tel4_pixloc_nom_y_Handle.p      = events_array[i].tel4_pixloc_nom_y.data();
	events_array[i].tel5_pixloc_nom_y_Handle.len    = events_array[i].tel5_pixloc_nom_y.size();	
	events_array[i].tel5_pixloc_nom_y_Handle.p      = events_array[i].tel5_pixloc_nom_y.data();
	
    }// end for loop

    hsize_t nevents = events_array.size();
    std::cout << i << " events read out of " << nevents << "\n" << std::endl;

    hsize_t dim_tel[] = { 5 };

    hsize_t dim_evt[] = { nevents };
    int rank_evt = sizeof(dim_evt) / sizeof(hsize_t);

    hsize_t dim_run[] = { 1 };
    int rank_run = sizeof(dim_run) / sizeof(hsize_t);

    hsize_t dim_mc_run[] = { 1 };
    int rank_mc_run = sizeof(dim_mc_run) / sizeof(hsize_t);

    hid_t vlenF_tid = H5Tvlen_create(H5T_NATIVE_FLOAT);
    hid_t vlenI_tid = H5Tvlen_create(H5T_NATIVE_INT);
    hid_t array_tid = H5Tarray_create( H5T_NATIVE_INT, 1, dim_tel );

    hid_t event_tid = H5Tcreate( H5T_COMPOUND, sizeof( event_data_struct ) );

    H5Tinsert( event_tid, "Event_ID", HOFFSET(event_data_struct, eventID), H5T_NATIVE_LLONG );
    H5Tinsert( event_tid, "Bunch_ID", HOFFSET(event_data_struct, bunchID), H5T_NATIVE_LLONG );
    H5Tinsert( event_tid, "Tels_in_event", HOFFSET(event_data_struct, tels_in_event), array_tid );
    H5Tinsert( event_tid, "Num_of_images", HOFFSET(event_data_struct, num_of_images), H5T_NATIVE_INT );

    H5Tinsert( event_tid, "CT1_image", HOFFSET(event_data_struct, tel1_image_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT2_image", HOFFSET(event_data_struct, tel2_image_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT3_image", HOFFSET(event_data_struct, tel3_image_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT4_image", HOFFSET(event_data_struct, tel4_image_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT5_image", HOFFSET(event_data_struct, tel5_image_Handle), vlenF_tid );

    H5Tinsert( event_tid, "CT1_pixel_ID", HOFFSET(event_data_struct, tel1_pixel_ID_Handle), vlenI_tid );
    H5Tinsert( event_tid, "CT2_pixel_ID", HOFFSET(event_data_struct, tel2_pixel_ID_Handle), vlenI_tid );
    H5Tinsert( event_tid, "CT3_pixel_ID", HOFFSET(event_data_struct, tel3_pixel_ID_Handle), vlenI_tid );
    H5Tinsert( event_tid, "CT4_pixel_ID", HOFFSET(event_data_struct, tel4_pixel_ID_Handle), vlenI_tid );
    H5Tinsert( event_tid, "CT5_pixel_ID", HOFFSET(event_data_struct, tel5_pixel_ID_Handle), vlenI_tid );

    // Telescope nominal system (not used at the moment) 
    /*
    H5Tinsert( event_tid, "CT1_pixloc_x_telnom", HOFFSET(event_data_struct, tel1_pixloc_telnom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT2_pixloc_x_telnom", HOFFSET(event_data_struct, tel2_pixloc_telnom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT3_pixloc_x_telnom", HOFFSET(event_data_struct, tel3_pixloc_telnom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT4_pixloc_x_telnom", HOFFSET(event_data_struct, tel4_pixloc_telnom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT5_pixloc_x_telnom", HOFFSET(event_data_struct, tel5_pixloc_telnom_x_Handle), vlenF_tid );

    H5Tinsert( event_tid, "CT1_pixloc_y_telnom", HOFFSET(event_data_struct, tel1_pixloc_telnom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT2_pixloc_y_telnom", HOFFSET(event_data_struct, tel2_pixloc_telnom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT3_pixloc_y_telnom", HOFFSET(event_data_struct, tel3_pixloc_telnom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT4_pixloc_y_telnom", HOFFSET(event_data_struct, tel4_pixloc_telnom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT5_pixloc_y_telnom", HOFFSET(event_data_struct, tel5_pixloc_telnom_y_Handle), vlenF_tid );
    */
    H5Tinsert( event_tid, "CT1_pixloc_x_nom", HOFFSET(event_data_struct, tel1_pixloc_nom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT2_pixloc_x_nom", HOFFSET(event_data_struct, tel2_pixloc_nom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT3_pixloc_x_nom", HOFFSET(event_data_struct, tel3_pixloc_nom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT4_pixloc_x_nom", HOFFSET(event_data_struct, tel4_pixloc_nom_x_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT5_pixloc_x_nom", HOFFSET(event_data_struct, tel5_pixloc_nom_x_Handle), vlenF_tid );

    H5Tinsert( event_tid, "CT1_pixloc_y_nom", HOFFSET(event_data_struct, tel1_pixloc_nom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT2_pixloc_y_nom", HOFFSET(event_data_struct, tel2_pixloc_nom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT3_pixloc_y_nom", HOFFSET(event_data_struct, tel3_pixloc_nom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT4_pixloc_y_nom", HOFFSET(event_data_struct, tel4_pixloc_nom_y_Handle), vlenF_tid );
    H5Tinsert( event_tid, "CT5_pixloc_y_nom", HOFFSET(event_data_struct, tel5_pixloc_nom_y_Handle), vlenF_tid );
    
//std::cout << " DEBUG " << std::endl;
    H5Tinsert( event_tid, "MonteCarloID", HOFFSET(event_data_struct, mcID), H5T_NATIVE_LLONG );
    H5Tinsert( event_tid, "MC_showerID", HOFFSET(event_data_struct, showerID), H5T_NATIVE_LLONG );
    H5Tinsert( event_tid, "PrimaryID", HOFFSET(event_data_struct, primaryID), H5T_NATIVE_INT );
    H5Tinsert( event_tid, "Energy", HOFFSET(event_data_struct, energy), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "Altitude", HOFFSET(event_data_struct, alt), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "Azimuth", HOFFSET(event_data_struct, az), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "DirX_Nominal", HOFFSET(event_data_struct, DirX_Nom), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "DirY_Nominal", HOFFSET(event_data_struct, DirY_Nom), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "Ra", HOFFSET(event_data_struct, ra), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "Dec", HOFFSET(event_data_struct, dec), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CoreX_Ground", HOFFSET(event_data_struct, CoreX_Ground), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CoreY_Ground", HOFFSET(event_data_struct, CoreY_Ground), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CoreZ_Ground", HOFFSET(event_data_struct, CoreZ_Ground), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CoreX", HOFFSET(event_data_struct, CoreX), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CoreY", HOFFSET(event_data_struct, CoreY), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CoreZ", HOFFSET(event_data_struct, CoreZ), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "XStart", HOFFSET(event_data_struct, xStart), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "XMax", HOFFSET(event_data_struct, xMax), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "First_int_alt", HOFFSET(event_data_struct, firstInteractionAlt), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "Time", HOFFSET(event_data_struct, time), H5T_NATIVE_DOUBLE );

    H5Tinsert( event_tid, "CT1_width", HOFFSET(event_data_struct, tel1_width), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_length", HOFFSET(event_data_struct, tel1_length), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_size", HOFFSET(event_data_struct, tel1_size), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_loc_dis", HOFFSET(event_data_struct, tel1_loc_dis), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_phi", HOFFSET(event_data_struct, tel1_phi), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_cog_X", HOFFSET(event_data_struct, tel1_cog_x), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_cog_Y", HOFFSET(event_data_struct, tel1_cog_y), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_skewness", HOFFSET(event_data_struct, tel1_skewness), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT1_kurtosis", HOFFSET(event_data_struct, tel1_kurtosis), H5T_NATIVE_FLOAT );

    H5Tinsert( event_tid, "CT2_width", HOFFSET(event_data_struct, tel2_width), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_length", HOFFSET(event_data_struct, tel2_length), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_size", HOFFSET(event_data_struct, tel2_size), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_loc_dis", HOFFSET(event_data_struct, tel2_loc_dis), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_phi", HOFFSET(event_data_struct, tel2_phi), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_cog_X", HOFFSET(event_data_struct, tel2_cog_x), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_cog_Y", HOFFSET(event_data_struct, tel2_cog_y), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_skewness", HOFFSET(event_data_struct, tel2_skewness), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT2_kurtosis", HOFFSET(event_data_struct, tel2_kurtosis), H5T_NATIVE_FLOAT );

    H5Tinsert( event_tid, "CT3_width", HOFFSET(event_data_struct, tel3_width), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_length", HOFFSET(event_data_struct, tel3_length), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_size", HOFFSET(event_data_struct, tel3_size), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_loc_dis", HOFFSET(event_data_struct, tel3_loc_dis), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_phi", HOFFSET(event_data_struct, tel3_phi), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_cog_X", HOFFSET(event_data_struct, tel3_cog_x), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_cog_Y", HOFFSET(event_data_struct, tel3_cog_y), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_skewness", HOFFSET(event_data_struct, tel3_skewness), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT3_kurtosis", HOFFSET(event_data_struct, tel3_kurtosis), H5T_NATIVE_FLOAT );

    H5Tinsert( event_tid, "CT4_width", HOFFSET(event_data_struct, tel4_width), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_length", HOFFSET(event_data_struct, tel4_length), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_size", HOFFSET(event_data_struct, tel4_size), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_loc_dis", HOFFSET(event_data_struct, tel4_loc_dis), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_phi", HOFFSET(event_data_struct, tel4_phi), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_cog_X", HOFFSET(event_data_struct, tel4_cog_x), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_cog_Y", HOFFSET(event_data_struct, tel4_cog_y), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_skewness", HOFFSET(event_data_struct, tel4_skewness), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT4_kurtosis", HOFFSET(event_data_struct, tel4_kurtosis), H5T_NATIVE_FLOAT );

    H5Tinsert( event_tid, "CT5_width", HOFFSET(event_data_struct, tel5_width), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_length", HOFFSET(event_data_struct, tel5_length), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_size", HOFFSET(event_data_struct, tel5_size), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_loc_dis", HOFFSET(event_data_struct, tel5_loc_dis), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_phi", HOFFSET(event_data_struct, tel5_phi), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_cog_X", HOFFSET(event_data_struct, tel5_cog_x), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_cog_Y", HOFFSET(event_data_struct, tel5_cog_y), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_skewness", HOFFSET(event_data_struct, tel5_skewness), H5T_NATIVE_FLOAT );
    H5Tinsert( event_tid, "CT5_kurtosis", HOFFSET(event_data_struct, tel5_kurtosis), H5T_NATIVE_FLOAT );

    hid_t run_tid = H5Tcreate( H5T_COMPOUND, sizeof( run_data_struct ) );
    H5Tinsert( run_tid, "Run_ID", HOFFSET(run_data_struct, run_number), H5T_NATIVE_UINT );
    H5Tinsert( run_tid, "NTels", HOFFSET(run_data_struct, ntels), H5T_NATIVE_INT );
    H5Tinsert( run_tid, "ObsAlt", HOFFSET(run_data_struct, obspos_alt), H5T_NATIVE_FLOAT );
    H5Tinsert( run_tid, "ObsAz", HOFFSET(run_data_struct, obspos_az), H5T_NATIVE_FLOAT );
    H5Tinsert( run_tid, "ObsRa", HOFFSET(run_data_struct, obspos_ra), H5T_NATIVE_FLOAT );
    H5Tinsert( run_tid, "ObsDec", HOFFSET(run_data_struct, obspos_dec), H5T_NATIVE_FLOAT );
    H5Tinsert( run_tid, "Duration", HOFFSET(run_data_struct, duration), H5T_NATIVE_INT );
    H5Tinsert( run_tid, "TimeStamp", HOFFSET(run_data_struct, time_stamp), H5T_NATIVE_LLONG );

    hid_t mc_run_tid = H5Tcreate( H5T_COMPOUND, sizeof( mc_run_data_struct ) );
    H5Tinsert( mc_run_tid, "Spectral_index", HOFFSET(mc_run_data_struct, spectral_index), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Max_energy", HOFFSET(mc_run_data_struct, max_e), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Min_energy", HOFFSET(mc_run_data_struct, min_e), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Max_cone", HOFFSET( mc_run_data_struct, max_cone ), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Min_cone", HOFFSET( mc_run_data_struct, min_cone ), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Core_pos_mode", HOFFSET( mc_run_data_struct, core_pos_mode ), H5T_NATIVE_INT );
    H5Tinsert( mc_run_tid, "Max_core_range", HOFFSET( mc_run_data_struct, max_core_range ), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Min_core_range", HOFFSET( mc_run_data_struct, min_core_range ), H5T_NATIVE_FLOAT );
    H5Tinsert( mc_run_tid, "Num_sim_showers", HOFFSET( mc_run_data_struct, num_sim_showers ), H5T_NATIVE_INT );
    H5Tinsert( mc_run_tid, "Times_shower_used", HOFFSET( mc_run_data_struct, times_shower_used ), H5T_NATIVE_INT );


    std::cout << "Converting MCDST root file to HDF5 format..." << std::endl;

    hid_t event_space   = H5Screate_simple(rank_evt, dim_evt, NULL);
    hid_t run_space     = H5Screate_simple(rank_run, dim_run, NULL);
    hid_t mc_run_space  = H5Screate_simple(rank_mc_run, dim_mc_run, NULL);

    // replace the .root extension with .h5
    file_name.replace( file_name.end() - extension.length(), file_name.end(), ".h5" );

    hid_t file = H5Fcreate( file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

    hid_t event_dataset = H5Dcreate(file, "Events_List", event_tid, event_space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(event_dataset, event_tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, &events_array[0]);

    hid_t run_dataset = H5Dcreate(file, "Run_Data", run_tid, run_space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite( run_dataset, run_tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, &run_data );

    hid_t mc_run_dataset = H5Dcreate(file, "MC_Run_Data", mc_run_tid, mc_run_space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite( mc_run_dataset, mc_run_tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, &mc_run_data );

    std::cout << "HDF5 file written to " + file_name +  "!" << std::endl;
    std::cout << "(file number " << file_count << " out of " << num_of_files << ")." << std::endl;

    H5Dclose( event_dataset );
    H5Dclose( run_dataset );
    H5Dclose( mc_run_dataset );
    H5Sclose( event_space );
    H5Sclose( run_space );
    H5Sclose( mc_run_space );
    H5Tclose( event_tid );
    H5Tclose( run_tid );
    H5Tclose( mc_run_tid );
    H5Fclose( file );

    f->Close("R");
    std::cout << "Closed TFile." << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    sleep(0.1);
    //DEBUG
    /*
    std::cout << 17354 << std::endl;
    std::cout << "event_num = " << events_array[17354].eventID << std::endl;
    std::cout << "MC shower Id = " << events_array[17354].showerID << std::endl;
    std::cout << "primary ID = " << events_array[17354].primaryID << std::endl;
    std::cout << "energy = " << events_array[17354].energy << std::endl;

    std::cout << "MC alt = " << events_array[17354].alt << std::endl;

    std::cout << "tels in event are " << events_array[17354].event_tels_str << std::endl;
    std::cout << "num of images is " << events_array[17354].num_of_images << std::endl;

    std::cout << "num of pixels = " << events_array[17354].tel1_image.size() << std::endl;

    std::cout << "tel1 width = " << events_array[17354].tel2_width << std::endl;
    std::cout << "tel1 kurtosis = " << events_array[17354].tel2_kurtosis << std::endl;

    for (std::vector<float>::const_iterator i = events_array[17354].tel2_image.begin(); i != events_array[17354].tel2_image.end(); ++i){
        std::cout << *i << ' ';
    }
    std::cout << std::endl;
    for (std::vector<int>::const_iterator i = events_array[17354].tel2_pixel_ID.begin(); i != events_array[17354].tel2_pixel_ID.end(); ++i)
                std::cout << *i << ' ';
    std::cout << std::endl;
    std::cout << std::endl;

    for (std::vector<float>::const_iterator i = events_array[17354].tel3_image.begin(); i != events_array[17354].tel3_image.end(); ++i){
        std::cout << *i << ' ';
    }
    std::cout << std::endl;
    for (std::vector<int>::const_iterator i = events_array[17354].tel3_pixel_ID.begin(); i != events_array[17354].tel3_pixel_ID.end(); ++i)
                std::cout << *i << ' ';
    std::cout << std::endl;
    std::cout << std::endl;*/

    } // close dst_list iterator

    return 0;
}

