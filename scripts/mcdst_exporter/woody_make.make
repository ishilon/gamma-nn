CPPFLAGS = -g -std=c++11 -Wall $(shell root-config --cflags) -I${HESSROOT}/include ${HDF5_INC}
LDFLAGS = $(shell root-config --ldflags) 
LDLIBS = $(shell root-config --glibs) ${HDF5_LIB} -L${HESSROOT}/lib -lrootHSenv -lrootHSbase -lrootHSevent -lrootsashfile -lrootmontecarlo -lrootstash -lrootmuon -lrootreco
CXX = $(shell root-config --cxx)

PROG=mcdst-exporter-v2.6
SRCS = $(PROG).cpp
OBJS = $(subst .cpp,.o,$(SRCS))

$(PROG): $(OBJS)
	$(CXX)  $(OBJS) -o $(PROG) $(LDFLAGS) $(LDLIBS)

$(OBJS): $(SRCS)
	$(CXX) $(CPPFLAGS) -c $(SRCS)

clean:
	rm -f *.o

