CPPFLAGS = -g -std=c++11 -Wall $(shell root-config --cflags) -I${HESSROOT}/include
LDFLAGS = $(shell root-config --ldflags) 
LDLIBS = $(shell root-config --glibs) -L${HESSROOT}/lib -lrootHSenv -lrootHSbase -lrootHSevent -lrootsashfile -lrootmontecarlo -lrootstash -lrootmuon -lrootreco
CXX = g++
#CXX = h5c++

HDF5FLAGS = -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_BSD_SOURCE -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -L/usr/lib64 /usr/lib64/libhdf5_hl_cpp.so /usr/lib64/libhdf5_cpp.so /usr/lib64/libhdf5_hl.so /usr/lib64/libhdf5.so -Wl,-z,relro -lz -ldl -lm -Wl,-rpath -Wl,/usr/lib64

PROG=mcdst-export-v2
SRCS = $(PROG).cpp
#OBJS = $(PROG).o
OBJS = $(subst .cpp,.o,$(SRCS))

$(PROG): $(OBJS)
	$(CXX) $(HDF5FLAGS) $(LDFLAGS) $(LDLIBS) -o $(PROG) $(OBJS)

$(OBJS): $(SRCS)
	#rootcint -c $(PROG).h
	$(CXX) $(CPPFLAGS) -c $(SRCS)

clean:
	rm -f *.o
