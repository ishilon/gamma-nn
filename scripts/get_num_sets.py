import os

log_dir = '/data/hessnn_data/models/direction-reco/konrad.mc_dst_hd_hap15-12.corsika74100_qgs2-04_urqmd1.3cr/phase2b5/gamma_pointsource.south.20deg.0.5deg.0.005-150tev/datasets_lists/2000.0k'

filelist = os.listdir(log_dir)

train_num = 0
valid_num = 0
test_num = 0

for file in filelist:
    if 'datasets' in file:
        print(file)
        with open(file, 'r') as f:
            lines = f.readlines()
            print(lines)
            train_num += int(lines[1].split(' ')[4])
            if lines[2]:
                valid_num += int( lines[2].split(' ')[4] )
                test_num += int( lines[3].split(' ')[4] )

print('train = ', train_num)
print('valid = ', valid_num)
print('test = ', test_num)