import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
import glob

paths = glob.glob('/home/tfischer/Dropbox/masterThesis/data/new_exports/PKS2155/*.h5')

ids = []
sizes = []
lods = []
for path in paths:
    with h5py.File(path, 'r') as f:
        triggered_telescope_count = np.sum((f['cam1_size'].value > 40.) &
                                           (f['cam1_loc_dis'].value < 0.525), axis=1)
        indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= 2]
        ids.append(f['id'][:][indices])
        sizes.append(f['cam1_size'][:][indices])
        lods.append(f['cam1_loc_dis'][:][indices])

ids = np.concatenate(ids)
sizes = np.concatenate(sizes)
lods = np.concatenate(lods)

uids = []
for id, size, lod in zip(ids, sizes, lods):
    uid = b'%i-%i-%i' % (id[5], id[1], id[0])
    if '75053-6060-999' in str(uid):
        print(id, size, lod)

with open('uids2.txt', 'w') as f:
    f.writelines("%s\n" % uid for uid in uids)

