import tensorflow as tf
import numpy as np
from tqdm import tqdm
import glob

paths = glob.glob('/media/tfischer/HD0/huge_pks_records/*.tfrecord')

def get_example(file_paths):
    filename_queue = tf.train.string_input_producer( file_paths, num_epochs=1 )
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read( filename_queue )
    tfrecord_features = tf.parse_single_example( serialized_example,
                                                 features = {'uid': tf.FixedLenFeature( [], tf.string ),
                                                             },
                                                 name = 'features' )
    return tfrecord_features

features = get_example(paths)


uids = []

sess = tf.Session()
sess.run(tf.global_variables_initializer())
sess.run(tf.local_variables_initializer())
coord = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coord)
try:
    k=0
    while not coord.should_stop():
        k+=1
        if not k%1000:
            print("\r",k, end='')
        uids.append(sess.run(features)['uid'])
except tf.errors.OutOfRangeError:
    print('Done iterating!')
finally:
    coord.request_stop()
coord.join(threads)
sess.close()

with open('uids.txt', 'w') as f:
    f.writelines("%s\n" % uid for uid in uids)

print("Total UIDs: ", len(uids))
print("Unique UIDs:", len(set(uids)))
