from gnnlib.utils import analysis_config_arg_parser as conf
from gnnlib.data import dataset_highspeed as dataset
from gnnlib.data import pixel_maps as pixm
from gnnlib.data import rebin_img
from gnnlib.utils import utils
from gnnlib.utils import eval_utils as evu
from gnnlib.evaluations import skymap

import importlib
import os
import tensorflow as tf
import pandas as pd
import math
from multiprocessing import Pool
import pickle
from datetime import datetime
import numpy as np


def analysis():
    config_args, conf_filename = conf.main()
    print( 'Your input arguments are:\n', config_args, '\n' )

    pickle_path = config_args.analysis_dir + 'presel_numbers.pickle'
    dict_name = 'cuts_' + str(config_args.h1_size_cut) + '_' + str(config_args.h1_loc_dis_cut)
    analysis_events_num = 0
    if os.path.exists(pickle_path) and not config_args.new_records_bool:
        with open( pickle_path, 'rb' )as handle:
            presel_num_dict = pickle.load(handle)
        if dict_name in presel_num_dict.keys():
            for key, value in presel_num_dict[dict_name].items():
                analysis_events_num += value
    else:
        presel_num_dict = { dict_name: {} }

    if config_args.new_records_bool:
        run_list = dataset.get_dsts_list([config_args.runs_dir])

        results_list = []

        def log_result( worker_files ):
            # This is called whenever process_dsts_list() returns a result.
            # result_list is modified only by the main process, not the pool workers.
            results_list.append( worker_files )

        counter = 0
        pool = Pool( processes=config_args.workers, maxtasksperchild=1 )
        for run in run_list:
            counter += 1
            if os.path.exists( config_args.analysis_dir + '/' + run[ run.rfind('/') + 1: -3] + '.tfrecord' ):
                continue
            pool.apply_async( dataset.process_dsts_list, args=(config_args, [run], run, True ), callback=log_result )
        pool.close()
        pool.join()

        for result in results_list:
            analysis_events_num += result[1]
            presel_num_dict[dict_name][result[0]] = result[1]

        with open( pickle_path, 'wb' ) as handle:
            pickle.dump( presel_num_dict, handle, protocol=pickle.HIGHEST_PROTOCOL )

    print(' The total number of events  = ', analysis_events_num)

    print( '\n***********************************' )
    print( 'Starting %s data Analysis' %config_args.data )
    print( '***********************************\n' )

    task_module = 'gnnlib.tasks.' + config_args.task
    model_module = 'gnnlib.nn.' + config_args.model + '_model'
    task = importlib.import_module( task_module )
    model = importlib.import_module( model_module )

    ckpt_dir = os.path.join(config_args.ckpt_dir, 'checkpoints_{0}/'.format(config_args.train_type))
    eval_dir = os.path.join(config_args.analysis_dir, config_args.model + '_eval/')

    tmp_dict = model.get_meta_consts()
    moving_average_decay = tmp_dict[ 'moving_average_decay' ]
    tower_name = utils.get_tower_name()

    csv_dir = os.path.join(config_args.analysis_dir , 'csv_files')
    if not os.path.exists( csv_dir ):
        os.mkdir( csv_dir )
    print('csv directory is: ', csv_dir)
    df_file_path = os.path.join(csv_dir , dict_name + '_' + config_args.model + '_' + config_args.train_type + '_analysis_df.csv')

    # #####################################################################

    # For running the analysis on each run separately
    #an = 1
    #for filename in os.listdir( csv_dir ):
    #    if '_an_' in filename:
    #        an += 1
    #df_file_path = csv_dir + dict_name + '_' + config_args.model + '_analysis_df_an_{0}.csv'.format(an)

    #analysis_events_num = 148184

    # ##################################################################################

    summaries = None
    if not os.path.isfile( df_file_path ) or config_args.redo_test_bool:
        test_tuple = None
        with tf.Graph().as_default() as graph:
            # Calculate loss
            with tf.variable_scope( tf.get_variable_scope() ):
                for i in range( config_args.num_gpus ):
                    with tf.device( '/gpu:%d' % i ):
                        with tf.name_scope( '%s_%d' % (tower_name, i) ) as scope:
                            # Calculate the loss for one tower of the HESSnn model. This function
                            # constructs the entire HESSnn model but shares the variables across
                            # all towers.
                            test_tuple = task.tower_loss( config_args, scope, model, config_args.analysis_dir, tower_name, eval_data='test' )
                            # Reuse variables for the next tower.
                            # This function lets get_variable() use an existing variable,
                            # rather than initialising a new one, with the name Variable_scope/Variable_name.
                            tf.get_variable_scope().reuse_variables()

                            # Retain the summaries from the final tower.
                            # tf.GraphKeys are standard names to graph collections
                            summaries = tf.get_collection( tf.GraphKeys.SUMMARIES, scope )

            # Restore the moving average version of the learned variables for eval.
            variable_averages = tf.train.ExponentialMovingAverage( moving_average_decay )
            variables_to_restore = variable_averages.variables_to_restore()
            saver = tf.train.Saver( variables_to_restore )

            # Build the summary operation based on the TF collection of Summaries.
            summary_writer = tf.summary.FileWriter( eval_dir, graph )

            # get the predictions
            if 'dir' in config_args.task:
                print('hi, direction reco')
                prediction = test_tuple[1]
            else:
                # a TF op to get the softmax activations for each class
                prediction = tf.nn.softmax( test_tuple[1] )
                print('hi, classification')

            config = tf.ConfigProto( allow_soft_placement=True )
            with tf.Session( config = config ) as sess:
                global_step = evu.load_checkpoint( ckpt_dir, sess, saver )
                sess.run( tf.local_variables_initializer() )
                # Start the queue runners.
                coord = tf.train.Coordinator()
                threads = []

                dfs = []
                try:
                    for qr in tf.get_collection( tf.GraphKeys.QUEUE_RUNNERS ):
                        threads.extend( qr.create_threads( sess, coord = coord, daemon = True, start = True ) )

                    num_iter = int( math.ceil( analysis_events_num / config_args.batch_size ) )
                    print( 'Number of batches: ', num_iter, '\tTotal number of test_examples:', analysis_events_num )
                    step = 0
                    num = analysis_events_num % config_args.batch_size

                    while step < num_iter and not coord.should_stop():
                        if step % 100 == 0:
                            print(datetime.now().strftime( '%d-%m-%Y %H:%M:%S' ) + ': Analysed {0} batches out of {1}'.format(step, num_iter))
                            # Build the summary operation from the last tower summaries.
                            summary_op = tf.summary.merge( summaries )
                            summary = sess.run( summary_op )
                            summary_writer.add_summary( summary, global_step )
                            summary_writer.flush()
                        (batch_uid, batch_prediction,
                         batch_ct1_loc_dis, batch_ct2_loc_dis, batch_ct3_loc_dis, batch_ct4_loc_dis,
                         batch_ct1_length, batch_ct2_length, batch_ct3_length, batch_ct4_length,
                         batch_ct1_width, batch_ct2_width, batch_ct3_width, batch_ct4_width,
                         batch_ct1_size, batch_ct2_size, batch_ct3_size, batch_ct4_size,
                         batch_src_alt, batch_src_az, batch_obs_alt, batch_obs_az, batch_obs_time ) = sess.run( [test_tuple[3], prediction,
                                                                                        test_tuple[5], test_tuple[6], test_tuple[7], test_tuple[8],
                                                                                        test_tuple[9], test_tuple[10], test_tuple[11], test_tuple[12],
                                                                                        test_tuple[13], test_tuple[14], test_tuple[15], test_tuple[16],
                                                                                        test_tuple[17], test_tuple[18], test_tuple[19], test_tuple[20],
                                                                                        test_tuple[21], test_tuple[22], test_tuple[23], test_tuple[24], test_tuple[25]] )
                        batch_obs_time = [time.decode() for time in batch_obs_time]
                        index = list( range(step * config_args.batch_size, (step + 1) * config_args.batch_size ) )
                        b_step = np.ones(config_args.batch_size)*step
                        # Don't include events that are already listed
                        # print(step, batch_uid[0])
                        if step == num_iter -1 and num != 0:
                            num = analysis_events_num % config_args.batch_size
                            print( 'Evaluating last batch, containing {0} events'.format( num ) )
                            batch_uid = batch_uid[: num ]
                            batch_prediction = batch_prediction[: num ]
                            batch_ct1_loc_dis = batch_ct1_loc_dis[: num ]
                            batch_ct2_loc_dis = batch_ct2_loc_dis[: num ]
                            batch_ct3_loc_dis = batch_ct3_loc_dis[: num ]
                            batch_ct4_loc_dis = batch_ct4_loc_dis[: num ]
                            batch_ct1_length = batch_ct1_length[: num ]
                            batch_ct2_length = batch_ct2_length[: num ]
                            batch_ct3_length = batch_ct3_length[: num ]
                            batch_ct4_length = batch_ct4_length[: num ]
                            batch_ct1_width = batch_ct1_width[: num ]
                            batch_ct2_width = batch_ct2_width[: num ]
                            batch_ct3_width = batch_ct3_width[: num ]
                            batch_ct4_width = batch_ct4_width[: num ]
                            batch_ct1_size = batch_ct1_size[: num ]
                            batch_ct2_size = batch_ct2_size[: num ]
                            batch_ct3_size = batch_ct3_size[: num ]
                            batch_ct4_size = batch_ct4_size[: num ]
                            batch_src_alt = batch_src_alt[: num ]
                            batch_src_az = batch_src_az[: num ]
                            batch_obs_alt = batch_obs_alt[: num]
                            batch_obs_az = batch_obs_az[: num]
                            batch_obs_time = batch_obs_time[: num ]
                            index = index[: num ]
                            b_step = b_step[:num]

                        if 'dir' in config_args.task:
                            pred_dalt = np.multiply(batch_prediction[:, 0], config_args.alt_scale).ravel()
                            pred_daz = np.multiply(batch_prediction[:, 1], config_args.az_scale).ravel()
                            dummy = -np.ones(len(pred_dalt))

                            pred_alt = (90- batch_src_alt) + pred_dalt
                            pred_az = batch_src_az + pred_daz
                            #print('pred alt', pred_alt, 'pred az', pred_az, 'pointing alt ', (90- batch_src_alt), 'pointing az', batch_src_az)
                            batch_df = pd.DataFrame({'uid': batch_uid, 'predicted_alt': pred_alt, 'predicted_az': pred_az,
                                                     'zeta': dummy, 'predicted_energy': dummy,
                                                     'predicted_corex': dummy, 'predicted_corey': dummy,
                                                     'ct1_loc_dis': batch_ct1_loc_dis, 'ct2_loc_dis': batch_ct2_loc_dis,
                                                     'ct3_loc_dis': batch_ct3_loc_dis,
                                                     'ct4_loc_dis': batch_ct4_loc_dis,
                                                     'ct1_length': batch_ct1_length, 'ct2_length': batch_ct2_length,
                                                     'ct3_length': batch_ct3_length, 'ct4_length': batch_ct4_length,
                                                     'ct1_width': batch_ct1_width, 'ct2_width': batch_ct2_width,
                                                     'ct3_width': batch_ct3_width, 'ct4_width': batch_ct4_width,
                                                     'ct1_size': batch_ct1_size, 'ct2_size': batch_ct2_size,
                                                     'ct3_size': batch_ct3_size, 'ct4_size': batch_ct4_size,
                                                     'pointing_alt': 90. - batch_src_alt, 'pointing_az': batch_src_az,
                                                     'obs_time': batch_obs_time, 'batch': b_step},
                                                     columns = ['uid', 'predicted_alt', 'predicted_az', 'zeta', 'predicted_energy',
                                                                'predicted_corex', 'predicted_corey', 'ct1_loc_dis', 'ct2_loc_dis',
                                                                'ct3_loc_dis','ct4_loc_dis','ct1_length','ct2_length','ct3_length',
                                                                'ct4_length','ct1_width','ct2_width','ct3_width','ct4_width',
                                                                'ct1_size','ct2_size','ct3_size','ct4_size','pointing_alt',
                                                                'pointing_az','obs_time','batch'], index=index)
                        else:
                            dummy = -np.ones(len(batch_src_alt))
                            batch_df = pd.DataFrame({'uid': batch_uid, 'predicted_alt': dummy, 'predicted_az': dummy,
                                                    'zeta': batch_prediction[:, 0], 'predicted_energy': dummy,
                                                    'predicted_corex': dummy, 'predicted_corey': dummy,
                                                    'ct1_loc_dis': batch_ct1_loc_dis,
                                                    'ct2_loc_dis': batch_ct2_loc_dis,
                                                    'ct3_loc_dis': batch_ct3_loc_dis,
                                                    'ct4_loc_dis': batch_ct4_loc_dis,
                                                    'ct1_length': batch_ct1_length, 'ct2_length': batch_ct2_length,
                                                    'ct3_length': batch_ct3_length, 'ct4_length': batch_ct4_length,
                                                    'ct1_width': batch_ct1_width, 'ct2_width': batch_ct2_width,
                                                    'ct3_width': batch_ct3_width, 'ct4_width': batch_ct4_width,
                                                    'ct1_size': batch_ct1_size, 'ct2_size': batch_ct2_size,
                                                    'ct3_size': batch_ct3_size, 'ct4_size': batch_ct4_size,
                                                    'pointing_alt': 90. - batch_src_alt, 'pointing_az': batch_src_az,
                                                    'obs_time': batch_obs_time, 'batch': b_step},
                                                     columns = ['uid', 'predicted_alt', 'predicted_az', 'zeta', 'predicted_energy',
                                                                'predicted_corex', 'predicted_corey', 'ct1_loc_dis', 'ct2_loc_dis',
                                                                'ct3_loc_dis','ct4_loc_dis','ct1_length','ct2_length','ct3_length',
                                                                'ct4_length','ct1_width','ct2_width','ct3_width','ct4_width',
                                                                'ct1_size','ct2_size','ct3_size','ct4_size','pointing_alt',
                                                                'pointing_az','obs_time','batch'], index=index)

                        dfs.append(batch_df)
                        step += 1

                except Exception as e:  # pylint: disable=broad-except
                    coord.request_stop( e )

                coord.request_stop()
                coord.join( threads, stop_grace_period_secs = 10 )

                # print('freezing your model...')
                # evu.freeze_model( sess=sess, output_graph_file=os.path.join( ckpt_dir, 'frozen_model.pb' ), output_node_names=[ 'logits_scope/output_node' ] )
            print('concatenating batches DFs')
            analysis_df = pd.concat( dfs )
            print('Sorting merged DF')
            analysis_df = analysis_df.sort_values( by=['uid'], ascending=True )
            analysis_df = analysis_df.drop_duplicates(['uid'])
            print('Writing dataframe to csv.')
            analysis_df.to_csv(df_file_path, index=False)
            if 'dir' in config_args.task:
                print('%s: Running skymap creation...' % datetime.now())
                skymap.skymap(df_file_path)
    else:
        analysis_df = pd.read_csv( df_file_path )
        if 'dir' in config_args.task:
            print('%s: Running skymap creation...' % datetime.now())
            skymap.skymap(df_file_path)


if __name__ == '__main__':
    analysis()