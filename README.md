Ground based γ-ray observations with Imaging Atmospheric Cherenkov Telescopes (IACTs) play a
significant role in the discovery of very high energy (E > 100 GeV) γ-ray emitters. The analysis of
IACT data demands a highly efficient background rejection technique, as well as methods to accurately
determine the energy of the recorded γ-ray and the position of its source in the sky. 

The code includes the entire pipeline and models we have used to present results
for background rejection and signal direction reconstruction from first studies of a novel data analysis
scheme for IACT measurements. The new analysis is based on a set of Convolutional Neural Networks
(CNNs) applied to images from the four H.E.S.S. phase-I telescopes. As the H.E.S.S. cameras pixels
are arranged in a hexagonal array, we demonstrate two ways to use such image data to train CNNs: by
resampling the images to a square grid and by applying modified convolution kernels that conserve the
hexagonal grid properties.

Paper published in: Astropart.Phys. 105 (2019) 44-53

Open access: https://arxiv.org/pdf/1803.10698.pdf