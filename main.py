import sys
import os
import shutil
import time
import importlib

from gnnlib.utils import utils
from gnnlib.utils import config_arg_parser as conf
from gnnlib.nn import train


def get_folder_names( config_args ):
    """
    Generate the model and dataset folders from the user input argument
    :param config_args: user input arguments from config or command line
    :return:
    """
    gnn_model = utils.get_env_variable( 'GNN_MDL' )
    num = str( config_args.train_events / 1000 )
    if config_args.ct5_resolution == 0:
        array = 'hess1'
    else:
        array = 'hess2'

    if 'clas' in config_args.task:
        train_type = 'classification'
    elif 'dir' in config_args.task:
        train_type = 'direction'
    elif 'en' in config_args.task:
        train_type = 'energy'
    elif 'core' in config_args.task:
        train_type = 'core'
    else:
        sys.exit('Please check your config train_type argument (sent from get_folder_names() in main).')

    if config_args.model_dir != 'pc':
        # in model_dir the checkpoints, training and evaluation files can be found
        model_dir = gnn_model + '/models/' + train_type + '/' + config_args.model_dir + '/'
        # in dataset dir the dataset lists and tfrecord can be found
        ds_dir = model_dir + 'dataset/'
    else:
        meta_dir = gnn_model + '/models/' + \
                   train_type + '/' + \
                   array + '/' + \
                   config_args.inst + '.' + \
                   '-'.join( config_args.phase ) + '.' +\
                   '-'.join( config_args.dtype ) + '.' + \
                   '-'.join( config_args.hemi ) + '.' + \
                   '-'.join( config_args.zenith ) + '.' + \
                   '-'.join( config_args.offset ) + '/'

        im_dir = None
        if array == 'hess1':
            im_dir = meta_dir + \
                       'h1_sampling_' + config_args.hess1_sampling + '.' + \
                       'h1_resolution_' + str( config_args.hess1_resolution ) + '.' + \
                       'num_rotations_' + str( config_args.num_image_rotations ) + '.' + \
                       'train_events_' + num + 'k/'
        elif array == 'hess2':
            im_dir = meta_dir + \
                     'h1_sampling_' + config_args.hess1_sampling + '.' + \
                     'h1_resolution_' + str( config_args.hess1_resolution ) + '.' + \
                     'ct5_sampling_' + config_args.ct5_sampling + '.' + \
                     'ct5_resolution_' + str( config_args.ct5_resolution ) + '.' + \
                     'num_rotations_' + str( config_args.num_image_rotations ) + '.' + \
                     'train_events_' + num + 'k/'

        ds_dir = os.path.join( im_dir, 'dataset/' )
        model_dir = os.path.join( im_dir, 'image_scale_' + config_args.scale_images + '.' +
                                  'method_' + config_args.task + '.' +
                                  'arc_' + config_args.train_type + '/' )

    if not os.path.exists( model_dir ):
        os.makedirs( model_dir )

    if not os.path.exists( ds_dir ):
        os.makedirs( ds_dir )

    return model_dir, ds_dir


def gnn_main():
    """Run with: python3 $GNN/main.py -c "config_file_name"
    No need to include the directory in which the file is saved or the ".conf" ending.
    """
    config_args, conf_filename = conf.main()
    print('Your input arguments are:\n', config_args, '\n')

    ev_module = 'gnnlib.evaluations.' + config_args.task + '_eval'
    ev = importlib.import_module( ev_module )

    # Create folders for dataset info and model files (including dataset files, checkpoints, etc.)
    model_dir, ds_dir = get_folder_names( config_args )

    # Keep a copy of the config file in model_dir
    shutil.copy( conf_filename, model_dir )

    if config_args.new_dataset_bool or not os.path.exists( os.path.join( ds_dir, 'train_file_f1_1.tfrecord' ) ):
        from gnnlib.data import dataset_highspeed as dataset

        print( 'Creating new dataset files for training and evaluation!' )
        # Get the list folders from which the dataset files will be created
        folders_list = dataset.get_h5dst_folders_list( config_args )
        # Write the folder list file to the dataset folder
        utils.print_list_to_txt( folders_list, ds_dir )

        # Create training tfrecords
        dsts_list = dataset.create_dataset_files( config_args, folders_list, ds_dir )

        del dsts_list

    if os.stat( '{0}train_file_f1_1.tfrecord'.format( ds_dir ) ).st_size < 100000:
        sys.exit( 'Exiting: Empty "training.tfrecord" file!' )

    # Read the number of examples for the datasets
    ds_nums = { }
    for key in [ 'train', 'valid', 'test' ]:
        with open( os.path.join( ds_dir, 'logfiles/{0}_log.txt'.format( key ) ) ) as log:
            lines = log.readlines()
            ds_nums[ key ] = int( lines[ 1 ].split( ' ' )[ 4 ] )

    # Create directory for writing checkpoints
    checkpoints_dir = os.path.join( model_dir, 'checkpoints_' + config_args.train_type + '/' )
    if not os.path.exists( checkpoints_dir ):
        os.makedirs( checkpoints_dir )

    if not config_args.test_set_bool:
        # Number of training steps needed to complete an epoch
        n_steps_per_epoch = ds_nums['train'] // config_args.batch_size

        # Number of training steps between evaluation runs
        n_steps_per_train_cycle = config_args.epochs_per_train_cycle * n_steps_per_epoch
        if n_steps_per_train_cycle > config_args.max_steps:
            print('The number of steps per train cycle you specified is larger than the max number of steps you specified.')
            utils.exit_point(20)
            n_steps_per_train_cycle = config_args.max_steps

        total_train_epochs = config_args.max_steps // n_steps_per_epoch

        print( 'The number of training steps per epoch is: {0}'.format( n_steps_per_epoch ) )
        print( 'Evaluating training and validation scores every {0} epochs.\n'.format( config_args.epochs_per_train_cycle ) )
        print( 'The number of training steps between evaluation runs is ', n_steps_per_train_cycle )
        print( 'The total number of epochs for this training run is ', total_train_epochs )

        i_epoch = 0  # epoch number

        while i_epoch < total_train_epochs:
            i_epoch += config_args.epochs_per_train_cycle
            run = 1  # "run" number

            # Don't run over existing files
            if os.path.exists( os.path.join(model_dir, 'train_' + config_args.train_type + '/') ):
                for filename in os.listdir( os.path.join(model_dir, 'train_' + config_args.train_type + '/') ):
                    if 'run_' in filename:
                        run += 1

            # Create directories for writing eval files
            train_dir = model_dir + '/train_' + config_args.train_type + '/train_run_' + str( run ) + '/'
            if not os.path.exists( train_dir ):
                os.makedirs( train_dir )
            train_eval_dir = model_dir + '/eval_' + config_args.train_type + '/train_eval_' + str( run ) + '/'
            if not os.path.exists( train_eval_dir ):
                os.makedirs( train_eval_dir )
            valid_eval_dir = model_dir + '/eval_' + config_args.train_type + '/valid_eval_' + str( run ) + '/'
            if not os.path.exists( valid_eval_dir ):
                os.makedirs( valid_eval_dir )

            print( '\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-' )
            print( 'Beginning {0} training'.format( config_args.train_type ) )
            print( '-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-' )
            train.execute_cycle( config_args, ds_dir, train_dir, checkpoints_dir, n_steps_per_train_cycle )

            time.sleep( 1.0 )
            ev.main(config_args, 'train', model_dir, ds_dir, train_eval_dir, checkpoints_dir, ds_nums['train'])
            time.sleep( 1.0 )
            ev.main(config_args,
                    'valid',
                    model_dir,
                    ds_dir,
                    valid_eval_dir,
                    checkpoints_dir,
                    ds_nums['valid']
                    )
            time.sleep( 1.0 )
    else:
        test_eval_dir = model_dir + '/test_eval_' + config_args.train_type + '/'
        if not os.path.exists( test_eval_dir ):
            os.makedirs( test_eval_dir )
        ev.main(config_args,
                'test',
                model_dir,
                ds_dir,
                test_eval_dir,
                checkpoints_dir,
                ds_nums['test']
                )


if __name__ == '__main__':
    gnn_main()
